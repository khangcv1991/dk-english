angular.module('app')
.controller('PrivacyPolicyController', ['$scope', '$http', function($scope, $http){
    var $logoHeight = $('.logo').outerHeight();
    var $footerHeight = $('.wrapper-footer').outerHeight();
    $('.page-content-box').css('height', $(window).height() - $logoHeight - $footerHeight - 80);
    new PerfectScrollbar('.page-content-box');

}])
