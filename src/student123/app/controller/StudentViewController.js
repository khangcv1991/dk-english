angular.module('app')
    .controller('StudentViewController', ['$rootScope', '$scope', '$http', '$templateCache', '$stateParams', '$q', function($rootScope, $scope, $http, $templateCache, $stateParams, $q) {
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        //get all subject list
        $scope.selsubject = null;
        $scope.userid = null;
        $scope.reloadPage = function(userid = null) {
            // 
            if($scope.userid != null){
                $scope.getTopicBySubject($scope.userid);
            }else if (userid != null) {
                $scope.getTopicBySubject($scope.userid);
            }
        }
        $scope.initiateSubjects = function(userid) {
            $scope.userid = userid;
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_student_unenrolled_subjects&userid=' + userid
            }).then(function successCallback(response) {
                //$scope.subjects = []; 
                $scope.subjectList = response.data.data;
                console.log($scope.subjectList);
                $(document).ready(function() {
                    if ($('.choose-new-subject-box').hasClass('choose-new-subject-box')) {
                        var $currentTitleHeight = $('.choose-new-subject-page h2').outerHeight();
                        var $titleSubject = $('.title-subject').outerHeight();
                        var $logoHeight = $('.logo-row').outerHeight();
                        var $footerHeight = $('.wrapper-footer').outerHeight();
                        $('.choose-new-subject-box').css('height', $(window).height() - $titleSubject - $logoHeight - $footerHeight - 50);
                        new PerfectScrollbar('.choose-new-subject-box');
                    }
                });
            }, function errorCallback(response) {});
        }

        $scope.initiateCurrentSubjects = function(userid) {
            $scope.userid = userid;
            loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_all_enrolled_subjects&userid=' + userid).then(function(response) {
                console.log(response);
                // if (response.data.error) {
                //     swal({
                //         icon: 'error',
                //         showCloseButton: true,
                //         closeModal: true,
                //         text: response.data.error,
                //         button: 'ok'
                //     })
                //     return;
                // }
                $scope.currentSubjects = response.data.data;
                console.log($scope.currentSubjects);
                if($scope.currentSubjects.length > 0){
                    $scope.activeCurrentSubject = true;
                }
                $(document).ready(function() {
                    if ($('.current-subjects').hasClass('current-subjects')) {
                        var $logoHeight = $('.logo-row').outerHeight();
                        var $footerHeight = $('.wrapper-footer').outerHeight();
                        var $currentTitleHeight = $('.choose-new-subject-page h2').outerHeight();
                        $('.current-subjects').css('height', $(window).height() - $logoHeight - $footerHeight - $currentTitleHeight - 50);
                        $('.current-subject-student').css('height', $(window).height() - $logoHeight - $footerHeight - 10);
                        new PerfectScrollbar('.current-subjects');
                    }
                });
            });
        }

        $scope.getAllSubjectData = function(userid) {
            $scope.userid = userid;
            loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_student_unenrolled_subjects&userid=' + userid).then(function(response) {
                if (response.data.error) {
                    swal({
                        icon: 'error',
                        showCloseButton: true,
                        closeModal: true,
                        text: response.data.error,
                        button: 'ok'
                    })
                    return;
                }
                $scope.subjects = [];
                $scope.subjects = response.data.data;
                console.log($scope.subjects);
                $(document).ready(function() {
                    if ($('.choose-new-subject-box').hasClass('choose-new-subject-box')) {
                        var $currentTitleHeight = $('.choose-new-subject-page h2').outerHeight();
                        var $logoHeight = $('.logo-row').outerHeight();
                        var $footerHeight = $('.wrapper-footer').outerHeight();
                        $('.choose-new-subject-box').css('height', $(window).height() - $currentTitleHeight - $logoHeight - $footerHeight - 50);
                        new PerfectScrollbar('.choose-new-subject-box');
                    }
                });
            });
        }
        //get all subject list
        $scope.getcurrentSubjectData = function(userid) {
            $scope.userid = userid;
            loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_all_enrolled_subjects&userid=' + userid).then(function(response) {
                if (response.data.error) {
                    swal({
                        icon: 'error',
                        showCloseButton: true,
                        closeModal: true,
                        text: response.data.error,
                        button: 'ok'
                    })
                    return;
                }
                $scope.currentSubjects = [];
                $scope.currentSubjects = response.data.data;
                console.log($scope.currentSubjects);
                $(document).ready(function() {
                    if ($('.current-subjects').hasClass('current-subjects')) {
                        var $logoHeight = $('.logo-row').outerHeight();
                        var $footerHeight = $('.wrapper-footer').outerHeight();
                        var $currentTitleHeight = $('.choose-new-subject-page h2').outerHeight();
                        $('.current-subjects').css('height', $(window).height() - $logoHeight - $footerHeight - $currentTitleHeight - 50);
                        $('.current-subject-student').css('height', $(window).height() - $logoHeight - $footerHeight - 10);
                        new PerfectScrollbar('.current-subjects');
                    }
                });
            });

            function logUser(){
                $.ajax({
                    url: '/local/b13_dashboard/load-ajax.php?classname=b13log&method=log_user_sign_in&userid='+userid,
                    type: 'GET',
                    success: function(data) {
                        console.log(data);
                    }
                });
            }

            $(document).ready(function(){
                logUser();
            });

            
        }

        //Remove Current Subject
        $scope.deleteCurrentSubjectData = function(userid, subjectid) {
            $scope.userid = userid;
            swal({
                    title: "Are you sure?",
                    text: "All progress will be deleted!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then(function(willDelete) {
                    if (willDelete) {
                        $http({
                            method: 'PUT',
                            url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=unenroll_user_subject&userid=' + userid + '&subjectid=' + subjectid,
                        }).then(function successCallback(response) {
                            swal("Your subject has been deleted!", {
                                icon: "success",
                            }).then(function(){
                                location.reload();
                            });
                            $scope.getcurrentSubjectData(userid);
                            console.log(response);
                            $scope.initiateSubjects(userid);
                        }, function errorCallback(response) {
                            //alert(response);
                        });
                    } else {
                        swal("Your subject is safe!");
                    }
                });
        }
        //choose new subject
        $scope.choose_new_subject = function(userid, subjectid) {
            $scope.userid = userid;
            $scope.showChooseOption == false;
            //update 
            loadDataFromUrl("/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=detail_subject&subjectid=" + subjectid).then(function(response) {
                $scope.subjects = response.data.data;
                $scope.getCurrentSubjects = $scope.subjects;
                console.log($scope.getCurrentSubjects);
            });
            loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_examboard_by_subject&subjectid=' + subjectid).then(function(response) {
                $scope.examboardOptions = response.data.data;
                if ($scope.examboardOptions.length) {
                    $scope.showChooseOption = true;
                } else {
                    console.log('test');
                }
                console.log($scope.examboardOptions);
                $('.modal').on('hidden.bs.modal', function() {
                    // location.reload();
                })
            })
            $scope.checkSubjectOption = false;
            $scope.hasChanged = function() {
                if ($scope.examboardid == null)
                    return;
                console.log($scope.examboardid);
                $scope.getExamboardId = $scope.examboardid;
                loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_options_by_examboard&examboardid=' + $scope.examboardid + "&subjectid=" + subjectid).then(function(response) {
                    $scope.subjectOptions = response.data.data;
                    if ($scope.subjectOptions.length > 0) {
                        $scope.checkSubjectOption = true;
                        console.log($scope.checkSubjectOption);
                    } else {
                        $scope.checkSubjectOption = false;
                        console.log($scope.checkSubjectOption);
                    }
                    console.log($scope.subjectOptions);
                })
            }
            $scope.selsubject = subjectid;
        }

        $scope.detail_subject_option = function(userid, subjectid) {
            $scope.userid = userid;
            //update 
            subjectid == null || subjectid == undefined ? $stateParams.id : $scope.selsubject;
            if (typeof subjectid === 'undefined' || subjectid === null) {
                // Do stuff
                subjectid = $stateParams.id;
            }
            loadDataFromUrl("/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=detail_subject&subjectid=" + subjectid).then(function(response) {
                $scope.subjects = response.data.data;
                $scope.getCurrentSubjects = $scope.subjects;
                $('.modal').on('hidden.bs.modal', function() {
                    // location.reload();
                })
            });

            //local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_options_by_examboard&examboardid=2
            if (userid == null) {
                userid = $scope.userid;
            }
            loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_student_configuration_by_subject&userid=' + userid + '&subjectid=' + subjectid).then(function(response) {
                if (response.data.error) {
                    console.log(response.data.error);
                    return;
                }
                console.log(response);
                $scope.selectedexamboardOptions = response.data.data[0].selexamboard;
                //console.log($scope.selectedexamboardOptions);
                $scope.selectedsubjectOptions = response.data.data[0].seloptions;
                $scope.selectedExamboard = [];
                $scope.teacheremail = response.data.data[0].teacher.teacheremail;
                $scope.teachername = response.data.data[0].teacher.teachername;

                angular.forEach($scope.selectedexamboardOptions, function(value, key) {
                    if (value.selected == 1) {
                        this.push(value);
                        loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_options_by_examboard&examboardid=' + value.id).then(function(response) {
                            $scope.subjectOptionByExamboard = response.data.data;
                            console.log($scope.subjectOptionByExamboard);
                        })
                    }
                }, $scope.selectedExamboard);
                $scope.selectedExamboard = $scope.selectedExamboard[0];
                console.log($scope.selectedExamboard);
                // $scope.examboardid = $scope.selectedExamboard.id;
                $scope.checkSubjectOption = false;
                $scope.hasChanged = function(examboardid) {
                    console.log(examboardid.id);
                    $scope.examboardid = examboardid.id;
                    loadDataFromUrl('/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_options_by_examboard&examboardid=' + examboardid.id + '&subjectid=' + $scope.selsubject).then(function(response) {
                        $scope.changedsubjectOptions = response.data.data;
                        $scope.selectedsubjectOptions = response.data.data;
                        if ($scope.changedsubjectOptions.length > 0) {
                            $scope.checkSubjectOption = true;
                        }
                        if ($scope.changedsubjectOptions.length > 0) {
                            $scope.checkSubjectOption = true;
                            console.log($scope.changedsubjectOptions);
                        } else {
                            $scope.changedsubjectOptions = false;
                            console.log($scope.checkSubjectOption);
                        }
                        console.log($scope.changedsubjectOptions);
                    })
                }


                if ($scope.selectedsubjectOptions.length > 0) {
                    $scope.checkSubjectOption = true;
                }
                $scope.selsubject = subjectid;
            })
        }
        //update select options
        $scope.updateStudentSelection = function(userid, subjectid, subjectOptions) {
            $scope.userid = userid;
            // if (!$scope.chooseNewSubject.$valid) {
            //     if ($('.teacher-email').val() == "") {
            //         swal({
            //             icon: 'error',
            //             showCloseButton: true,
            //             closeModal: true,
            //             text: 'Teacher email is required',
            //             button: 'ok'
            //         });
            //     } else {
            //         swal({
            //             icon: 'error',
            //             showCloseButton: true,
            //             closeModal: true,
            //             text: 'Enter a valid email',
            //             button: 'ok'
            //         });
            //     }
            //     return;
            // }

            subjectid == null ? $scope.selsubject : subjectid;
            var options = [];
            for (var i = 0; i < subjectOptions.length; i++) {
                if (subjectOptions[i].selected == 1) {
                    options.push(subjectOptions[i].id)
                }
            }
            if (options.length == 0 && subjectOptions > 0) {
                swal({
                    icon: 'error',
                    showCloseButton: true,
                    closeModal: true,
                    text: 'Select options are required',
                    button: 'ok'
                })
                return;
            }
            $scope.selectOptionData = {
                examboardid: parseInt($scope.selectedExamboard.id),
                userid: parseInt(userid),
                subjectid: parseInt(subjectid),
                options: options,
                teacheremail: $scope.teacheremail,
                teachername: $scope.teachername
            }

            $http({
                method: 'POST',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=add_student_selection',
                data: $.param($scope.selectOptionData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function(response) {
                if (response.data.error) {
                    swal({
                        icon: 'warning',
                        showCloseButton: true,
                        closeModal: true,
                        text: response.data.error,
                        button: 'ok'
                    })
                } else {
                    swal({
                        icon: 'success',
                        showCloseButton: true,
                        closeModal: true,
                        text: 'Changes Saved',
                        button: 'ok'
                    }).then(function(){
                        location.reload();
                    });
                }
            });


        }

        $scope.addStudentSelection = function(userid, subjectid, subjectOptions, examboardid, teachername, teacheremail) {
            $scope.userid = userid;
            if ($("#examboard ")[0].selectedIndex <= 0) {
                    swal({
                        icon: 'error',
                        showCloseButton: true,
                        closeModal: true,
                        text: 'Exam board is required',
                        button: 'ok'
                    });
                    return;
                }else{
                    $(document).ready(function(){
                            var email = $(".teacher-email").val();
                            if(email != 0)
                              {
                                if(isValidEmailAddress(email))
                                {subjectid == null ? $scope.selsubject : subjectid;
                                    var options = [];
                                    for (var i = 0; i < subjectOptions.length; i++) {
                                        if (subjectOptions[i].selected == 1) {
                                            options.push(subjectOptions[i].id)
                                        }
                                    }
                                    $scope.selectOptionData = {
                                        examboardid: parseInt(examboardid),
                                        userid: parseInt(userid),
                                        subjectid: parseInt(subjectid),
                                        options: options,
                                        teacheremail: $scope.teacheremail,
                                        teachername: $scope.teachername
                                    }
                                    $http({
                                        method: 'POST',
                                        url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=enroll_user_subject',
                                        data: $.param({
                                            userid: parseInt(userid),
                                            subjectid: parseInt(subjectid)
                                        }),
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        }
                                    }).then(function(response) {
                                        console.log(response);
                                        if(response.data.data){
                                            $scope.initiateCurrentSubjects(response.data.data[0].userid);
                                            $scope.initiateSubjects(response.data.data[0].userid);
                                            $http({
                                                method: 'POST',
                                                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=add_student_selection',
                                                data: $.param($scope.selectOptionData),
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                }
                                            }).then(function(response) {
                                                console.log(response);
                                                if (response.data.error) {
                                                    swal({
                                                        icon: 'warning',
                                                        showCloseButton: true,
                                                        closeModal: true,
                                                        text: response.data.error,
                                                        button: 'ok'
                                                    })
                                                    } else {
                                                        swal({
                                                            icon: 'success',
                                                            showCloseButton: true,
                                                            closeModal: true,
                                                            text: 'Subject details saved',
                                                            button: 'ok'
                                                        }).then(function() {
                                                            location.reload();
                                                            console.log(response);
                                                        });
                                                    }
                                                });
                                            }else{
                                                swal({
                                                    icon: 'warning',
                                                    showCloseButton: true,
                                                    closeModal: true,
                                                    text: response.data.error,
                                                    button: 'ok'
                                                })
                                            }
                                        })
                                } else {
                                    swal({
                                        icon: 'error',
                                        showCloseButton: true,
                                        closeModal: true,
                                        text: 'Enter a valid email',
                                        button: 'ok'
                                    });
                                }
                            } else {
                                // swal({
                                //     icon: 'error',
                                //     showCloseButton: true,
                                //     closeModal: true,
                                //     text: 'Teacher email is required',
                                //     button: 'ok'
                                // });  
                                
                                var options = [];
                                    for (var i = 0; i < subjectOptions.length; i++) {
                                        if (subjectOptions[i].selected == 1) {
                                            options.push(subjectOptions[i].id)
                                        }
                                    }
                                    $scope.selectOptionData = {
                                        examboardid: parseInt(examboardid),
                                        userid: parseInt(userid),
                                        subjectid: parseInt(subjectid),
                                        options: options,
                                        teacheremail: $scope.teacheremail,
                                        teachername: $scope.teachername
                                    }
                                    $http({
                                        method: 'POST',
                                        url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=enroll_user_subject',
                                        data: $.param({
                                            userid: parseInt(userid),
                                            subjectid: parseInt(subjectid)
                                        }),
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        }
                                    }).then(function(response) {
                                        console.log(response);
                                        if(response.data.data){
                                            $scope.initiateCurrentSubjects(response.data.data[0].userid);
                                            $scope.initiateSubjects(response.data.data[0].userid);
                                            $http({
                                                method: 'POST',
                                                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=add_student_selection',
                                                data: $.param($scope.selectOptionData),
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                }
                                            }).then(function(response) {
                                                console.log(response);
                                                if (response.data.error) {
                                                    swal({
                                                        icon: 'warning',
                                                        showCloseButton: true,
                                                        closeModal: true,
                                                        text: response.data.error,
                                                        button: 'ok'
                                                    })
                                                    } else {
                                                        swal({
                                                            icon: 'success',
                                                            showCloseButton: true,
                                                            closeModal: true,
                                                            text: 'Subject details saved',
                                                            button: 'ok'
                                                        }).then(function() {
                                                            location.reload();
                                                            console.log(response);
                                                        });
                                                    }
                                                });
                                            }else{
                                                swal({
                                                    icon: 'warning',
                                                    showCloseButton: true,
                                                    closeModal: true,
                                                    text: response.data.error,
                                                    button: 'ok'
                                                })
                                            }
                                        })
                            }
                    });

                    function isValidEmailAddress(emailAddress) {
                        var filter = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                        if (filter.test(emailAddress)) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                       
            }
    }

        $scope.getAllExamboard = function() {
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_all_examboards'
            }).then(function successCallback(response) {
                $scope.examboards = response.data.data;
                console.log($scope.examboards);
            }, function errorCallback(response) {
                alert(response);
            });
        }

        $scope.getTopicBySubject = function(userid) {
            $scope.userid = userid;
            $scope.selsubject = $stateParams.id;
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=load_topics_by_student&subjectid=' + $scope.selsubject + '&userid=' + userid
            }).then(function successCallback(response) {
                if ($('.current-subjects').hasClass('current-subjects')) {
                    var $logoHeight = $('.logo-row').outerHeight();
                    var $footerHeight = $('.wrapper-footer').outerHeight();
                    $('.current-subjects').css('height', $(window).height() - $logoHeight - $footerHeight - 50);
                    $('.current-subject-student').css('height', $(window).height() - $logoHeight - $footerHeight - 10);
                    new PerfectScrollbar('.current-subjects');
                }
                $scope.topics = response.data.data.filter(function(item, index, arr){
                    return item.nosubtopics > 0;
                });
                console.log($scope.topics);
                if($scope.topics.length > 0){

                }else{
                    swal({
                        icon: 'warning',
                        showCloseButton: true,
                        closeModal: true,
                        text: 'Please choose option to show topic',
                        button: 'ok'
                    })
                }
            }, function errorCallback(response) {
                alert(response);
            });
        }

        $scope.getSubtopicByTopic = function(userid) {
            $scope.userid = userid;
            $scope.topicId = parseInt($stateParams.id);
            $scope.subjectId = $stateParams.topicid;
            console.log($scope.topicId);
            console.log($scope.subjectId);
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=load_all_subtopics_by_student&topicid=' + $scope.topicId + '&userid=' + userid
            }).then(function successCallback(response) {
                if (response.data.error) {
                    swal({
                        icon: 'warning',
                        showCloseButton: true,
                        closeModal: true,
                        text: response.data.error,
                        button: 'ok'
                    })
                }
                if ($('.current-subjects').hasClass('current-subjects')) {
                    var $logoHeight = $('.logo-row').outerHeight();
                    var $footerHeight = $('.wrapper-footer').outerHeight();
                    $('.current-subjects').css('height', $(window).height() - $logoHeight - $footerHeight - 50);
                    $('.current-subject-student').css('height', $(window).height() - $logoHeight - $footerHeight - 10);
                    new PerfectScrollbar('.current-subjects');
                }
                $scope.subtopics = response.data.data;
                console.log(response);
            }, function errorCallback(response) {
                alert(response);
            });
        }

        $scope.nextPageVisible = true;
        $scope.previousPageVisible = false;

        $scope.getBookBySubtopic = function($userid) {
            $scope.subTopicId = $stateParams.id;
            $scope.pageno = $stateParams.pageno;
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_status_student_subtopic&userid=' + $userid + '&subtopicid=' + $scope.subTopicId + '&pageno=' + $scope.pageno
            }).then(function successCallback(response) {
                var url;
                // console.log(response);
                // if (response.data.error != null) {
                //     console.log(response.data.error);
                //     $(document).ajaxComplete(function(){
                //     var statusStudent = response.data.data[0];
                //         url = '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid=' + statusStudent.$userid + '&subtopicid=' + $scope.subTopicId + '&pageno=1';
                //     });
                // } else {
                //     var statusStudent = response.data.data[0];
                //     url = '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid=' + statusStudent.$userid + '&subtopicid=' + $scope.subTopicId + '&pageno=' + statusStudent.readedpage;
                // }
                // var statusStudent = response.data.data[0];
                // url = '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid=' + statusStudent.$userid + '&subtopicid=' + $scope.subTopicId + '&pageno=' + statusStudent.readedpage;
            
                // $http({
                //     method: 'GET',
                //     url: url
                // }).then(function successCallback(response) {
                //     if (response.data.error) {
                //         console.log(response.data.error);
                //         return;
                //     }
                //     $scope.contentCover = response.data.data[0];

                //     var pagenum = parseInt($scope.contentCover.pagenum);

                //     if (pagenum == 1) {
                //         $scope.previousPageVisible = false;
                //     } else {
                //         $scope.previousPageVisible = true;
                //     }

                // }, function errorCallback(response) {
                //     alert(response);
                // });


            }, function errorCallback(response) {
                alert(response);
            });


        }
        $scope.nextPage = function($userid, $nextPageId, $totalPages) {
            $scope.subTopicId = $stateParams.id;
            $totalPage = parseInt($totalPages);
            $nextPage = parseInt($nextPageId);
            $pageno = parseInt($nextPage) + 1;
            if ($nextPage > 0) {
                $scope.previousPageVisible = true;
            }
            console.log($pageno);
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid=' + $userid + '&subtopicid=' + $scope.subTopicId + '&pageno=' + $pageno
            }).then(function successCallback(response) {
                //$scope.getBookBySubtopic($userid); 
                $scope.contentCover = response.data.data[0];
                var pagenum = parseInt($scope.contentCover.pagenum);
                console.log(pagenum);
                var revision = parseInt($scope.contentCover.revision);
                if (pagenum == revision) {
                    $scope.nextPageVisible = false;
                }
            });
        }
        $scope.previousPage = function($userid, $nextPageId, $totalPages) {
            $scope.subTopicId = $stateParams.id;
            $totalPage = parseInt($totalPages);
            $nextPage = parseInt($nextPageId);
            $pageno = parseInt($nextPage) - 1;
            console.log($pageno);
            $scope.nextPageVisible = true;
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid=' + $userid + '&subtopicid=' + $scope.subTopicId + '&pageno=' + $pageno
            }).then(function successCallback(response) {
                //$scope.getBookBySubtopic($userid); 
                $scope.contentCover = response.data.data[0];
                var pagenum = parseInt($scope.contentCover.pagenum);
                console.log(pagenum);
                var revision = parseInt($scope.contentCover.revision);
                if (pagenum == 1) {
                    $scope.previousPageVisible = false;
                }
            });
        }

        function sendRequest(method, classname, fname, data) {
            var deferred = $q.defer();
            var url = '/local/b13_dashboard/load-ajax.php?classname=' + classname + '&method=' + fname;

            $http({
                method: method,
                url: url,
                data: $.param(data),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function(response) {
                if (response.error) {
                    swal({
                        icon: 'warning',
                        showCloseButton: true,
                        closeModal: true,
                        text: response.error,
                        button: 'ok'
                    })
                    deferred.reject(response);
                } else {
                    swal({
                        icon: 'success',
                        showCloseButton: true,
                        closeModal: true,
                        text: 'Changes Saved',
                        button: 'ok'
                    }).then(function() {
                        //console.log(response);
                        $rootScope.$apply(function() {
                            // location.reload();
                        });
                    });
                    deferred.resolve(response);
                }

            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;

        }
        $scope.backToTopic = function() {
            window.location.href = "/student/#/topic/" + $scope.subjectId;
        }


        function loadDataFromUrl(url) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        }
    }]);