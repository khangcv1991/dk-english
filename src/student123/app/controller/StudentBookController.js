angular.module('app')
.controller('StudentBookController', ['$rootScope', '$scope', '$http', '$templateCache', '$stateParams', '$q', '$location', function($rootScope, $scope, $http, $templateCache,$stateParams, $q, $location){
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    
    
    $scope.nextPageVisible = true;
    $scope.previousPageVisible = false;
    $scope.quizTestPage = false;
    $scope.getBookBySubtopic = function(userid){
        $scope.subTopicId = $stateParams.id;
        $scope.pageno = $stateParams.pageno;
        $scope.userid = userid;
        $http({
            method: 'GET',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_status_student_subtopic&userid='+userid+'&subtopicid='+$scope.subTopicId+'&pageno='+$scope.pageno
        }).then(function successCallback(response) {
            var url;
            if(response.data.error){
                url = '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid='+$scope.userid +'&subtopicid='+$scope.subTopicId+'&pageno=1';
            }else{
                var statusStudent = response.data.data[0];
                if(statusStudent.readedpage == 0){
                    url = '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid='+statusStudent.userid+'&subtopicid='+$scope.subTopicId+'&pageno=1';
                }else{
                    url = '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid='+statusStudent.userid+'&subtopicid='+$scope.subTopicId+'&pageno='+statusStudent.readedpage;
                }
            }
            $scope.contentCover = {};
            $scope.currentpage = statusStudent == null? 1:statusStudent.readedpage;
            $http({
                method: 'GET',
                url:  url
            }).then(function successCallback(response) {
                $scope.nextPageVisible = false;
                if(response.data.error){
                        swal({
                        icon: 'warning',
                        showCloseButton: true,
                        closeModal:true,
                        text: response.data.error,
                        button:'Back'
                        }).then(function(){
                            window.history.back();
                        });
                    return;
                }
                $scope.contentCover = response.data.data[0];
                $scope.contentCover.pagenum = $scope.currentpage ;
                $('#loading').hide();
                $scope.reportIssueSubmit = function(){
                    $('#loading').show();
                    var issueData = {
                        mailto:"khangcv1991@gmail.com",
                        title: $scope.issueTitle,
                        body: $scope.issueContent,
                        url: $location.absUrl(),
                        userid:$scope.userid,
                        subjectid:$scope.contentCover.subjectid,
                        topicid:$scope.contentCover.topicid,
                        subtopicid:$scope.subTopicId,
                        quizid:'',
                        questionid:'',
                        bookid:$scope.contentCover.bookid,
                        chapterid:$scope.contentCover.chapterid
                    };
                    $.ajax({
                        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url         : '/local/b13_dashboard/load-ajax.php?classname=b13students&method=create_content_correction', // the url where we want to POST
                        data        : issueData, // our data object
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
                    }).done(function(response) {
                            $('#loading').hide();
                            if(response.data.error){
                                        swal({
                                            icon: 'warning',
                                            showCloseButton: true,
                                            closeModal:true,
                                            text: response.data.error,
                                            button:'Ok'
                                        }).then(function(){
                                            location.reload();
                                        });
                                    }else{
                                        swal({
                                            icon: 'success',
                                            showCloseButton: true,
                                            closeModal:true,
                                            text: 'Sent successfully!!!',
                                            button:'Ok'
                                        }).then(function(){
                                            location.reload();
                                        });
                                    }
                    });
                    // $http({
                    //     method: 'POST',
                    //     url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=create_content_correction',
                    //     data: $scope.issueData,
                    //     headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    // }).then(function(response) {
                    //     if(response.data.error){
                    //         swal({
                    //             icon: 'warning',
                    //             showCloseButton: true,
                    //             closeModal:true,
                    //             text: response.data.error,
                    //             button:'Ok'
                    //         }).then(function(){
                    //             location.reload();
                    //         });
                    //     }else{
                    //         swal({
                    //             icon: 'success',
                    //             showCloseButton: true,
                    //             closeModal:true,
                    //             text: 'Sent successfully!!!',
                    //             button:'Ok'
                    //         }).then(function(){
                    //             location.reload();
                    //         });
                    //     }
                    //   }, function(error) {
             
                    // });
                }

                console.log($scope.contentCover);
                $scope.backToSubTopic = function(){
                    window.location.href="/student/#/"+$scope.contentCover.subjectid+"/subtopic/"+$scope.contentCover.topicid;
                }
                if( $scope.currentpage == 1){
                    $scope.previousPageVisible = false;
                }else{
                    $scope.previousPageVisible = true;

                }
                if( parseInt($scope.currentpage) == $scope.contentCover.totalpages){
                    $scope.nextPageVisible = false;
                    $scope.quizTestPage = true;
                }else{
                    $scope.nextPageVisible = true;
                }

                $scope.quizTestButton = function(){
                    window.location.href="/student/#/test-question/"+$scope.contentCover.subjectid+"/"+$scope.contentCover.topicid+"/"+$scope.contentCover.subtopic+"/"+$scope.subTopicId;
                }
                
            }, function errorCallback(response) {
                alert(response);
            });
         
            
        }, function errorCallback(response) {
            alert(response);
        });

        
    }

    $scope.nextPage = function(userid,totalPages){
        $scope.subTopicId = $stateParams.id;
        totalPage = parseInt(totalPages);
        pageno = parseInt($scope.currentpage) + 1;
        console.log(pageno);
        if($scope.currentpage > 0){
            $scope.previousPageVisible = true;
        }
        if(pageno == totalPages){
            $scope.nextPageVisible = false;
            $scope.quizTestPage = true;

        }
        if($scope.currentpage == totalPages){
            return;
        }
        $scope.currentpage = pageno;
        $http({
            method: 'GET',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid='+userid+'&subtopicid='+$scope.subTopicId+'&pageno='+pageno
        }).then(function successCallback(response) {
            //$scope.getBookBySubtopic(userid); 
            $scope.contentCover = response.data.data[0];
            var pagenum = parseInt($scope.contentCover.pagenum);
            console.log(pagenum);
            var revision = parseInt($scope.contentCover.revision);
            if( pagenum == revision){
                $scope.nextPageVisible = false;
            }
        });
    }

    $scope.previousPage = function(userid,totalPages){
        $scope.subTopicId = $stateParams.id;
        totalPage = parseInt(totalPages);
        pageno = parseInt($scope.currentpage) - 1;
        $scope.quizTestPage = false;
        console.log(pageno);
        $scope.nextPageVisible = true;
        if($scope.currentpage == 2){
            $scope.previousPageVisible = false;
        }
        if(pageno == 0){
            return;
        }
        $scope.currentpage = pageno;
        $http({
            method: 'GET',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_book_page&userid='+userid+'&subtopicid='+$scope.subTopicId+'&pageno='+pageno
        }).then(function successCallback(response) {
            //$scope.getBookBySubtopic(userid); 
            $scope.contentCover = response.data.data[0];
            var pagenum = parseInt($scope.contentCover.pagenum);
            console.log(pagenum);
            var revision = parseInt($scope.contentCover.revision);
            if( pagenum == 1){
                $scope.previousPageVisible = false;
            }
        });
    }

}]);