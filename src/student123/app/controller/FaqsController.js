angular.module('app')
.controller('FaqsController', ['$scope', '$http', function($scope, $http){
	$(document).ready(function(){
        new PerfectScrollbar('.toggleFaqsBox');
    });
    var $logoHeight = $('.logo').outerHeight();
    var $footerHeight = $('.wrapper-footer').outerHeight();
    $('.page-content-box').css('height', $(window).height() - $logoHeight - $footerHeight - 110);
    new PerfectScrollbar('.page-content-box');
}])