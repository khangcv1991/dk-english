angular.module('app')
.controller('StudentQuestionController', ['$rootScope', '$scope', '$http', '$templateCache', '$stateParams', '$q', function($rootScope, $scope, $http, $templateCache,$stateParams, $q){
    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    //get all subject list
    $scope.selsubject = null;
    $scope.reloadPage = function(){
        location.reload();
    }
    $scope.selexamboard = -1;
    $scope.enableTypingQuestion = false;
    $scope.stopQuestion = false;
    $scope.disableQuestion = false;
    $scope.hideContinue = true;
    $scope.userid = null;
    $scope.enableFeedback = false;
    $scope.feedbackcontent = "";
    $scope.listQuestionBySutopic = function(userid){
        $scope.userid = userid;
        $scope.getSubTopicId = $stateParams.subtopicid;
        $scope.getTopicId = $stateParams.topicid;
        $scope.subTopicId = $stateParams.id;
        console.log($scope.subTopicId);
        $scope.subTopicName = $stateParams.slug;
        $http({
            method: 'GET',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13srl&method=list_questions_by_subtopic&subtopic='+$scope.subTopicId+'&userid='+userid
        }).then(function successCallback(response) {
            // console.log(response);
            $scope.completeTest = false;
            $scope.allQuestionList  = response.data.data;
            console.log($scope.allQuestionList);
            if($scope.allQuestionList.length == 0){
                swal({
                    icon: 'warning',
                    showCloseButton: true,
                    closeModal:true,
                    text: 'No Questions',
                    button:'Exit'
                }).then(function(){
                    window.history.go(-1);
                });
            }
            $scope.srlQuestionList = response.data.data.filter(function(item, index, arr){

                return item.boxnumber < 5;
            
            });
            if($scope.srlQuestionList.length == 0){
                $scope.srlQuestionList = $scope.allQuestionList ;
            }
            $scope.currentQuestion = -1;
            for (var i = 0; i < $scope.srlQuestionList.length; i++){
                if( $scope.srlQuestionList[i].answered == 0 ){
                    $scope.currentQuestion = i;
                    break;
                }
            }

            // if($scope.currentQuestion == -1){
            //     $scope.completeTestQuestion();
            // }
            $scope.totalQuestion =  $scope.srlQuestionList.length;
            $scope.hideSubmitAnswer = true;
            $scope.showContinue = false;
            
            $http({
                method: 'GET',
                url: '/local/b13_dashboard/load-ajax.php?classname=b13srl&method=detail_question&questiongroupid='+ $scope.srlQuestionList[$scope.currentQuestion].id + "&userid=" + $scope.userid
            }).then(function successCallback(result) {
                $scope.detailQuestion = result.data.data;
                $scope.enableFeedback = false;
                $scope.feedbackcontent = "";
                // if($scope.detailQuestion[0].question.qtype == 'multichoice' && parseInt($scope.srlQuestionList[$scope.currentQuestion].boxnumber)>= 3 && $scope.detailQuestion[0].question.numberofcorrectanswers == 1){
                //     $scope.enableTypingQuestion = true;
                // }else{
                //     $scope.enableTypingQuestion = false;
                // }

                console.log($scope.detailQuestion);
                $scope.checkArray = [];
                angular.forEach($scope.detailQuestion[0].answers, function(value, key){
                    $scope.object = {id: value.id, checked: false};
                    $scope.checkArray.push($scope.object);
                });

                console.log($scope.checkArray);
                
                $scope.CheckBox_Checked = function (position,checkArray) {
                    // for (var i = 0; i < $scope.checkArray.length; i++) {
                    //     if ($scope.checkArray[i].id != selectedId) {
                    //         $scope.checkArray[i].selected = false;
                    //     }
                    // }
                    angular.forEach(checkArray, function(subscription, index) {
                        if (position != index) 
                          subscription.checked = false;
                    });
                }
            });

            console.log($scope.srlQuestionList);
            console.log($scope.totalQuestion);
        });
    }

    $scope.nextQuestion = function(){
        $scope.resultAnswer = null;
        $scope.currentQuestion += 1;
        $scope.hideSubmitAnswer = true;
        $scope.showContinue = false;
        $scope.enableFeedback = false;
        $scope.feedbackcontent = "";
        $http({
            method: 'GET',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13srl&method=detail_question&questiongroupid='+$scope.srlQuestionList[$scope.currentQuestion].id + "&userid=" + $scope.userid
        }).then(function successCallback(result) {
            $scope.detailQuestion = result.data.data;
            
            // if($scope.detailQuestion[0].question.qtype == 'multichoice' && parseInt($scope.srlQuestionList[$scope.currentQuestion].boxnumber) >= 3 && $scope.detailQuestion[0].question.numberofcorrectanswers == 1){
            //     $scope.enableTypingQuestion = true;
            // }else{
            //     $scope.enableTypingQuestion = false;
            // }
            console.log($scope.srlQuestionList);
            console.log($scope.detailQuestion);
            // console.log(result);
        });
    }
  
    $scope.saveProgression = function(){
        var data = {};
         data.userid = $scope.userid;
         data.quizlist = JSON.stringify($scope.allQuestionList) ;
         data.subtopicid = $scope.subTopicId;
        $http({
            method: 'POST',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=save_quiz_progression',
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            swal({
                icon: 'success',
                showCloseButton: true,
                closeModal:true,
                text: 'Saved progress Successfully',
                button:'Exit'
            }).then(function(){
                window.history.go(-1);
            });
          }, function(error) {

        });
    }
    $scope.completeTestQuestion = function(){
        var data = {};
        data.userid = $scope.userid;
        for(var i = 0; i < $scope.allQuestionList.length; i++){
            $scope.allQuestionList[i].answered = 0;
            $scope.allQuestionList[i].attempted = 0;
        }
        data.quizlist = JSON.stringify($scope.allQuestionList) ;
        data.subtopicid = $scope.subTopicId;
       $http({
           method: 'POST',
           url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=save_quiz_progression',
           data: $.param(data),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'}
       }).then(function(response) {
            console.log(response);
            swal({
                icon: 'success',
                showCloseButton: true,
                closeModal:true,
                text: 'Completed Test Successfully',
                button:'See Result'
            }).then(function(){
                $http({
                    method: 'GET',
                    url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_subtopic_quiz_progression&subtopicid='+$scope.getSubTopicId+'&userid='+$scope.userid
                }).then(function successCallback(response) {
                    $scope.resultquiztest = response.data.data;
                    window.location.href="/student/#/test-result/"+$scope.getTopicId+"/"+$scope.getSubTopicId+"/"+$scope.subTopicName+"/"+$stateParams.id+"/result";
                })
              
            });
         }, function(error) {

       });
    };

    $scope.showloading = true;
    $scope.submitQuestion = function(questionid, choice, typeQuestion){
        var answers = [];
        $scope.enableFeedback = false;
        $scope.feedbackcontent = "";
        if((typeQuestion == 'truefalse' || typeQuestion == 'multichoice') && $scope.enableTypingQuestion == false){
            if(questionid == null || choice == null){
                $scope.hideSubmitAnswer = true;
                $scope.showContinue = false;
                return;
            }
            angular.forEach(choice, function(value, key){
                if(choice[key].checked){
                    answers.push(choice[key].id);
                }
            });
        }else if(typeQuestion == 'multichoice' && $scope.enableTypingQuestion){
            var typedAnswer = $('#typedAnswer').val();
            for(var i = 0; i < choice.length; i++){
                if(typedAnswer.trim().toLocaleLowerCase().replace(/(\r\n|\n|\r)/gm, "").replace(/ /g,'') == $(choice[i].answer).text().trim().replace(/(\r\n|\n|\r)/gm, "").replace(/ /g,'').toLocaleLowerCase()){
                    answers.push(choice[i].id);
                }
            }
            if(answers.length == 0 ){
                answers.push(-1);
            }
        }
        else{
            var shortAnswer = $('.shortanswer-type input').val();
            if(shortAnswer != "" && shortAnswer != null){
                answers.push(shortAnswer);
            }
            console.log(shortAnswer);
        }
        console.log(answers);
        console.log(choice);
        $scope.answerArray = [];
        for(var i = 0; i < $scope.detailQuestion[0].answers.length; i ++){
            if(parseFloat($scope.detailQuestion[0].answers[i].fraction) > 0){
                  $scope.idAnswer = $scope.detailQuestion[0].answers[i];
                  $scope.answerArray.push($scope.idAnswer.id);
                 $scope.checkIsAnswer = $scope.idAnswer.id;
            }
        }
        console.log($scope.answerArray);
        if(answers.length == 0 ){
            swal({
                icon: 'error',
                showCloseButton: true,
                closeModal: true,
                text: 'Please select or type at least one option',
                button: 'ok'
            });
            return;
        }
        if(typeQuestion == 'multichoice' && answers.length != parseInt($scope.detailQuestion[0].question.numberofcorrectanswers)){
            
            swal({
                icon: 'error',
                showCloseButton: true,
                closeModal: true,
                text: 'You need to select ' + $scope.detailQuestion[0].question.numberofcorrectanswers + ' correct answers',
                button: 'ok'
            });
            return;
        }
        $http({
            method: 'POST',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13srl&method=check_question_answers',
            data: $.param({
                userid: $scope.userid,
                subtopicid: $scope.subTopicId,
                questionid:parseInt(questionid), 
                answers:answers,
                attempted: $scope.srlQuestionList[$scope.currentQuestion].attempted
            }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            $scope.showloading = false;
            console.log(response);
            // $scope.
            $scope.isChecked = function(id){
                return answers.indexOf(id) != -1;
            }
            $scope.resultAnswer = response.data.data;
            console.log($scope.resultAnswer);
            $scope.hideSubmitAnswer = true;
            $scope.showContinue = false;
            if($scope.resultAnswer == 1){
               $scope.showContinue = true;
               $scope.hideSubmitAnswer = false;
               $scope.srlQuestionList[$scope.currentQuestion].answered ++;
               $scope.srlQuestionList[$scope.currentQuestion].attempted ++;
               
              
            }else{
               $scope.srlQuestionList[$scope.currentQuestion].attempted ++;
               $scope.showContinue = false;
               $scope.feedbackcontent = "<p>The correct answer is: </p>";
               $scope.enableFeedback = true;
               $scope.answerArray = []; 
               for(var i = 0; i < $scope.detailQuestion[0].answers.length; i ++){
                   if(parseFloat($scope.detailQuestion[0].answers[i].fraction) > 0){
                        $scope.feedbackcontent += '<div>' + $scope.detailQuestion[0].answers[i].answer.split(".*").join("") + '</div>';
                         $scope.idAnswer = $scope.detailQuestion[0].answers[i];
                         $scope.answerArray.push($scope.idAnswer.id);
                        console.log($scope.idAnswer.id);
                        $scope.checkIsAnswer = $scope.idAnswer.id;
                        console.log($scope.idAnswer);
                   }
               }
               console.log($scope.answerArray);

               if($scope.feedbackcontent){
                    $('.checkboxOptions').each(function(){
                        $(this).find('label').click(function(){
                            $(this).find('i').css('display','none');
                        });
                        if($(this).find('input:checkbox:checked').length > 0){
                            $(this).find('label').click();
                        }
                    });
               }


               
            }
            updateItemQuestionList($scope.srlQuestionList[$scope.currentQuestion]);
            if($scope.currentQuestion >= $scope.totalQuestion -1){
                if($scope.resultAnswer == 1){
                    $scope.showContinue = false;
                    $scope.completeTest = true;
                    return;
                }
            }
        })
    }

    $scope.result = function($userid){
        $scope.subTopicName = $stateParams.slug;
        $scope.score = $stateParams.id;
        $scope.subtopicid = $stateParams.subtopicid;
        $scope.topicid = $stateParams.topicid;
        $scope.questionid = $stateParams.questionid;
        $http({
            method: 'POST',
            url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_subtopic_quiz_progression&userid='+$userid+'&subtopicid='+$scope.questionid,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            $scope.resultData = response.data.data;
            console.log($scope.resultData);
        })
    }
    $scope.backtoSubtopic = function(){
        window.location.href="/student/#/"+$scope.topicid+"/subtopic/"+$scope.subtopicid;
    }
    $scope.continueTest = function(){
        window.location.href="/student/#/test-question/"+$scope.topicid+"/"+$scope.subtopicid+"/"+$scope.subTopicName+"/"+$scope.questionid;
    }

    function updateItemQuestionList(questionitem){
        for(var i = 0; i < $scope.allQuestionList.length; i++){
            if(questionitem.id == $scope.allQuestionList[i].id){
                $scope.allQuestionList[i] = questionitem;
                return;
            }
        }
    }
    // $scope.reloadQuestion = function(){
    //     location.reload();
    // }

}]);
