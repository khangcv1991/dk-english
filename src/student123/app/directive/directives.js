angular.module('app.directives', [])
.directive('loading', ['$http', function ($http) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			scope.isLoading = function () {
				return $http.pendingRequests.length > 0;
			};
			scope.$watch(scope.isLoading, function (value) {
				if (value) {
					element.removeClass('ng-hide');
				} else {
					element.addClass('ng-hide');
				}

			});
		}
	};
}])

.directive('checkduplicatedata', ['$http', function ($http) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			$(element).each(function(){
				console.log($(element).lenth);
				console.log($(element).attr('id'));
			});
		}
	};
}])
