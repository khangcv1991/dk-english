(function () {
    'use strict';
    angular.module('app')
    .factory('authenticationService', function($http) {
        return {
            isLogged: function() {
                return $http.get('checkLogin.php')
                .then(function(response){
                    return response;
                });
            }
        };
    })
})();