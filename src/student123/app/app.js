			(function () {
				'use strict';	
				angular.module('app',[
					'oc.lazyLoad',
					'ui.router',
					'ui.router.state.events',
					'app.directives',
					'ngSanitize',
					])
				.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
					$stateProvider
					.state("about-us", {
						url:"/about-us",
						templateUrl: "views/aboutus.php",
						controller: "AboutUsController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/AboutUsController.js"])
							}]
						},
						authenticate: false
					})
					.state("terms-and-conditions", {
						url:"/terms-and-conditions",
						templateUrl: "views/terms_and_conditions.php",
						controller: "T&CsController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/T&CsController.js"])
							}]
						},
						authenticate: false
					})
					.state("privacy-policy", {
						url:"/privacy-policy",
						templateUrl: "views/privacy_policy.php",
						controller: "PrivacyPolicyController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/PrivacyPolicyController.js"])
							}]
						},
						authenticate: false
					})
					.state("faqs", {
						url:"/faqs",
						templateUrl: "views/faqs.php",
						controller: "FaqsController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/FaqsController.js"])
							}]
						},
						authenticate: false
					})
					.state("student-view", {
						url:"/student-view",
						templateUrl: "views/student_view.php",
						controller: "StudentViewController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/StudentViewController.js"])
							}]
						},
						authenticate: true
					})
					.state("choose-new-subject", {
						url:"/choose-new-subject",
						templateUrl: "views/choose_new_subject.php",
						controller: "StudentViewController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/StudentViewController.js"])
							}]
						},
						authenticate: true
					})
					.state("topic", {
						url:"/topic/:id",
						params: {
							id: { type: 'string', raw: true }
						},
						templateUrl: "views/topic.php",
						controller: "StudentViewController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/StudentViewController.js"])
							}],
						},
						authenticate: true
					})
					.state("subtopic", {
						url:"/:topicid/subtopic/:id",
						params: {
							id: { type: 'string', raw: true },
							topicid: { type: 'string', raw: true }
						},
						templateUrl: "views/subtopic.php",
						controller: "StudentViewController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/StudentViewController.js"])
							}]
						},
						authenticate: true
					})
					.state("book", {
						url:"/book/:id/:pageno",
						params: {
							id: { type: 'string', raw: true },
							pageno:{ type: 'string', raw: true }
						},
						templateUrl: "views/detail.php",
						controller: "StudentBookController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/StudentBookController.js"])
							}]
						},
						authenticate: true
					})
					.state("test-question", {
						url:"/test-question/:topicid/:subtopicid/:slug/:id",
						params: {
							subtopicid: { type: 'string', raw: true },
							topicid: { type: 'string', raw: true },
							id: { type: 'string', raw: true },
							slug: { type: 'string', raw: true }

						},
						templateUrl: "views/test_question.php",
						controller: "StudentQuestionController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/StudentQuestionController.js"])
							}]
						},
						authenticate: true
					})
					.state("test-result", {
						url:"/test-result/:topicid/:subtopicid/:slug/:questionid/result",
						params: {
							subtopicid: { type: 'string', raw: true },
							topicid: { type: 'string', raw: true },
							questionid: { type: 'string', raw: true },
							slug: { type: 'string', raw: true }
						},
						templateUrl: "views/test_result.php",
						controller: "StudentQuestionController",
						resolve:{
							loadAsset: ["$ocLazyLoad", function($ocLazyLoad){
								return $ocLazyLoad.load(["app/controller/StudentQuestionController.js"])
							}]
						},
						authenticate: true
					});
					$urlRouterProvider.otherwise("/student-view");
					$locationProvider.hashPrefix("");

				}])
				.run(['$rootScope', '$state', '$location', 'authenticationService', function($rootScope, $state, $location, authenticationService) {
					$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
						authenticationService.isLogged().then(function(response){
							var $isLogged = response.data;
							if($isLogged){
								var checkAuthen = true;
							}else{
								var checkAuthen = false;
							}
							console.log(checkAuthen);
							console.log(toState.authenticate);
						if(toState.authenticate && !checkAuthen){
							event.preventDefault();
							//$location.path('/');
							window.location.href = '/';
							console.log('abc');
						}
						});
					});
				}])
			})();
			//var url = "http://localhost:8090/student/";
			$(window).on('load', function (){
				$('.se-pre-con').hide();
				console.log('abc');
				// new PerfectScrollbar('.wrapper-page');
				// $('.wrapper-page').css('height', ($(window).height()))
			});
