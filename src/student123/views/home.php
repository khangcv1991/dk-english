<div class="home-page">
<div class="container">
        <div class="row">
            <div class="col-md-7 padding-right-0">
                        <div id="page-content">
                            <h1 class="logo">
                                <a href="/"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
                            </h1>
                            <h2>Welcome to Learnlock</h2>
                            <p>
                                A revision site to help you learn quickly and achieve exam success.
                            </p>
                            <p>
                                Our revision content is specific to the subject you are revising for, the level you are at and the exam boards you are taking your exams with.
                            </p>
                            <p>
                                Each topic is divided into sub topics and after you revision you then take tests to measure how much you have learnt. We provide a unique system that means you will continually be tested until you get the right answers and therefore you know the learning is locked in.
                            </p>
                            <p>
                                Each user will have their own dashboard to see how far through the topic they are in both the revision and the tests.
                            </p>
                            <!-- <ul>
                                <li>
                                    <a><img src="../student/assets/img/navi-icon.png"> <span>Aliquam nec tortor dictum, luctus quam a. </span></a>
                                </li>
                                <li>
                                    <a><img src="../student/assets/img/navi-icon.png"> <span>Posuere nisi. Integer hendrerit bibendum bibendum. </span></a>
                                </li>
                                <li>
                                    <a><img src="../student/assets/img/navi-icon.png"> <span>Phasellus id elit in neque ultrices eleifend non at lorem. </span></a>
                                </li>
                            </ul>
                            <p>Proin tincidunt euismod metus non fermentum. Aliquam congue mi lobortis, maximus lectus eu, lobortis metus. Mauris est purus, sagittis in luctus in, accumsan vel dolor. Suspendisse lacus eros, convallis eu lacus et, tempus vestibulum elit.</p>
                             -->
                            <div class="row submit-form">
                                <div class="col-md-5">
                                    <div class="loginBtn">
                                        <a href="/login">
                                            Login
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="linkSignupBtn">
                                        <a href="/login/signup.php?">
                                            Register
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <ul class="menu-links">
                                <li><a href="/student/#/about-us">About us</a></li>
                                <li class="hyphen-link">-</li>
                                <li><a href="/student/#/faqs">FAQs</a></li>
                            </ul>
                </div>
            </div>
        </div>
    </div>