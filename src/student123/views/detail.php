<?php
include( '../../config.php' ); 
    global $USER;
    // print_r($USER->id);
    $sesskey = $USER->sesskey;
?>
<style>
    .wrapper-page .wrapper-footer{
        position:initial;
    }
</style>
<div class="student-page" ng-init="getBookBySubtopic(<?php echo $USER->id ?>)">
    <div id="page-content">
        <div class="row logo-row">
            <div class="col-md-12">
                <h1 class="logo">
                    <a href="/student/#/student-view" class="logoUrl"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#logoutModal" class="btn btn-primary logout"><span>Logout</span></a>
                </h1>
                <h2 class="breadcrumbs"><a href="javascript:void(0)" ng-click="backToSubTopic()" ><i class="fa fa-angle-left"></i></a>{{contentCover.subtopic}}</h2>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Report issue -->
        <div class="modal fade" id="reportIssueModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Report</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="issueTitle">Title</label>
                            <input type="text" class="form-control" id="issueTitle" aria-describedby="issueTitle" placeholder="Title..." ng-model="issueTitle">
                        </div>
                        <div class="form-group">
                            <label for="issueContent">Content</label>
                            <input type="text" class="form-control" id="issueContent" aria-describedby="issueContent" placeholder="Content..." ng-model="issueContent">
                        </div>
                        <button type="button" class="btn btn-warning" style="background: #ffd306;border:1px solid #ffd306;" data-dismiss="modal" ng-click="reportIssueSubmit()">Send</button>
                    </div>
                </div>
            </div>
        </div>
            <button class="btn btn-warning" style="background: #ffd306;border:1px solid #ffd306;" data-toggle="modal" data-target="#reportIssueModal">Report issue</button>
            <div class="student-detail">
            <div id="loading" style="display: none;text-align: center;background: #fff;padding: 10px;width: 150px;border-radius: 5px;margin: 0 auto;box-shadow: 0px 0px 3px #ccc;"><i class="fa fa-cog"></i> Processing...</div>
                <div class="main-student-content">
                    <h1 class="title">{{contentCover.title}}</h1>
                    <div class="row">
                        <div class="student-pagination col-md-3 pull-right">
                            <div class="previous-student-pagination" ng-show="previousPageVisible">
                                <button type="button" class="btn btn-primary pull-right" ng-click="previousPage(<?php echo $USER->id ?>,contentCover.totalpages)">Previous Page</button>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div ng-bind-html="contentCover.content"></div>
                </div>
                <div class="student-pagination">
                    <div class="row">
                        <div class="col-md-3">
                                <button type="button" onclick="window.history.go(-1); return false;"  class="btn btn-secondary">Back To Topics</button>
                        </div>
                        <div class="col-md-6">
                                <p>Page {{currentpage}} of {{contentCover.totalpages}}</p>
                        </div>
                        <div class="col-md-3" ng-show="nextPageVisible">
                                <button type="button" class="btn btn-primary pull-right" ng-click="nextPage(<?php echo $USER->id ?>,contentCover.totalpages)">Next Page</button>
                        </div>
                        <div class="col-md-3" ng-show="quizTestPage">
                                <button type="button" class="btn btn-primary pull-right" ng-click="quizTestButton()">Start The Exam</button>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
