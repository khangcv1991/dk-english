<?php
include( '../../config.php' ); 
    global $USER;
    // print_r($USER->id);
    $sesskey = $USER->sesskey;
?>
<div class="student-page student-dashboard">
    <div id="page-content">
        <div class="row logo-row">
            <div class="col-md-12">
                <h1 class="logo">
                    <a class="logoUrl"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
                    <a href="javascript:void()"  data-toggle="modal" data-target="#logoutModal" class="btn btn-primary logout"><span>Logout</span></a>
                </h1>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="current-subjects current-subject-student" ng-init="getcurrentSubjectData(<?php echo $USER->id ?>)">
        <div class="col-md-4 col-sm-6">
                <div class="subject-box subject-add-more">
                    <a href="/student/#/choose-new-subject">
                        <span class="fa fa-plus"></span>
                        <p>New Subject</p>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 visible_{{currentSubject.visible}}" ng-repeat="currentSubject in currentSubjects">
                <div class="subject-box">
                    <span class="fa fa-close" ng-click="deleteCurrentSubjectData(<?php echo $USER->id ?>, currentSubject.id)"></span>
                        <h2>
                            <a href="#/topic/{{currentSubject.id}}">
                                {{currentSubject.name}}
                            </a>
                        </h2>
                        <div class="subject-meta-box subject-content-covered">
                            <h3>Content Covered <span>{{currentSubject.bookstatus*100 | currency:' ':0}}%</span></h3>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                aria-valuemin="0" aria-valuemax="100" style="width:{{currentSubject.bookstatus*100 | currency:' ':0}}&#37">
                                </div>
                            </div>
                        </div>
                        <div class="subject-meta-box subject-content-locked">
                            <h3>Content Locked <span>{{currentSubject.quizstatus*100 | currency:' ':0}}%</span></h3>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100" style="width:{{currentSubject.quizstatus*100 | currency:' ':0}}&#37">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>