<?php
include( '../../config.php' ); 
    global $USER;
    // print_r($USER->id);
    $sesskey = $USER->sesskey;
?>
<div class="student-page" ng-init="result(<?php echo $USER->id ?>)">
    <div id="page-content">
        <div class="row logo-row">
            <div class="col-md-12">
                <h1 class="logo">
                    <a href="/student/#/student-view">
                        <img src="../student/assets/img/logo.svg" title="logo" alt="logo">
                    </a>
                    <a href="javascript:void()"  data-toggle="modal" data-target="#logoutModal" class="btn btn-primary logout"><span>Logout</span></a>
                </h1>
                <h2 class="breadcrumbs">{{subTopicName}}</h2>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
            <div class="student-test test-result">
                <div class="main-student-content">
                    <div class="main-student-question">
                        <p>Congratulations!  You have now locked</p>
                        <h2>{{resultData *100 | currency:' ':0}}%</h2>
                        <p>into your brain!</p>
                    </div>
                </div>
                <div class="student-pagination">
                    <div class="col-md-12">
                        <div class="student-pagination-box">
                            <button type="button" ng-click="backtoSubtopic()" class="btn btn-secondary save">Back to subtopics</button>
                            <button type="button" ng-if="resultData != 1" ng-click="continueTest()" class="btn btn-primary complete">Continue test</button>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
