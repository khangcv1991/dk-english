<div class="about-page">
        <h1 class="logo">
            <a href="/"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
        </h1>
        <div class="row page-content-box">
            <div class="col-md-7 col-sm-7 padding-right-0">
                        <div id="page-content">
                            <h2>Privacy notice</h2>
                            <p><b>Introduction</b></p>
                            <p>
                                The term ‘Learn Lock’ or ‘us’ or ‘we’ refers to the owner of the website.  The term ‘you’ refers to the user or viewer of our website.     
                            </p>
                            <p>
                                At Learn Lock, we respect the privacy of our users and are committed to protecting your personal data. This policy sets out how we will treat your personal information through the use of this website including any data provided when you register on this website. We recommend that you read this notice so you are aware how and why we use and process your data 
                            </p>
                            <p>
                                This privacy notice supplements our terms and conditions.
                            </p>
                            <p><b>Who we are?</b></p>
                            <p>
                                Learn Lock is the data controller and responsible for your personal data. If you have any questions about this notice, please contact us at the email address at the end of this document
                            </p>
                            <p>
                                <b>What information do we collect?</b>
                            </p>
                            <p>Your personal data is collected in the following ways:</p>
                            <ul>
                                <li><p>Directly - This includes information you provide when you register on this website and use our products and services</p></li>
                                <li><p>Automated - This includes technical data about your usage, browsing activities and patterns and equipment. </p></li>
                                <li><p>Third parties - This includes identity, profile data and contact information from facebook </p></li>
                            </ul>
                            <p>We may collect, store and use the following kinds of personal data about yourself and your subject teacher(s). This includes (by the way of a non exhaustive list):</p>
                            <ul>
                                <li><p>Identity data - User name, first name and last name</p></li>
                                <li><p>Contact data - Including email address and phone number</p></li>
                                <li><p>Technical data - information about your visits to and use of this website, geographical location, browser type and version, operating system, referral source, length of visit, page views, website navigation, internet protocol ("IP") address, your login data, time zone setting and location, browser plug-in types and versions, operating system and platform and other technology on the devices you use to access this website.</p></li>
                                <li><p>Usage Data -  Includes information about how you use our website any other services or products.</p></li>
                                <li><p>Marketing and Communications Data - Includes your preferences in receiving marketing from us and other third parties and your communication preferences.</p></li>
                                <li><p>Aggregated Data - We may collect, use and share aggregated data including statistical or demographic data for any purpose. Aggregated Data may be obtained from your personal data but is not considered personal data in law as this data does not directly or indirectly reveal your identity.</p></li>
                                <li><p>Other data - The name of the school you attend</p></li>
                            </ul>
                            <p><b>How do we use personal information?</b></p>
                            <p>Personal information submitted to us via this website will be used for the purposes specified in this privacy notice or in relevant parts of the website:</p>
                            <ul>
                                <li><p>To register you on this website</p></li>
                                <li><p>To allow you to use and access this website and our services</p></li>
                                <li><p>To send you general non-marketing communications</p></li>
                                <li><p>To gain feedback on our website and services</p></li>
                                <li><p>To communicate with you in order to provide you with services or information about our website and products</p></li>
                                <li><p>To respond to your questions or comments.</p></li>
                                <li><p>To inform you about changes in our services and important service related notices</p></li>
                                <li><p>If you explicitly opt-in to our direct marketing on registration we will send you marketing material. If you have did not explicitly opt-in you will not receive such communication.</p></li>
                                <li><p>We may use your information to comply with applicable legal or regulatory obligations, including informal requests from law enforcement or other governmental authorities.</p></li>
                                <li><p>To deal with enquiries or complaints about the website</p></li>
                            </ul>
                            <p><b>What legal basis do we have for processing your personal data?</b></p>
                            <p>We have the legal grounds to process your data on the grounds of consent, contract and legitimate interest. Your consent is given by creating an account on this website.</p>
                            <p><b>When do we share personal data?</b></p>
                            <p>We shall not sell, transfer or lease out your personal data to third parties.</p>
                            <p>We do share your personal information only in limited cases in order to facilitate our service via marketing</p>
                            <p><b>Where do we store and process personal data and how do we secure it?</b></p>
                            <p>We will ensure the data is processed in line with our privacy policy and in line with UK laws. We have in place measures to ensure your data is not lost, used or accessed in an unauthorised way. We limit access to employees, agents and contractors and other third parties who have a need to know.</p>
                            <p>When you set up your account your account is protected by a password of your choosing for your privacy and security. We highly recommend you prevent unauthorized access to your account and personal information by not disclosing or sharing your password and limiting access to your computer or device and browser by signing off after you have finished accessing your account We endeavor to protect the privacy of your account and other personal information we hold in our records, but we cannot guarantee complete security.</p>
                            <p><b>How long do we keep your personal data for?</b></p>
                            <p>We will only retain your personal data for as long as necessary to fulfil the purposes we collected it for, which includes satisfying any legal, accounting, or reporting requirements.</p>
                            <p>We consider the amount, nature and sensitivity of the personal data. </p>
                            <p>In some circumstances you can ask us to delete your data: see Your legal rights below for further information.<p>
                            <p>In some circumstances we may anonymise your personal data (so that it can no longer be associated with you) for research or statistical purposes in which case we may use this information indefinitely without further notice to you.</p>
                            <p><b>Your rights in relation to personal data</b></p>
                            <p>In all above cases when we collect and store your personal information, you have the following rights:</p>
                            <ul>
                                <li><p>Request access to your personal data.</p></li>
                                <li><p>Request correction of your personal data.</p></li>
                                <li><p>Request erasure of your personal data. Please note there may be circumstances where you ask us to erase your Personal Data but we are legally entitled to retain it</p></li>
                                <li><p>Object to processing of your personal data.</p></li>
                                <li><p>Request restriction of processing your personal data.</p></li>
                                <li><p>Request transfer of your personal data.</p></li>
                                <li><p>Right to withdraw consent.</p></li>
                            </ul>
                            <p><b>How to contact us?</b></p>
                            <p>Learn Lock is owned by Luke Jackson who can be contacted at <a href="mailto:luke@teacherclub.co.uk">luke@teacherclub.co.uk</a></p>
                            <div class="row submit-form">
                                <div class="col-md-5">
                                    <div class="go-back margin-top-10">
                                        <a href="/">Go Back</a>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
</div>