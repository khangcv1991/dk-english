<?php
include( '../../config.php' ); 
    global $USER;
    // print_r($USER->id);
    $sesskey = $USER->sesskey;
?>
<div class="student-page">
    <div id="page-content">
        <div class="row logo-row">
            <div class="col-md-12">
                <h1 class="logo">
                    <a href="/student/#/student-view" class="logoUrl"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
                    <a href="javascript:void()"  data-toggle="modal" data-target="#logoutModal" class="btn btn-primary logout"><span>Logout</span></a>
                </h1>
                <h2 class="breadcrumbs"><a style="cursor: pointer;" ng-click="backToTopic()" ><i class="fa fa-angle-left"></i></a>{{subtopics[0].topic}}</h2>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="current-subjects" ng-init="getSubtopicByTopic(<?php echo $USER->id?>)">
            <div class="col-md-4 col-sm-4 col-xs-12" ng-repeat="subtopic in subtopics">
            <div class="subject-box">
                        <span class="subtopic-status-lock status-lock">
                            <img style="width:27px; height:24px;" ng-if="subtopic.quizstatus >= 0 && subtopic.quizstatus < 0.25" ng-src="../student/assets/img/0.png" alt="">
                            <img ng-if="subtopic.quizstatus >= 0.25 && subtopic.quizstatus < 0.5" ng-src="../student/assets/img/25.png" alt="">
                            <img ng-if="subtopic.quizstatus >= 0.5 && subtopic.quizstatus < 0.75" ng-src="../student/assets/img/50.png" alt="">
                            <img ng-if="subtopic.quizstatus >= 0.75 && 0.9 >= subtopic.quizstatus" ng-src="../student/assets/img/75.png" alt="">
                            <img ng-if="subtopic.quizstatus == 1" ng-src="../student/assets/img/100.png" alt="">
                        </span>
                        <h2>
                            <a href="#/book/{{subtopic.id}}/{{subtopic.readedpage}}">
                                {{subtopic.fullname}}
                            </a>
                        </h2>
                        <div class="subject-meta-box subject-content-covered">
                            <h3 ng-class="{stopAction: subtopic.bookstatus*100 == '100'}">
                                <span class="b13-tooltip">Click on content covered to revise topic</span>
                                <a href="#/book/{{subtopic.id}}/{{subtopic.readedpage}}">
                                    Content Covered <span> {{subtopic.bookstatus * 100 | currency:' ':0}}&#37;</span>
                                </a>
                            </h3>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                aria-valuemin="0" aria-valuemax="0" style="width:{{subtopic.bookstatus * 100 | currency:' ':0}}&#37">
                                </div>
                            </div>
                        </div>
                        <div class="subject-meta-box subject-content-locked">
                            <h3 ng-class="{stopAction: subtopic.quizstatus == '1'}">
                                <span class="b13-tooltip">Click on Content Locked to take topic test</span>
                                <a href="#/test-question/{{subjectId}}/{{topicId}}/{{subtopic.fullname}}/{{subtopic.id}}">
                                    Content Locked <span>{{subtopic.quizstatus * 100 | currency:' ':0}}&#37;</span>
                                </a>
                            </h3>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                aria-valuemin="0" aria-valuemax="0" style="width:{{subtopic.quizstatus * 100 | currency:' ':0}}&#37">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>