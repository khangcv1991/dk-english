<div class="about-page">
        <h1 class="logo">
                <a href="/"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
        </h1>
        <div class="row page-content-box">
            <div class="col-md-7 col-sm-7 padding-right-0">
                        <div id="page-content">
                            <h2>Website usage terms and conditions </h2>
                            <p>
                                Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy governs Learn Lock’s relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.
                            </p>
                            <p>
                                The term ‘Learn Lock’ or ‘us’ or ‘we’ refers to the owner of the website. The term ‘you’ refers to the user or viewer of our website.
                            </p>
                            <p>
                                The use of this website is subject to the following terms of use:
                            </p>
                            <br>
                            <p> <b>General</b> </p>
                            <ul>
                                <li>
                                        <p>The content of the pages of this website is for your general information and use only. It is subject to change without notice. </p>
                                </li>
                                <li>
                                        <p>This website uses cookies to monitor browsing preferences. </p>
                                </li>
                                <li>
                                        <p>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>
                                </li>
                                <li>
                                        <p>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</p>
                                </li>
                                <li>
                                        <p>You may for your own personal, non commercial use, retrieve, display and view the content on a computer screen</p>
                                </li>
                                <li>
                                        <p>You may not use this website to make, transmit or for storing electronic or other copies of the content of the site without permission of the owner</p>
                                </li>
                                <li>
                                        <p>This website contains material which is owned by or licensed to us. Content means any text, graphics, images, audio, video, software, data compilations, page layout, underlying code and software and any other information capable of being stored in a computer that appears on or forms part of this website, including any content uploaded by users. By continuing to use this website you acknowledge that such content is protected by copyright, trademarks, database rights and other intellectual property rights. Nothing on this site shall be construed as granting, by implication, estoppel, or otherwise any licence or right to use any trademark, logo or service mark displayed on this site without the owners written prior permission. </p>
                                </li>
                                <li>
                                        <p>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website. </p>
                                </li>
                                <li>
                                        <p>Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</p>
                                </li>
                                <li>
                                        <p>From time to time, this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</p>
                                </li>
                                <li>
                                        <p>Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Northern Ireland, Scotland and Wales.</p>
                                </li>
                                <li>
                                        <p>These terms may be varied from time to time. Any revised terms will apply to the website from the date of publication. Users should check the terms and conditions on a regular basis to ensure familiarity with the current version.</p>
                                </li>
                            </ul>
                            <p><b>Registration</b></p>
                            <ul>
                                <li>
                                        <p>You must ensure the details provided by you on registration or at any other time are correct and complete </p>
                                </li>
                                <li>
                                        <p>You must inform us immediately of any changes to your information by updating your personal details to ensure we can communicate with you effectively </p>
                                </li>
                                <li>
                                        <p>We may suspend or cancel your registration at any time without notice if you breach these terms and conditions</p>
                                </li>
                                <li>
                                        <p>You may cancel your registration at any time by informing us via the email address at the end of this document. If you do so, you must immediately stop using the website. Cancellation or suspension does not affect your statutory rights.</p>
                                </li>
                                <li>
                                        <p>When you register on this website you will be asked to create a password which should be kept confidential and not disclosed to or shared with anyone else</p>
                                </li>
                                <li>
                                        <p>If we believe there is, or likely to be, misuse of the website or breach of security, we may require you to change your password or suspend your account</p>
                                </li>
                            </ul>
                            <p><b>Learn Lock is owned by Luke Jackson who can be contacted at <a href="mailto:luke@teacherclub.co.uk">luke@teacherclub.co.uk</a></b></p>
                            <div class="row submit-form">
                                <div class="col-md-5">
                                    <div class="go-back margin-top-10">
                                        <a href="/">Go Back</a>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
</div>
<style>
    .wrapper-page .wrapper-footer {
        position: initial;
        margin-top: 15px;
    }
</style>