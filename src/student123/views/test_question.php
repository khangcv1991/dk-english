<?php
include( '../../config.php' ); 
    global $USER;
    // print_r($USER->id);
    $sesskey = $USER->sesskey;
?>

<div class="student-page"  ng-init="listQuestionBySutopic(<?php echo $USER->id ?>)">
    <div id="page-content">
        <div class="row logo-row">
            <div class="col-md-12">
                <h1 class="logo">
                    <a href="/student/#/student-view">
                        <img src="../student/assets/img/logo.svg" title="logo" alt="logo">
                    </a>
                    <a href="javascript:void()"  data-toggle="modal" data-target="#logoutModal" class="btn btn-primary logout"><span>Logout</span></a>
                </h1>
                <h2 class="breadcrumbs" ng-repeat="question in detailQuestion">{{subTopicName}}</h2>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
            <div class="student-test">
                <div class="main-student-content">
                        <!-- Preloader start -->
                        <!-- <div class="question-loading" ng-show="showloading"></div> -->
                        <!-- Preloader End -->
                    <div class="main-student-question" ng-repeat="question in detailQuestion" ng-class="{active: resultAnswer == 1}">
                        <p style="font-weight:bold; text-align:left;" ng-bind-html="question.question.generalfeedback"></p>
                        <h3><span ng-bind-html="question.question.questiontext"></span></h3>
                        <div class="form-group" ng-if="question.question.qtype == 'truefalse'">
                            <div class="checkbox checkboxOptions" ng-repeat="result in question.answers">
                                <label>
                                    <input type="checkbox" ng-model="result.checked" value="{{result.id}}">
                                    <!-- <input type="checkbox" class="cb" onchange="cbChange(this)" ng-model="result.checked" value="{{result.id}}" ng-if="allQuestionList.qtype == 'truefalse'" > -->
                                    <span ng-bind-html="result.answer"></span>
                                    <i class="fa fa-close" ng-if="isChecked(result.id) && resultAnswer == 0"></i>
                                    <i class="fa fa-check" ng-if="isChecked(result.id) && resultAnswer == 1"></i>
                                </label>
                            </div>
                            <!-- 
                            <div class="checkbox checkboxOptions">
                                <label><input type="checkbox" value="">Hastings <i class="fa fa-check"></i></label>
                            </div> -->
                        </div>
                        <div class="form-group" ng-if="question.question.qtype == 'multichoice' && (enableTypingQuestion == false)">
                            <div class="checkbox checkboxOptions" ng-repeat="result in question.answers">
                                <label for="chkQuestion_{{result.id}}">
                                    <input type="checkbox" ng-click="CheckBox_Checked($index, question.answers)" ng-model="result.checked" value="{{result.id}}" id="chkQuestion_{{result.id}}">
                                    <!-- <input type="checkbox" class="cb" onchange="cbChange(this)" ng-model="result.checked" value="{{result.id}}" ng-if="allQuestionList.qtype == 'truefalse'" > -->
                                    <span ng-bind-html="result.answer"></span>
                                    <i class="fa fa-close" ng-if="answerArray.indexOf(result.id) === -1 && resultAnswer == 0"></i>
                                    <i class="fa fa-check" ng-if="answerArray.indexOf(result.id) !== -1 && resultAnswer == 1"></i>
                                </label>
                            </div>
                            <!-- 
                            <div class="checkbox checkboxOptions">
                                <label><input type="checkbox" value="">Hastings <i class="fa fa-check"></i></label>
                            </div> -->
                        </div>

                        <div class="form-group" ng-if="question.question.qtype == 'multichoice' && (enableTypingQuestion == true)">
                            <div class="checkbox checkboxOptions" >
                                <label>
                                    <input type="text" autocomplete="off" id="typedAnswer" placeholder="Type your answer...">
                                    <i class="fa fa-close" ng-if="resultAnswer == 0"></i>
                                    <i class="fa fa-check" ng-if="resultAnswer == 1"></i>
                                </label>
                            </div>
                            <!-- 
                            <div class="checkbox checkboxOptions">
                                <label><input type="checkbox" value="">Hastings <i class="fa fa-check"></i></label>
                            </div> -->
                        </div>

                        <div class="form-group shortanswer-type" ng-if="question.question.qtype == 'shortanswer'">
                            <div class="checkbox checkboxOptions">
                                <label>
                                    <input type="text" ng-model="shortAnswer" placeholder="Type your answer...">
                                    <i class="fa fa-close" ng-if="resultAnswer == 0"></i>
                                    <i class="fa fa-check" ng-if="resultAnswer == 1"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="feedback" ng-if="enableFeedback" style="margin-bottom:10px;" > <span ng-bind-html="feedbackcontent" style="color:green;"></span></div>
                </div>
                <div class="student-pagination" ng-repeat="question in detailQuestion">
                    <div class="col-md-12">
                        <div class="student-pagination-box">
                            <button type="button" class="btn btn-secondary save" ng-if="!completeTest" ng-click="saveProgression()">Save Progress & Exit</button>
                            <button type="button" class="btn btn-primary complete" ng-if="previousQuestion == true">Previous</button>
                            <button type="button" class="btn btn-primary complete hideSubmitAnswer" ng-show="hideSubmitAnswer" ng-click="submitQuestion(question.question.id, question.answers, question.question.qtype)">Submit Answer</button>
                            <!--<button type="button" class="btn btn-primary complete disabled" ng-disabled="submit"  ng-if="resultAnswer != 1" ng-hide="disableQuestion">Next</button>-->
                            <button type="button" class="btn btn-primary complete" ng-show="showContinue" ng-click="nextQuestion()">Continue</button>
                            <button type="button" class="btn btn-primary complete" ng-if="resultAnswer == 1 && completeTest" ng-click="completeTestQuestion()" style="margin-left:0px;">Complete Test</button>
                        </div>
                        <!-- <div class="student-pagination-box" ng-if="enableFeedback">
                            <button type="button" class="btn btn-secondary complete" style="color:#fff" ng-click="reloadQuestion()">Ok</button>
                        </div> -->
                    </div>
                </div>
        </div>
    </div>
</div>
