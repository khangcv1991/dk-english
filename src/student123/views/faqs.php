<div class="faqs-page">
        <h1 class="logo">
            <a href="/"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
        </h1>
        <div class="row page-content-box">
            <div class="col-md-7 col-sm-7 padding-right-0">
                        <div id="page-content">
                            <h2>FAQs </h2>
                            <!-- <p>
                                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                            </p> -->
                            <div class="toggleFaqsBox">
                                <ul class="toggleFaqs">
                                    <li>
                                        <a href="javascript:void(0)" ng-click="booleanToggle = !booleanToggle">
                                            How do I access the website?<i ng-show="!booleanToggle" class="fa fa-angle-down"></i> <i ng-show="booleanToggle" class="fa fa-angle-up"></i>
                                            <div class="contentToggle animate-show" ng-show="booleanToggle">
                                                <h3>You will need to create an account which will give you access to the site</h3>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" ng-click="booleanToggle1 = !booleanToggle1">
                                            What do I do if I am not revising for all the subjects listed? <i ng-show="!booleanToggle1" class="fa fa-angle-down"></i> <i ng-show="booleanToggle1" class="fa fa-angle-up"></i>
                                            <div class="contentToggle animate-show" ng-show="booleanToggle1">
                                                <h3>You can create your own dashboard by selecting only subjects are you studying for and at the appropriate level and exam board</h3>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" ng-click="booleanToggle2 = !booleanToggle2">
                                            What do I do if I start a test but don’t have time to finish it?<i ng-show="!booleanToggle2" class="fa fa-angle-down"></i> <i ng-show="booleanToggle2" class="fa fa-angle-up"></i>
                                            <div class="contentToggle animate-show" ng-show="booleanToggle2">
                                                <h3>You have the option to save your progress so far and come back to complete it at another time </h3>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" ng-click="booleanToggle3 = !booleanToggle3">
                                            How long will it take me to revise each module and complete the tests?<i ng-show="!booleanToggle3" class="fa fa-angle-down"></i> <i ng-show="booleanToggle3" class="fa fa-angle-up"></i>
                                            <div class="contentToggle animate-show" ng-show="booleanToggle3">
                                                <h3>This depends on how fast you are learning and retaining the information. There is no set amount of time you will have access to the site for</h3>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" ng-click="booleanToggle4 = !booleanToggle4">
                                            What do I do if I have forgotten my password?<i ng-show="!booleanToggle4" class="fa fa-angle-down"></i> <i ng-show="booleanToggle4" class="fa fa-angle-up"></i>
                                            <div class="contentToggle animate-show" ng-show="booleanToggle4">
                                                <h3>Use the ‘reset password’ link</h3>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row submit-form">
                                <div class="col-md-5">
                                    <div class="go-back margin-top-10">
                                        <a href="/">Go Back</a>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
</div>

