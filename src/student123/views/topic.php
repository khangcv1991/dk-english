<?php
include( '../../config.php' ); 
    global $USER;
    $sesskey = $USER->sesskey;
?>
<div class="student-page" ng-init="getTopicBySubject(<?php echo  $USER->id; ?>)">
    <div id="page-content">
        <div class="row logo-row">
            <div class="col-md-12">
                <h1 class="logo">
                    <a href="/student/#/student-view" class="logoUrl"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
                    <a href="javascript:void()"  data-toggle="modal" data-target="#logoutModal" class="btn btn-primary logout"><span>Logout</span></a>
                </h1>
                <h2 class="breadcrumbs"><a href="#/"><i class="fa fa-angle-left"></i></a>{{topics[0].subject}}<a href="javascript:void(0)" class="change-subject-option" data-toggle="modal" data-target="#currentSubject" ng-click="detail_subject_option(<?php echo $USER->id?>, topics[0].parent)"><span class="fa fa-cog"></span>Change Subject Options</a></h2>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="current-subjects">
            <div class="col-md-4 col-sm-4 col-xs-12 visible_{{topic.visible}}" ng-repeat="topic in topics">
                <div class="subject-box" >
                        <span class="subject-status-lock status-lock">
                            <img style="width:27px; height:24px;" ng-if="topic.topicquizstatus >= 0 && topic.topicquizstatus < 0.25" ng-src="../student/assets/img/0.png" alt="">
                            <img ng-if="topic.topicquizstatus >= 0.25 && topic.topicquizstatus < 0.5" ng-src="../student/assets/img/25.png" alt="">
                            <img ng-if="topic.topicquizstatus >= 0.5 && topic.topicquizstatus < 0.75" ng-src="../student/assets/img/50.png" alt="">
                            <img ng-if="topic.topicquizstatus >= 0.75 && 0.9 >= topic.topicquizstatus" ng-src="../student/assets/img/75.png" alt="">
                            <img ng-if="topic.topicquizstatus == 1" ng-src="../student/assets/img/100.png" alt="">
                        </span>
                        <h2>
                            <a href="#/{{topic.subjectid}}/subtopic/{{topic.id}}" ng-click="getBookBySubtopic(<?php echo  $USER->id; ?>,topic.id)">
                                {{topic.name}}
                            </a>
                        </h2>
                        <div class="subject-meta-box subject-content-covered">
                            <h3>
                                    Content Covered <span>{{topic.topicbookstatus*100 | currency:' ':0}}&#37;</span>
                            </h3>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                aria-valuemin="0" aria-valuemax="0" style="width:{{topic.topicbookstatus*100 | currency:' ':0}}&#37">
                                </div>
                            </div>
                        </div>
                        <div class="subject-meta-box subject-content-locked">
                            <h3>
                                    Content Locked <span>{{topic.topicquizstatus*100 | currency:' ':0}}&#37;</span>
                            </h3>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                aria-valuemin="0" aria-valuemax="0" style="width:{{topic.topicquizstatus*100 | currency:' ':0}}&#37">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="currentSubject" ng-init="detail_subject_option(<?php echo $USER->id?>, topics[0].parent)" tabindex="-1" role="dialog" aria-labelledby="currentSubjectLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{topics[0].subject}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="reloadPage(<?php echo $USER->id?>)">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="container-fluid" name="chooseNewSubject" novalidate>
            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Exam Board</label>
                </div>
                <div class="col-md-8">
                    <div class="form-group examboard-form">
                            <select class="form-control" name="examboard" id="examboard" data-ng-model="selectedExamboard" data-ng-options="examboard.title for examboard in selectedexamboardOptions" ng-change="hasChanged(selectedExamboard)"></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Choose Options</label>
                </div>
                <div class="col-md-8">
                    <div class="form-group checkboxSubjectOptionBox ng-hide" ng-if="!checkSubjectOption" style="margin-bottom: 20px;">
                        <div class="checkbox checkboxSubjectOption">
                            <label><input type="checkbox">Option Default</label>
                        </div>
                    </div>
                    <div class="form-group checkboxSubjectOptionBox" style="margin-bottom: 20px;" ng-if="checkSubjectOption">
                        <div class="checkbox checkboxSubjectOption" ng-repeat="subjectOption in selectedsubjectOptions">
                            <label><input type="checkbox" ng-model="subjectOption.selected" ng-true-value="1" ng-false-value="0" value="{{subjectOption.id}}">{{subjectOption.title}}</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Teachers Name</label>
                </div>
                <div class="col-md-8">
                        <div id="teacher-group" class="form-group">
                                <input type="text" name="teacher-name" ng-model="teachername" class="form-control checkMaxLenth" maxlength="50" name="title" placeholder="Teacher Name">
                        </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Teachers Email</label>
                </div>
                <div class="col-md-8">
                        <div id="teacher-group" class="form-group">
                            <input type="email" name="teacher-email" ng-model="teacheremail" class="teacher-email form-control checkMaxLenth" maxlength="50" name="title" placeholder="Teacher Email" required>
                        </div>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
         <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="reloadPage(<?php echo $USER->id?>)">Cancel</button>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6" ng-init="hasChanged()">
                    <button type="button" ng-click="updateStudentSelection(<?php echo $USER->id ?>,selsubject,selectedsubjectOptions,teachername,teacheremail)" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

