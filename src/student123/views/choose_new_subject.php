<?php
include( '../../config.php' ); 
    global $USER;
    // print_r($USER->id);
?>
<style>
    .wrapper-page{
        overflow:hidden;
    }
</style>
<div class="student-page choose-new-subject-page">
    <div id="page-content">
        <div class="row logo-row">
            <div class="col-md-12">
                <h1 class="logo">
                    <a href="/student/#/student-view" class="logoUrl"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#logoutModal" class="btn btn-primary logout"><span>Logout</span></a>
                </h1>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" ng-init="initiateCurrentSubjects(<?php echo $USER->id ?>)">
                <div class="col-md-4 col-sm-6 padding-0" ng-if="activeCurrentSubject">
                    <div class="current-subject-box">
                    <h2>Current Subjects</h2>
                    <div class="current-subjects">
                    <div class="col-md-12 padding-left-0" >
                    <div class="subject-box {{currentSubject.id}} visible_{{currentSubject.visible}}" ng-repeat="currentSubject in currentSubjects">
                    <span class="fa fa-close" ng-click="deleteCurrentSubjectData(<?php echo $USER->id ?>, currentSubject.id)"></span>
                            <h2>
                                <a href="#/topic/{{currentSubject.id}}">
                                    {{currentSubject.name}}
                                </a>
                            </h2>
                            <div class="subject-meta-box subject-content-covered">
                                <h3><a href="#/topic/{{currentSubject.id}}">Content Covered <span>{{currentSubject.bookstatus*100 | currency:' ':0}}%</span></a></h3>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                    aria-valuemin="0" aria-valuemax="100" style="width:{{currentSubject.bookstatus*100 | currency:' ':0}}&#37">
                                    </div>
                                </div>
                            </div>
                            <div class="subject-meta-box subject-content-locked">
                                <h3><a href="#/topic/{{currentSubject.id}}">Content Locked <span>{{currentSubject.quizstatus*100 | currency:' ':0}}%</span></a></h3>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                        aria-valuemin="0" aria-valuemax="100" style="width:{{currentSubject.quizstatus*100 | currency:' ':0}}&#37">
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6" ng-class="{'col-md-8': activeCurrentSubject == true, 'col-md-12': activeCurrentSubject != true}">
                <div class="title-subject">
                    <h2>Choose New Subject</h2>
                    <p>This is your subject dashboard. Please click below to add your GCSE subjects - you will be taken to a list of available subjects</p>
                </div>
                <div class="row">
                <div class="choose-new-subject-box" ng-init="initiateSubjects(<?php echo $USER->id ?>)">
                    <div class="visible_{{subject.visible}}" ng-class="{'col-md-4': activeCurrentSubject == true, 'col-md-3': activeCurrentSubject != true}" ng-repeat="subject in subjectList" id="{{subject.id}}" ng-if="subject.noexamboards > 0">
                        <div class="choose-new-subject">
                            <a href="javascrip:void(0)" data-toggle="modal" data-target="#currentSubject" ng-click="choose_new_subject(<?php echo $USER->id?>,subject.id); selsubject =subject.id " >
                                <img ng-if="subject.icon" ng-src="../local/b13_dashboard/classes/{{subject.icon}}"/>
                                <i ng-if="!subject.icon" class="fa fa-book"></i>
                                <span>{{subject.name}}</span>
                            </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="currentSubject" ng-init="choose_new_subject()" tabindex="-1" role="dialog" aria-labelledby="currentSubjectLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" ng-show="showChooseOption">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" ng-repeat="getCurrentSubject in getCurrentSubjects">{{getCurrentSubject.name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <form name="chooseNewSubject" novalidate>
            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Exam Board</label>
                </div>
                <div class="col-md-8">
                    <div class="form-group examboard-form">
                                <select class="form-control" name="examboard" id="examboard" ng-model="examboardid" ng-options="examboard.id as examboard.title for examboard in examboardOptions" ng-change="hasChanged()">
                                    <option value=""></option>
                                </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Choose Options</label>
                </div>
                <div class="col-md-8">
                    <div class="form-group checkboxSubjectOptionBox" style="margin-bottom: 20px;">
                        <div class="checkbox checkboxSubjectOption" ng-repeat="subjectOption in subjectOptions">
                            <label><input type="checkbox" ng-model="subjectOption.selected" ng-true-value="1" ng-false-value="0" value="{{subjectOption.id}}">{{subjectOption.title}}</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Teachers Name</label>
                </div>
                <div class="col-md-8">
                        <div id="teacher-group" class="form-group">
                                <input type="text" name="teacher-name" ng-model="teachername" class="form-control checkMaxLenth" maxlength="50" name="title" placeholder="Teacher Name">
                        </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-4">
                    <label class="customLabel">Teachers Email</label>
                </div>
                <div class="col-md-8">
                        <div id="teacher-group" class="form-group">
                                <input type="email" name="teacher-email" ng-model="teacheremail" class="teacher-email form-control checkMaxLenth" maxlength="50" name="title" placeholder="Teacher Email" required>
                        </div>
                </div>

            </div>
        </form>
        </div>
      </div>
      <div class="modal-footer">
         <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6" ng-init="hasChanged()">
                    <button type="button" ng-click="addStudentSelection(<?php echo $USER->id ?>,selsubject,subjectOptions,getExamboardId,teachername,teacheremail)" class="btn btn-primary" >Save</button>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>