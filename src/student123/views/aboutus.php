<div class="about-page">
        <h1 class="logo">
            <a href="/"><img src="../student/assets/img/logo.svg" title="logo" alt="logo"></a>
        </h1>
        <div class="row page-content-box">
            <div class="col-md-7 col-sm-7 padding-right-0">
                        <div id="page-content">
                            <h2>About us</h2>
                            <p>
                                We have a background in education and know how important it is for pupils and schools to achieve good results in exams. 
                            </p>
                            <p>
                                Our aim is to provide extra resources for pupils at different levels. We not only provide content, but the ability to test the learning and make sure it is understood and remembered. We believe the system that we use ensures the course content is remembered not only for the exam sitting, but longer term for further and higher education.
                            </p>
                            <div class="row submit-form">
                                <div class="col-md-5">
                                    <div class="go-back margin-top-10">
                                        <a href="/">Go Back</a>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
</div>

