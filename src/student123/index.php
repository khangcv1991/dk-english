<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LearnLock</title>
    <meta name="google" content="notranslate" />
    <link rel="stylesheet" href="node_modules/bootstrap3/dist/css/bootstrap.min.css">
    <link href="/local/b13_dashboard/assets/js/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

</head>

<body>
    <!-- Preloader start -->
    <div class="se-pre-con"></div>
    <!-- Preloader End -->
    <div class="wrapper-page">
         <img src="../student/assets/img/background.png" class="background-icon">
         <div class="container">
         <ui-view></ui-view>
            <footer class="wrapper-footer">
                <div id="course-footer">
                        <p>© Copyright ©2019 LearnLock</p>
                        <ul class="footer-menu">
                            <li><a href="/student/#/terms-and-conditions">Terms and Conditions</a></li>
                            <li><a href="/student/#/privacy-policy">Privacy Policy</a></li>
                        </ul>
                </div>
            </footer> 
        </div>   
    </div>
    <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap3/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/toastr/toastr.min.js"></script>
    <script type="text/javascript" src="node_modules/angular/angular.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-route/angular-route.js"></script>
    <script type="text/javascript" src="node_modules/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-ui-router/release/stateEvents.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-sanitize/angular-sanitize.js"></script>
    <script type="text/javascript" src="assets/js/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
    <!-- <script src="node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js"></script>
    <script src="node_modules/ng-file-upload/dist/ng-file-upload.min.js"></script> -->
    <script type="text/javascript" src="node_modules/oclazyload/dist/ocLazyLoad.min.js"></script>
    <script type="text/javascript" src="app/app.js"></script>
    <script type="text/javascript" src="app/services/authentication.service.js"></script>
    <script type="text/javascript" src="app/directive/directives.js"></script>
    <script type="text/javascript" src="assets/js/custom-script.js"></script>
    <!-- end the script -->
</body>

<!-- Mirrored from sakura.mrkayo.com/index-demo1.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Mar 2019 04:43:54 GMT -->
</html>
