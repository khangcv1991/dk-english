import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import appReducers from './reducers/studentView/index';
import './index.css';
import App from './App';
import serviceWorker from './serviceWorker';
const store = createStore(appReducers, applyMiddleware(thunk))
ReactDOM.render(
        <Provider store={store} >
            <App />
        </Provider>, document.getElementById('root')
);
serviceWorker();