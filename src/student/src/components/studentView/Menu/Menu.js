import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Route} from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import {actFetchAllSubjectsRequest} from '../../../actions/studentView/index';

const MenuBarLink = ({id,label, to, activeOnlyWhenExtact,topics}) =>{
  var getUrlParams = function (url) 
    {
      var params = {};
      (url + '?').split('?')[1].split('&').forEach(
        function (pair) 
        {
          pair = (pair + '=').split('=').map(decodeURIComponent);
          if (pair[0].length) 
          {
            params[pair[0]] = pair[1];
          }
      });
      return params;
    };
    var params = getUrlParams( window.location.href );
    //console.log(params);
  return(
    <Route 
      path={to}
      exact={activeOnlyWhenExtact}
      children={({ match }) =>{
        var active = match ? 'active' : '';
        return(
          <li>
            <Nav.Link className={active} href={to} key={id}>{label}</Nav.Link>
            <ul>
              {
                topics.map((topic, i) => {
                  //console.log(topic);
                  if(topic.visible == 1){
                    return(
                    <li key={i}><Nav.Link  href={`/subject/${id}/topic/${topic.id}`}>{topic.name}</Nav.Link></li>
                  )
                  }
                })
              }
            </ul>
          </li>
        );
      }}
    />
  );
};

class Menubar extends Component{
  constructor(props){
    super(props);
  }
  componentDidMount(){
    this.props.fetchAllSubjects();
  }
  render(){
    var menubar = [];
    //console.log(this.props.allSubjects.data);
    var allSubjects = this.props.allSubjects.data;
    //console.log(JSON.parse(this.props.allSubjects));
    allSubjects.map((subject, index) => {
        //console.log(JSON.parse(subject.topics));
        if(subject.visible == '1'){
          var subjectList = {id:subject.id,name:subject.name,to:'/subject'+subject.path,topics:JSON.parse(subject.topics)}
          menubar.push(subjectList);
        }
        //console.log(menubar);
    });
    return(
          <>
              <Navbar className="main-menu" expand="lg" fixed="top">
               <Container>
              <Navbar.Brand href="/">Test English</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                  <ul>
                    {this.showMenubar(menubar)}
                  </ul>
                </Nav>
              </Navbar.Collapse>
              </Container>
              </Navbar>
          </>
    )
  }
  showMenubar= (menubar) => {
    var result = null;
    if(menubar.length > 0){
      result = menubar.map((item, index) =>{
          return(
            <MenuBarLink
              key={index}
              id={item.id}
              label={item.name}
              to={item.to}
              activeOnlyWhenExtact = {item.exact}
              topics={item.topics}
            />
          )
      });
    }
    return result;
  }

}
const mapStateToProps = state =>{
  return{
    allSubjects: state.allSubjects,
  }
}
const mapDispatchToProps = (dispatch, props) =>{
  return{
      fetchAllSubjects: () =>{
          dispatch(actFetchAllSubjectsRequest());
      }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Menubar);