import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

class Footer extends Component{
  render(){
    return(
          <footer>
            <Container>
                <Row>
                    <p className="col text-center">© 2010 DK-Team</p>
                </Row>
            </Container>
          </footer>
    )
  }

}
export default Footer;