import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    data:[],
    isLoading:false
};
const topics = (state = initialState, action) =>{
    switch(action.type){
        case Types.FETCH_TOPICS:
            return Object.assign({}, state,{
                data:action.topics,
                isLoading:true
            })
            //state = action.topics;
            //return [...state];
        default: return state;
    }
};
export default topics;