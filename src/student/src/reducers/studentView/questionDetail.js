import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    data:[],
    isLoading:false,
    isProccessing:false,
    seeResult:false,
};
const questionDetail = (state = initialState, action) =>{
    switch(action.type){
        case Types.SENT_QUESTION_REQUEST:
            return Object.assign({}, state,{
                data:[],
                isProccessing:false,
            });
        case Types.FETCH_QUESTION_DETAIL:
            return Object.assign({}, state, {
                data: action.questionDetail,
                isLoading:true,
                isProccessing:true,
            });
           // state = action.questionDetail;
           // return [...state];
        default: return state;
    }
};
export default questionDetail;