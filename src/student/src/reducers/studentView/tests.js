import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    data:[],
    isLoading:false
};
const tests = (state = initialState, action) =>{
    switch(action.type){
        case Types.FETCH_TESTS:
            return Object.assign({}, state, {
                data:action.tests,
                isLoading:true
            })
            //state = action.tests;
            //return [...state];
        default: return state;
    }
};
export default tests;