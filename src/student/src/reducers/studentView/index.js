import { combineReducers } from 'redux';
import subjects from './subjects';
import topics from './topics';
import tests from './tests';
import questions from './questions';
import questionDetail from './questionDetail';
import answers from './answers';
import checkResult from './checkResult';
import getTestResult from './getTestResult';
import allSubjects from './allSubjects';




const appReducers = combineReducers({
    subjects,
    topics,
    tests,
    questions,
    questionDetail,
    answers,
    checkResult,
    getTestResult,
    allSubjects
});
export default appReducers;