import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    isProccessing:true,
    answers:"",
    answerId:"",
    disabled:false,
    data:[],
    seeResult:false
};
const answers = (state = initialState, action) =>{
    switch(action.type){
        case 'SUBMIT_REQUEST':
            return Object.assign({},state,{
                data:[],
                answerId:"",
                isProccessing:false
            })
        case 'SUBMIT_ANSWER':
            return Object.assign({},state,{
                data: action.answer,
                isProccessing:true
            })
        case 'CHECK_VALUE_ANSWER':
            return Object.assign({},state,{
                answerId: action.answerId,
                disabled:true
            })
        default: return state;
    }
};
export default answers;