import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    isLoading:false,
    data:[],
};
const allSubjects = (state = initialState, action) =>{
    switch(action.type){
        case Types.FETCH_ALL_SUBJECTS:
            return Object.assign({},state,{
                data: action.allSubjects,
                isLoading:true
            });
        default: return state;
    }
};
export default allSubjects;