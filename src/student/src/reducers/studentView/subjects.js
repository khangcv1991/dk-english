import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    isLoading:false,
    data:[],
};
const subjects = (state = initialState, action) =>{
    switch(action.type){
        case Types.FETCH_SUBJECTS:
            return Object.assign({},state,{
                data: action.subjects,
                isLoading:true
            });
            //state = action.subjects;
            // return {
            //     data:[...state],
            //     isLoading: false,
            // };
            //return [...state]
        default: return state;
    }
};
export default subjects;