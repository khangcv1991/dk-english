import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    resultData:[]
};
const getTestResult = (state = initialState, action) =>{
    switch(action.type){
        case 'FETCH_TEST_RESULT':{
            return Object.assign({}, state, {
                resultData:action.resultData
            });
        }
        default: return state;
    }
};
export default getTestResult;