import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    data:[],
    currentQuestion:'',
    isLoading:false
};
const questions = (state = initialState, action) =>{
    switch(action.type){
        case 'FETCH_QUESTIONS':
            return Object.assign({}, state, {
                data:action.questions,
                isLoading:true
            });
        // case 'CURRENT_QUESTION':
        //     return Object.assign({}, state, {
        //         data:action.questions,
        //     });
        default: return state;
    }
};
export default questions;