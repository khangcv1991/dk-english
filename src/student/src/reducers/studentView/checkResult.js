import * as Types from './../../constants/studentView/ActionTypes';
var initialState = {
    seeResult:false
};
const checkResult = (state = initialState, action) =>{
    switch(action.type){
        case 'FETCH_QUESTION_RESULT':{
            return Object.assign({}, state, {
                seeResult:action.seeResult
            });
        }
        default: return state;
    }
};
export default checkResult;