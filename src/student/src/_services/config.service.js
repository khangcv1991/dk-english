import axios from 'axios';
import config from '../config/config';

export const configService = {
    callApi
};
function callApi(apiEndpoint, method, body){
    return axios({
        method: method,
        url: config.baseUrl+apiEndpoint,
        headers: getOptions(),
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": body
        })
        .catch(err=>{
            console.log(err);
        })
}

function getOptions(){
    let options = {
        'content-type': 'application/x-www-form-urlencoded',
        "Access-Control-Allow-Origin": "*"
    };
    return options;
}