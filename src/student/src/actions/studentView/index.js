import * as Types from './../../constants/studentView/ActionTypes';
import {wordService} from './../../_services/index';

import {configService} from './../../_services/index';
var userid = 2;
// function shuffle(array) {
//     var currentIndex = array.length, temporaryValue, randomIndex;
  
//     // While there remain elements to shuffle...
//     while (0 !== currentIndex) {
  
//       // Pick a remaining element...
//       randomIndex = Math.floor(Math.random() * currentIndex);
//       currentIndex -= 1;
  
//       // And swap it with the current element.
//       temporaryValue = array[currentIndex];
//       array[currentIndex] = array[randomIndex];
//       array[randomIndex] = temporaryValue;
//     }
//     return array;
// }
//SUBJECTS
//GET ALL SUBJECTS
export const actFetchAllSubjectsRequest = () =>{
    return dispatch =>{
        return (
            configService.callApi('local/b13_dashboard/load-ajax.php?classname=b13subjects&method=load_all_subjects&userid=' + userid,'GET',null).then(response => {
                console.log(response.data);
                //console.log('---- start ---');
                //console.log(JSON.parse(response.data));
                //console.log('---- end ---');
                dispatch(actAllFetchSubjects(response.data.data));
            })
        )
    };
}
export const actAllFetchSubjects = (allSubjects) =>{
    //console.log(allSubjects);
    return{
        type: Types.FETCH_ALL_SUBJECTS,
        allSubjects
    }
}
export const actFetchSubjectsRequest = () =>{
    return dispatch =>{
        return (
            configService.callApi('local/b13_dashboard/load-ajax.php?classname=b13subjects&method=load_all_subjects&userid=' + userid,'GET',null).then(response => {
                //console.log(response);
                dispatch(actFetchSubjects(response.data.data));
            })
        )
    };
}
export const actFetchSubjects = (subjects) =>{
    return{
        type: Types.FETCH_SUBJECTS,
        subjects
    }
}

// GET ALL TOPICS OF CURRENT SUBJECT
export const actFetchTopicsRequest = (topicId) =>{
    return dispatch =>{
        return (
            configService.callApi('local/b13_dashboard/load-ajax.php?classname=b13topics&method=load_all_topics&subjectid=' + topicId +'&userid=' + userid,'GET', null).then(response =>{
                console.log(response);
                dispatch(actFetchTopics(response.data.data));
            })
        )
    };
}

export const actFetchTopics = (topics) =>{
    return{
        type: Types.FETCH_TOPICS,
        topics
    }
}

// GET ALL TESTS OF CURRENT SUBJECT
export const actFetchTestsRequest = (subTopicId) =>{
    return dispatch =>{
        return (
            configService.callApi('local/b13_dashboard/load-ajax.php?classname=b13subtopics&method=load_all_subtopics&topicid=' + subTopicId +'&userid=' + userid,'GET', null).then(response =>{
                //console.log(response);
                dispatch(actFetchTests(response.data.data));
            })
        )
    };
}

export const actFetchTests = (tests) =>{
    return{
        type: Types.FETCH_TESTS,
        tests
    }
}
var getAllQuestionList = [];
// GET ALL QUESTIONS OF CURRENT SUBTOPIC, TOPIC, SUBJECT
export const actFetchQuestionsRequest = (subTopicId) =>{
    return dispatch =>{
        return (
            configService.callApi('local/b13_dashboard/load-ajax.php?classname=b13srl&method=list_questions_by_subtopic&subtopic=' + subTopicId +'&userid=' + userid + '&numberquestions=50','GET', null).then(response =>{
                //console.log(response);
                var allQuestionList = response.data.data;
                getAllQuestionList.push(allQuestionList);
                //console.log(allQuestionList);
                    if(allQuestionList.length == 0){
                        alert('No Questions');
                    }
                    var currentQuestion = -1;
                    for (var i = 0; i< allQuestionList.length; i++){
                        currentQuestion = i;
                        break;
                    }
                    //shuffle array
                    // if(currentQuestion == 0){
                    //     allQuestionList = shuffle(allQuestionList);
                    // }

                    // var totalQuestion = allQuestionList.length;
                    // var hideSubmitAnswer = true;


                    if(currentQuestion == -1){
                        currentQuestion = allQuestionList.length - 1;
                    }
                    //currentQuestion += 1;
                    console.log(currentQuestion);
                    dispatch(actFetchQuestionDetailRequest(parseInt(allQuestionList[currentQuestion].id)));
                    dispatch(actFetchQuestions(allQuestionList));
            })
        )
    };
}
export const actFetchQuestions = (questions) =>{
    console.log(questions);
    return{
        type: Types.FETCH_QUESTIONS,
        questions,
    }
}

//QUESTION LIST REQUEST
export const actQuestionRequest = (questions) =>{
    return {
        type: Types.SENT_QUESTION_REQUEST,
        questions
    }
}

//GET QUESTION DETAIL
export const actFetchQuestionDetailRequest = (questionId, seeResult)=>{
    return dispatch =>{
        dispatch(actSendQuestionRequest());
        return(
            configService.callApi('local/b13_dashboard/load-ajax.php?classname=b13srl&method=detail_question&questionid=' + questionId +'&userid=' + userid,'GET', null).then(response =>{
                //console.log(response.data.data);
                var DetailData = response.data.data;
                if(DetailData){
                    dispatch(actFetchQuestionDetail(response.data.data));
                    //localStorage.clear();
                }else{

                }
                //console.log(seeResult);
            })
        )
    }
}

export const actSendQuestionRequest = (questionDetail) => {
    return {
        type: Types.SENT_QUESTION_REQUEST,
        questionDetail
    }
}

export const actFetchQuestionDetail = (questionDetail) =>{
    return{
        type: Types.FETCH_QUESTION_DETAIL,
        questionDetail
    }
}

//SUBMIT ANSWER OF QUESTION
export const actSubmitAnswerRequest = (form, obj, questionList, currentIndexQuestion, seeResult) =>{
    // console.log(obj);
    // console.log(questionList);
    // console.log(getAllQuestionList);
    questionList = Object.values(questionList);
    return dispatch =>{
        dispatch(actSubmitRequest());
        return(
            configService.callApi('local/b13_dashboard/load-ajax.php?classname=b13srl&method=check_question_answers','POST', form).then(response => {
                dispatch(actSubmitAnswer(response.data.data));
                dispatch(actCheckValueAnswer(obj.answers));
                //dispatch(actFetchQuestionDetailRequest(parseInt(response.data.data[0].id)));
                var correctData = '';
                if(response.data.data === 1){
                    correctData = true;
                }else{
                    correctData = false;
                }
                var customObject = {
                    "userid":parseInt(obj.userid),
                    "subtopicid":parseInt(obj.subtopicid),
                    "questionid":obj.questionid,
                    "attempted":parseInt(obj.attempted),
                    "answersvalue":obj.answers,
                    "correct":correctData,
                    "question":questionList[0],
                    "answers":questionList[1]
                }
                 var temp = [];
                 temp.push(customObject);
                 var localTemp = localStorage.getItem("resultTest");
                 if(localTemp != null){
                    localTemp = JSON.parse(localTemp);
                 }else{
                     localTemp = [];
                 }
                 localTemp.push(customObject);
                 localStorage.setItem("resultTest",JSON.stringify(localTemp));
                 //console.log(localTemp);

                // if(parseInt(obj.questionid) <= obj.totalQuestionLength){
                //console.log(getAllQuestionList[0][currentIndexQuestion].id);
                //console.log(currentIndexQuestion);
                dispatch(actFetchQuestionDetailRequest(getAllQuestionList[0][currentIndexQuestion].id, seeResult));
                const resultData = JSON.parse(localStorage.getItem('resultTest'));
                //console.log(resultData);
                dispatch(getActFetchQeustionResult(resultData));

                if(seeResult){
                    //console.log('stop here');
                    dispatch(actFetchQeustionResult(seeResult));
                }
            })
        )
    }
}
export const actFetchQeustionResult = (seeResult) =>{
    //console.log(seeResult); 
    return{
        type: Types.FETCH_QUESTION_RESULT,
        seeResult
    }
}
export const getActFetchQeustionResult = (resultData) =>{
    //console.log(resultData); 
    return{
        type: Types.FETCH_TEST_RESULT,
        resultData
    }
}
//SUBMIT REQUEST
export const actSubmitRequest = (answer) =>{
    return{
        type: Types.SUBMIT_REQUEST,
        answer
    }
}
export const actSubmitAnswer = (answer) =>{
    return{
        type: Types.SUBMIT_ANSWER,
        answer
    }
}
//CHECK VALUE ANSWER
export const actCheckValueAnswer = (answerId)=>{
    //console.log(answerId);
    return{
        type: Types.CHECK_VALUE_ANSWER,
        answerId
    }
}