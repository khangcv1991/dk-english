import React from 'react';
import HomePage from './pages/StudentView/HomePage/HomePage';
import TopicPage from './pages/StudentView/TopicPage/TopicPage';
import TestPage from './pages/StudentView/TestPage/TestPage';
import QuestionPage from './pages/StudentView/QuestionPage/QuestionPage';
import ResultPage from './pages/StudentView/ResultPage/ResultPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';

const routes = [
    {
        path:'/',
        exact:true,
        main:() => <HomePage />
    },
    {
        path:'/subject/:id',
        exact:true,
        main: ({match, history}) => <TopicPage match={match} history={history}/>
    },
    {
        path:'/subject/:id/topic/:id',
        exact:true,
        main: ({match, history}) => <TestPage match={match} history={history}/>
    },
    {
        path:'/subject/:id/topic/:subtopicid/question/:id',
        exact:true,
        main: ({match, history}) => <QuestionPage match={match} history={history}/>
    },
    {
        path:'/test-result',
        exact:true,
        main: ({match, history}) => <ResultPage match={match} history={history}/>
    },
    {
        path: '',
        exact: false,
        main:() => <NotFoundPage />
    }
];
const mapStateToProps = state =>{
    return{
        subjects: state.subjects,
        topics: state.topics,
        isLoading:state.subjects.isLoading
    }
}
export default routes;