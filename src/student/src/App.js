import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css';
import './assets/font-awesome-4.7.0/css/font-awesome.min.css';
import {Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import routes from './routes';
import Menu from './components/studentView/Menu/Menu';
import Footer from './components/studentView/Footer/Footer';
export default function App() {
  const showContentMenus = (routes) =>{
    var result = null;
        if(routes.length > 0){
          result = routes.map((route, index)=>{
            return (
              <Route
                key = {index}
                path= {route.path}
                exact = {route.exact}
                component = {route.main}
              />
            )
          })
        }
        return <Switch>{result}</Switch>
    }
    return (
      <div className="wrapper">
          <Router>
          <CssBaseline />
          <Menu />
          <main className="main-page">
            {showContentMenus(routes)}
          </main>
          <Footer/>
        </Router>
      </div>
    );
  }
