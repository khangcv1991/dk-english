import React, { Component } from 'react';
import { connect } from 'react-redux';
//import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import ButtonToolbar from 'react-bootstrap/Spinner';
import Parser from 'html-react-parser';
import {actFetchQuestionsRequest} from '../../../actions/studentView/index';
import {actFetchQuestionDetailRequest} from '../../../actions/studentView/index';
import {actSubmitAnswerRequest} from '../../../actions/studentView/index';
import {resetLocalStorage} from '../../../util/util';

class TestPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            isProccessing:false,
            answers:[],
            minutes:15,
            seconds:0
        };
        this.onChangeAnswer = this.onChangeAnswer.bind(this);
        this.nextQuestionAfterSelect = React.createRef();
        resetLocalStorage();
    }
    componentDidMount(){
        var { match } = this.props;
        if(match){
            var subtopicId = match.params.id;
            var getsubtopicId = match.params.subtopicid;
            this.props.fetchQuestions(subtopicId);
            console.log(subtopicId);
            console.log(getsubtopicId);
        }
        const userid = localStorage.getItem('userid');
        const subtopicid = localStorage.getItem('subtopicid');
        const questionid = localStorage.getItem('questionid');
        const attempted = localStorage.getItem('attempted');
        const answers = localStorage.getItem('answers');

        this.setState({
            userid,
            subtopicid,
            questionid,
            attempted,
            answers,
        });
        //countdown timer
        this.countDownInterval = setInterval(() => {
            const { seconds, minutes} = this.state;
            if(seconds > 0){
                this.setState(({seconds}) => ({
                    seconds : seconds - 1
                }))
            }
            if(seconds === 0){
                if(minutes === 0){
                    clearInterval(this.countDownInterval)
                }else{
                    this.setState(({minutes}) => ({
                        minutes: minutes - 1,
                        seconds: 59
                    }))
                }
                //alert('ok');
            }
            
        }, 1000)
    }

    onFetchNextQuestion = (id) =>{
        this.props.fetchQuestionDetail(id);
    }
    onFetchPreviousQuestion = (id) =>{
        this.props.fetchQuestionDetail(id);
    }
    onChangeAnswer(e){
        this.setState({
            answers: e.target.value,
        })
    }
    onSubmitAnswer = (questionId, totalQuestionLength, questionList, currentIndexQuestion, seeResult) =>{
        var {answers} =  this.state;
        //console.log(answers);
        //console.log(totalQuestionLength);

        var getAnswerArray = [answers];
        //console.log(getAnswerArray);
        var { match } = this.props;
        var getsubtopicId = match.params.subtopicid;
        var form = new FormData();
        var obj = {
            "userid": "2",
            "subtopicid": getsubtopicId,
            "questionid": questionId,
            "attempted": "0",
            "answers": parseInt(answers),
            "totalQuestionLength":totalQuestionLength
        }
        //console.log(obj);
        form.append("userid", "2");
        form.append("subtopicid", getsubtopicId);
        form.append("questionid", questionId);
        form.append("attempted", "0");
        form.append("answers[0]", parseInt(answers));
        this.props.submitAnswer(form, obj, questionList, currentIndexQuestion, seeResult);
        //this.nextQuestionAfterSelect.current.click();

    }
    onResetTesting = () =>{
        resetLocalStorage();
        window.location.reload();
    }
    render(){
        const {minutes, seconds} = this.state;
        var answerId = this.props.answers.answerId;
        var isLoading = this.props.questionDetail.isLoading;
        var isProccessing = this.props.questionDetail.isProccessing;
        var isSubmitProcessing = this.props.answers.isProccessing;
        if(this.props.questions.data[0]){
            var showQuestionList = this.props.questions.data[0];
        }else{
            var showQuestionList = [];
        }
        console.log(showQuestionList);
        //console.log(this.props.questionDetail);
        if(!isLoading){
            return (
            <div className={'loadingPage'}>
                    <div className={'loadingBox'}>
                        <Spinner animation="grow" variant="info" />
                    </div>
            </div>   
            )
        }
        //console.log(this.state);
        var questions = this.props.questions.data;
        //var currentQuestions = this.props.questions;
        var questionDetail = this.props.questionDetail.data;
        //console.log(this.props.questionDetail);
        //console.log(questions);
        //console.log(this.props.questions);
        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;
            // While there remain elements to shuffle...
            while (0 !== currentIndex) {
              // Pick a remaining element...
              randomIndex = Math.floor(Math.random() * currentIndex);
              currentIndex -= 1;
              // And swap it with the current element.
              temporaryValue = array[currentIndex];
              array[currentIndex] = array[randomIndex];
              array[randomIndex] = temporaryValue;
            }
            return array;
        }
        var allQuestionList = this.props.questions.data;
        var totalQuestion = allQuestionList.length;

        if(!questionDetail.length){
            return(
                <>
                   <div>
                        <div className="banner-top">
                            <img
                                src="/assets/banner-topic.jpg"
                                alt="Banner Topic"
                            />
                        </div>
                        <div className="question-content">
                        <Container>
                            <Form id="formQuestion">
                                <fieldset id="singleQuestion">
                                    <Form.Group as={Row}>
                                        <Form.Label as="legend">
                                            
                                        </Form.Label>
                                        <div className="questionBox">
                                                        <div className={'loadingPage isProcessing'}>
                                                            <div className={'loadingBox'}>
                                                            <ButtonToolbar>
                                                                <Button variant="primary" disabled>
                                                                    <Spinner
                                                                    as="span"
                                                                    animation="grow"
                                                                    size="sm"
                                                                    role="status"
                                                                    aria-hidden="true"
                                                                    />
                                                                    Loading...
                                                                </Button>
                                                                </ButtonToolbar>
                                                            </div>
                                                </div>     
                                        </div> 
                                    </Form.Group>
                                </fieldset>
                            </Form>
                        </Container>
                        </div>
                    </div>
                </>
            )
        }else{
        var convertQuestion = Object.values(questionDetail)[0];
        var totalQuestion = questions.length;
        var answers = this.props.answers.data;
        var getCurrentIndex = [];
        for (var i = 0; i< allQuestionList.length; i++){
            // var currentQuestion = i;
            if(allQuestionList[i].id === convertQuestion.question.id){
                getCurrentIndex.push({'index':i, 'id':allQuestionList[i].id});
            }
        }
        var currentQuestion = getCurrentIndex[0].index;
        //console.log(currentQuestion);
        var seeResult = false;
        if(currentQuestion < (allQuestionList.length - 1)){
            currentQuestion +=1;
            //console.log(currentQuestion);
            var submitAnswer = <Button variant="primary" onClick={()=> this.onSubmitAnswer(questionId, totalQuestion, convertQuestion, parseInt(currentQuestion), seeResult )}> Next question </Button>
        }else if(currentQuestion == (allQuestionList.length - 1)){
            var seeResult = true;
            var submitAnswer = <Button variant="success" onClick={()=> this.onSubmitAnswer(questionId, totalQuestion, convertQuestion, parseInt(currentQuestion), seeResult )}> Finish test </Button>
        }
        var questionId = parseInt(convertQuestion.question.id);
            
        //console.log(convertQuestion.question);

        console.log(this.props.checkResult.seeResult);
        if(this.props.checkResult.seeResult){
            const data = JSON.parse(localStorage.getItem('resultTest'));
            var countCorrectAnswer = [];
            for(var i = 0; i < data.length; i++){
                if(data[i].correct){
                    console.log(data[i].correct);
                    countCorrectAnswer.push(data[i].correct);
                }
            }
            if(!data){
                return (
                <div className={'loadingPage'}>
                        <div className={'loadingBox'}>
                            <Spinner animation="grow" variant="info" />
                        </div>
                </div>   
                )
            }
            return(
                <>
                    <div className="banner-top">
                    <img
                        src="/assets/banner-topic.jpg"
                        alt="Banner Topic"
                    />
                </div>
                <div className="question-content result-page">
                <Container>
                        <div className="result-content-page">
                        <h1>You have completed this test</h1>
                        <p className="correct-question-box">
                            <p>
                                Correct answers:{countCorrectAnswer.length}/{data.length}
                            </p> 
                            <p>
                                Your score:{(countCorrectAnswer.length/data.length*100)}%
                            </p>
                        </p>
                        <Button className="resetTesting" variant="primary" onClick={() => this.onResetTesting()}> Start again </Button>
                        <div className="clearfix"></div>
                        <h2>Check your answers:</h2>
                        {data.map((item, index) => {
                            return(
                                    <Form.Group as={Row} key="index">
                                        <Form.Label as="legend" className="d-flex">
                                            <span>{(index + 1)}</span>{Parser(item.question.questiontext)}
                                        </Form.Label>
                                        <div className="clearfix"></div>
                                        <div className="questionBox">
                                            <ul>
                                            {
                                                item.answers.map((answerList, subindex) => {
                                                    console.log(parseInt(answerList.id));
                                                    console.log(parseInt(item.answersvalue));
                                                    console.log(answerList.feedback.length)
                                                    if(item.correct && item.answersvalue === parseInt(answerList.id) || answerList.feedback.length > 0){
                                                        var successIcon = <i className="fa fa-check "></i>
                                                        var correctAnswer = "correctAnswer";
                                                    } 
                                                    if(!item.correct && item.answersvalue === parseInt(answerList.id)){
                                                        var failedIcon = <i className="fa fa-close "></i>
                                                        var incorrectAnswer = "incorrectAnswer";
                                                    }
                                                    if(item.answersvalue === parseInt(answerList.id) || answerList.feedback.length > 0){
                                                        var checked = true;
                                                        console.log(checked);
                                                    }
                                                    return(
                                                        <>
                                                            <li key={subindex} className={correctAnswer || incorrectAnswer}>
                                                                        <label>{Parser(answerList.answer)} {failedIcon}{successIcon}</label>
                                                            </li>
                                                            <div className="clearfix"></div>
                                                        </>
                                                    )
                                                })
                                            }
                                            </ul>
                                        </div> 
                                    </Form.Group>
                            )
                        })}
                        </div>
                </Container>
                </div>
                </>
            )
        }
        return(
            <div>
            <div className="banner-top">
                <img
                    src="/assets/banner-topic.jpg"
                    alt="Banner Topic"
                />
            </div>
            <div className="question-content">
             <div className="container">
                <div className="col-md-12">
                    <div className="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href={`/subject/${showQuestionList.subjectid}`}>{showQuestionList.subject}</a></li>
                            <li><a href={`/subject/${showQuestionList.subjectid}/topic/${showQuestionList.topicid}`}>{showQuestionList.topic}</a></li>
                            <li>{showQuestionList.subtopic}</li>
                        </ul>
                    </div>
                    <h1 className="main-title">{showQuestionList.subtopic} <small>(<i>{this.props.questionDetail.data[0].question.name}</i>)</small></h1>
                    <div className="direction-box">
                        {Parser(this.props.questionDetail.data[0].question.generalfeedback)}
                    </div>
                </div> 
             </div>  
             <Container>
                <Form id="formQuestion">
                {/* <span className="countTime">{minutes}:{seconds < 10 ? `0${ seconds }` : seconds}</span> */}
                    <fieldset id="singleQuestion">
                        <Form.Group as={Row}>
                            <Form.Label as="legend">
                                {Parser(convertQuestion.question.questiontext)}
                            </Form.Label>
                            <div className="questionBox">
                                {
                                    convertQuestion.answers.map((getQuestionDetail) => {
                                        if(answers === 1 && answerId === parseInt(getQuestionDetail.id)){
                                            var successIcon = <i className="fa fa-check "></i>
                                        }else if(answers === 1 && answerId !== parseInt(getQuestionDetail.id) || answers === 0 && answerId === parseInt(getQuestionDetail.id) || answers ==undefined){
                                            var failedIcon = <i className="fa fa-close "></i>
                                        }
                                        if(getQuestionDetail.id == this.props.answers.answerId){
                                            var checked = true;
                                        }
                                        if(isSubmitProcessing === false ){
                                            var showSubmitBtn = true;
                                            return(
                                                    <div key={getQuestionDetail.id} className={'loadingPage isProcessing'}>
                                                        <div className={'loadingBox'}>
                                                        <ButtonToolbar>
                                                            <Button variant="primary" disabled>
                                                                <Spinner
                                                                as="span"
                                                                animation="grow"
                                                                size="sm"
                                                                role="status"
                                                                aria-hidden="true"
                                                                />
                                                                Loading...
                                                            </Button>
                                                            </ButtonToolbar>
                                                        </div>
                                                    </div>   
                                            )  
                                        }else{
                                            return(
                                                    <div key={getQuestionDetail.id}>
                                                        <Form.Check
                                                        type="radio"
                                                        label={Parser(getQuestionDetail.answer)}
                                                        name="formHorizontalRadios"
                                                        id={getQuestionDetail.id}
                                                        defaultValue={getQuestionDetail.id}
                                                        onChange={ this.onChangeAnswer}
                                                        />
                                                    </div>                  
                                            )
                                        }
                                    })
                                }
                            </div> 
                        </Form.Group>
                        <Row>
                            {/* <Col align="center">
                                {previousQuestionBtn}
                            </Col> */}
                            <Col align="center" xs={12}>
                                {submitAnswer}
                            </Col>
                            {/* <Col align="center">
                                {nextQuestionBtn}
                            </Col> */}
                        </Row>
                    </fieldset>
                </Form>
              </Container>
             </div>
            </div>
            )
        }
    }
}
const mapStateToProps = state =>{
    return{
        questions: state.questions,
        questionDetail: state.questionDetail,
        answers: state.answers,
        checkResult:state.checkResult
    }
}
const mapDispatchToProps = (dispatch, props) =>{
    return{
        fetchQuestions: (subtopicId) =>{
            dispatch(actFetchQuestionsRequest(subtopicId));
        },
        fetchQuestionDetail: (questionId) =>{
            dispatch(actFetchQuestionDetailRequest(questionId));
        },
        submitAnswer: (form, obj, questionList, currentIndexQuestion, seeResult) =>{
            dispatch(actSubmitAnswerRequest(form, obj, questionList, currentIndexQuestion, seeResult));
        },
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TestPage);