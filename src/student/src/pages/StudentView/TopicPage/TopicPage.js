import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import {actFetchTopicsRequest, actFetchTestsRequest} from '../../../actions/studentView/index';
import {resetLocalStorage} from '../../../util/util';

class TopicPage extends Component{
    constructor(props){
        super(props);
        this.state = {};
        resetLocalStorage();
    }
    componentDidMount(){
        var { match } = this.props;
        if(match){
            var subjectID = match.params.id;
            this.props.fetchTopics(subjectID);
        }
    }
    // onFetchTests = (topicId) =>{
    //     console.log(topicId);
    //     this.props.fetchTests(topicId);
    // }
    render(){
        var topics = this.props.topics.data;
        var isLoading = this.props.topics.isLoading;
        console.log(topics);
        if(!isLoading){
            return (
              <div className={'loadingPage'}>
                    <div className={'loadingBox'}>
                        <Spinner animation="grow" variant="info" />
                    </div>
              </div>   
            )
        }
        //console.log(topics);
        return(
            <>
            <div className="banner-top">
                            <img
                            src="../assets/banner-topic.jpg"
                            alt="Banner Topic"
                            />
            </div>
            <div className="page-wrapper">
            <Container>
                <Row>
                <Col md="12">
                    <div className="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>{topics[0].subject}</li>
                        </ul>
                    </div>
                    <h1 className="main-title">{topics[0].subject}</h1>    
                </Col>
               {
                topics.map((topic, index) => {
                    if(topic.visible == 1){
                        return(
                        <Col xs="6" sm="6" md="3" key={index}>
                            <Card className="subject-box">
                            <Card.Link href={`/subject/${topic.parent}/topic/${topic.id}`}
                                // onClick={() => this.onFetchTests(topic.id)}
                            >
                                {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
                                <i className="fa fa-book"></i>
                            </Card.Link>
                            <Card.Body>
                                <Card.Title>
                                    <Card.Link href={topic.id}>
                                        {topic.name}
                                    </Card.Link>
                                </Card.Title>
                                <Card.Text>
                                         {topic.description}   
                                </Card.Text>
                            </Card.Body>
                            </Card>
                        </Col>

                        )
                    }
                    })
                }
                </Row>
                </Container>
                </div>
            </>
        )
    }
}
const mapStateToProps = state =>{
    return{
        topics: state.topics
    }
}
const mapDispatchToProps = (dispatch, props) =>{
    return{
        fetchTopics: (subjectID) =>{
            dispatch(actFetchTopicsRequest(subjectID));
        },
        // fetchTests: (topicId) =>{
        //     dispatch(actFetchTestsRequest(topicId));
        // }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TopicPage);