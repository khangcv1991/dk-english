import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import {actFetchTestsRequest} from '../../../actions/studentView/index';
import {resetLocalStorage} from '../../../util/util';

class TestPage extends Component{
    constructor(props){
        super(props);
        this.state = {};
        resetLocalStorage();
    }
    componentDidMount(){
        var { match } = this.props;
        if(match){
            var subtopicId = match.params.id;
            //console.log(subtopicId);
            this.props.fetchTests(subtopicId);
        }
    }
    render(){
        var tests = this.props.tests.data;
        console.log(tests);
        var isLoading = this.props.tests.isLoading;
        if(!isLoading){
            return (
              <div className={'loadingPage'}>
                    <div className={'loadingBox'}>
                        <Spinner animation="grow" variant="info" />
                    </div>
              </div>   
            )
        }
        //console.log(tests);
        return(
            <>
            <div className="banner-top">
                            <img
                            src="/assets/banner-topic.jpg"
                            alt="Banner Topic"
                            />
            </div>
            <div className="page-wrapper">
            <Container>
                <Row>
                    
                <Col md="12">
                    <div className="breadcrumb">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href={`/subject/${tests[0].subjectid}`}>{tests[0].subject}</a></li>
                            <li>{tests[0].topic}</li>
                        </ul>
                    </div>
                    <h1 className="main-title">{tests[0].topic}</h1>    
                </Col>
               {
                tests.map((test, index) => {
                    return(
                        <Col xs="6" sm="6" md="3" key={index}>
                            <Card className="subject-box">
                            <Card.Link href={`/subject/${test.subjectid}/topic/${test.topicid}/question/${test.id}`}
                                // onClick={() => this.onFetchTests(test.id)}
                            >
                                {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
                                <i className="fa fa-book"></i>
                            </Card.Link>
                            <Card.Body>
                                <Card.Title>
                                    <Card.Link href={`/subject/${test.subjectid}/topic/${test.topicid}/question/${test.id}`}>
                                        {test.fullname}
                                    </Card.Link>
                                </Card.Title>
                            </Card.Body>
                            </Card>
                        </Col>

                        )
                    })
                }
                </Row>
                </Container>
                </div>
            </>
        )
    }
}
const mapStateToProps = state =>{
    return{
        tests: state.tests
    }
}
const mapDispatchToProps = (dispatch, props) =>{
    return{
        fetchTests: (subtopicId) =>{
            dispatch(actFetchTestsRequest(subtopicId));
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TestPage);