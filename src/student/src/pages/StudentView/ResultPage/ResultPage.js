import React, {Component} from 'react';
import { connect } from 'react-redux';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import ButtonToolbar from 'react-bootstrap/Spinner';
import Parser from 'html-react-parser';
class ResultPage extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        const data = JSON.parse(localStorage.getItem('resultTest'));
        //console.log(data);
    }
    onResetTesting = () =>{
        localStorage.clear(function(){
            window.location.reload();
        })
    }
    render(){
        const data = JSON.parse(localStorage.getItem('resultTest'));
        var countCorrectAnswer = [];
        for(var i = 0; i < data.length; i++){
            if(data[i].correct){
                console.log(data[i].correct);
                countCorrectAnswer.push(data[i].correct);
            }
        }
        console.log(countCorrectAnswer.length);

        // const questionDetail = this.props.questionDetail;
         console.log(data);
        // console.log(questionDetail);
        if(!data){
            return (
            <div className={'loadingPage'}>
                    <div className={'loadingBox'}>
                        <Spinner animation="grow" variant="info" />
                    </div>
            </div>   
            )
        }
        //c
        return(
            <div>
                <div className="banner-top">
                    <img
                        src="/assets/banner-topic.jpg"
                        alt="Banner Topic"
                    />
                </div>
                <div className="question-content result-page">
                <Container>
                        <div className="result-content-page">
                        <h1>You have completed this test</h1>
                        <p className="correct-question-box">
                            <p>
                                Correct answers:{countCorrectAnswer.length}/{data.length}
                            </p> 
                            <p>
                                Your score:{(countCorrectAnswer.length/data.length*100)}%
                            </p>
                        </p>
                        <Button className="resetTesting" variant="primary" onClick={() => this.onResetTesting()}> Start again </Button>
                        <div className="clearfix"></div>
                        <h2>Check your answers:</h2>
                        {data.map((item, index) => {
                            return(
                                    <Form.Group as={Row} key="index">
                                        <Form.Label as="legend" className="d-flex">
                                            <span>{(index + 1)}</span>{Parser(item.question.questiontext)}
                                        </Form.Label>
                                        <div className="clearfix"></div>
                                        <div className="questionBox">
                                            <ul>
                                            {
                                                item.answers.map((answerList, subindex) => {
                                                    console.log(parseInt(answerList.id));
                                                    console.log(parseInt(item.answersvalue));
                                                    console.log(answerList.feedback.length);
                                                    if(item.correct && item.answersvalue === parseInt(answerList.id) || answerList.feedback.length > 0){
                                                        var successIcon = <i className="fa fa-check "></i>
                                                        var correctAnswer = "correctAnswer";
                                                    } 
                                                    if(!item.correct && item.answersvalue === parseInt(answerList.id)){
                                                        var failedIcon = <i className="fa fa-close "></i>
                                                        var incorrectAnswer = "incorrectAnswer";
                                                    }
                                                    if(item.answersvalue === parseInt(answerList.id) || answerList.feedback.length > 0){
                                                        var checked = true;
                                                        console.log(checked);
                                                    }
                                                    return(
                                                        <>
                                                            <li key={subindex} className={correctAnswer || incorrectAnswer}>
                                                                        <label>{Parser(answerList.answer)} {failedIcon}{successIcon}</label>
                                                            </li>
                                                            <div className="clearfix"></div>
                                                        </>
                                                    )
                                                })
                                            }
                                            </ul>
                                        </div> 
                                    </Form.Group>
                            )
                        })}
                        </div>
                </Container>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state =>{
    return{
        getTestResult: state.getTestResult
    }
}
const mapDispatchToProps = (dispatch, props) =>{
    return{
        
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ResultPage);