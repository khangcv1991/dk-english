import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Carousel from 'react-bootstrap/Carousel';
import Spinner from 'react-bootstrap/Spinner';

import {actFetchSubjectsRequest} from '../../../actions/studentView/index';
import {actFetchTopicsRequest} from '../../../actions/studentView/index';
import {resetLocalStorage} from '../../../util/util';

class HomePage extends Component{
    constructor(props){
        super(props);
        this.state = {
            subjects: [],
            topics:[],
        };
        resetLocalStorage();
    }
    componentDidMount(){
        this.props.fetchAllSubjects();
    }
    render(){
        var getAllSubjects = this.props.subjects.data;
        var isLoading = this.props.subjects.isLoading;
        console.log(getAllSubjects);
        if(!isLoading){
            return (
              <div className={'loadingPage'}>
                    <div className={'loadingBox'}>
                        <Spinner animation="grow" variant="info" />
                    </div>
              </div>   
            )
        }else{
        return(
            <>
                    <Carousel>
                        <Carousel.Item>
                            <img
                            src="assets/banner-topic.jpg"
                            alt="First slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            src="assets/banner-topic.jpg"
                            alt="First slide"
                            />
                        </Carousel.Item>
                    </Carousel>
                <Container>
                    <Row>
                    {
                    getAllSubjects.map((subject, index) => {
                    if(subject.visible == '1'){
                        if(subject.icon == ''){
                        var imageIcon = <i className="fa fa-book"></i>;
                    }else{
                        var imageIcon =<Card.Img variant="top" src={`/local/b13_dashboard/classes/${subject.icon}`} />
                    }
                    return(
                        <Col xs="6" sm="6" md="3" key={index}>
                            <Card className="subject-box">
                                <Card.Link href={`/subject/${subject.id}`}>
                                    {imageIcon}   
                                </Card.Link>
                                <Card.Body>
                                    <Card.Title>
                                        <Card.Link href={`/subject/${subject.id}`}>
                                            {subject.name}
                                        </Card.Link>
                                    </Card.Title>
                                    <Card.Text>
                                            {subject.description}   
                                    </Card.Text>
                                    {/* <Button variant="primary">
                                        <Card.Link href={`/subject/${subject.id}`}>
                                            Take a test
                                        </Card.Link>
                                    </Button> */}
                                </Card.Body>
                            </Card>
                        </Col>

                        )
                    }
                    })
                }
                    </Row>
                </Container>
            </>
        )
        }
    }
}
const mapStateToProps = state =>{
    return{
        subjects: state.subjects,
        topics: state.topics,
        isLoading:state.subjects.isLoading
    }
}
const mapDispatchToProps = (dispatch, props) =>{
    return{
        fetchAllSubjects: () =>{
            dispatch(actFetchSubjectsRequest());
        },
        // fetchTopics: (id) =>{
        //     console.log(id);
        //     dispatch(actFetchTopicsRequest(id));
        // }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(HomePage);