<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Functions used to show question editing interface
 *
 * @package    moodlecore
 * @subpackage questionbank
 * @copyright  2019 khangcao {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/questionlib.php');


function get_questions_by_subtopic($subtopicid ){
    global $CFG, $DB;
    $data = $DB->get_records_sql('SELECT q.*
                                    FROM {question} q
                                    JOIN {question_categories} qc
                                    ON q.category = qc.id
                                    JOIN {context} c
                                    ON qc.contextid = c.id
                                    JOIN {course_modules} cm
                                    ON cm.id = c.instanceid
                                    JOIN {course} co
                                    ON co.id = cm.course
                                    WHERE  co.id = ' . $subtopicid );
    return $data;
}

function update_questiongroup($questionid) {
    global $CFG, $DB;
    $question = $DB->get_record('question', array('id'=> $questionid));
    $questioncategory = $DB->get_record('question_categories', array('id' => $question->category));
    $subtopic = $DB->get_record_sql("SELECT co.* 
                                    FROM {question_categories} qc
                                    JOIN {context} c
                                    ON qc.contextid = c.id
                                    JOIN {course_modules} cm
                                    ON cm.id = c.instanceid
                                    JOIN {course} co
                                    ON co.id = cm.course 
                                    WHERE qc.id  = $questioncategory->id ");
    $topic = $DB->get_record("course_categories", array('id' => $subtopic->category));
    $questiontitle = str_replace(' ', '',strtolower($question->name));

   
    if(! $DB->record_exists_select('b13_questiongroup', "title like '$questiontitle' AND subtopicid = $subtopic->id ")){
        $transaction = $DB->start_delegated_transaction();
        $updatedata = new \stdClass();
        $updatedata->title = $questiontitle;
        $updatedata->subtopicid = $subtopic->id;
        $updatedata->topicid = $topic->id;
        $updatedata->subjectid = $topic->parent;
        $DB->insert_record('b13_questiongroup', $updatedata);
        $transaction->allow_commit();
    }
    $questiongroup = $DB->get_record_sql("SELECT * FROM {b13_questiongroup} WHERE title like '$questiontitle'  AND subtopicid = $subtopic->id  ");

  
        $transaction = $DB->start_delegated_transaction();
        $DB->delete_records('b13_questiongroup_question',array('questionid'=>$questionid));
        $questiongroupquestion = new \stdClass();
        $questiongroupquestion->questiongroupid = $questiongroup->id;
        $questiongroupquestion->questionid = $questionid;
        $questiongroupquestion->qtype = $question->qtype;
        $DB->insert_record('b13_questiongroup_question', $questiongroupquestion);
        $transaction->allow_commit();
      

    return true;
}

function refresh_questiongroup_question($subtopicid){
    global $DB;
    $questions = get_questions_by_subtopic($subtopicid);
}
function delete_questiongroup_question($questionid){
    global $DB;
    $transaction = $DB->start_delegated_transaction();
    $DB->delete_records('b13_questiongroup_question', array('questionid' => $questionid));
    $transaction->allow_commit();
}
