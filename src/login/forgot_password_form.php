<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Forgot password page.
 *
 * @package    core
 * @subpackage auth
 * @copyright  2006 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/user/lib.php');

/**
 * Reset forgotten password form definition.
 *
 * @package    core
 * @subpackage auth
 * @copyright  2006 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class login_forgot_password_form extends moodleform {

    /**
     * Define the forgot password form.
     */
    function definition() {
        global $USER;

        $mform    = $this->_form;
        $mform->setDisableShortforms(true);

        //$mform->addElement('header', 'searchbyusername', get_string('searchbyusername'), '');
        //$mform->addElement('header', 'searchbyusername', 'Enter Your User Name', '');

        $purpose = user_edit_map_field_purpose($USER->id, 'username');
        $mform->addElement('text', 'username', get_string('username'), 'size="20"' . $purpose);
        $mform->setType('username', PARAM_RAW);

        //$submitlabel = get_string('search');
        $submitlabel = 'Reset Password';
        $mform->addElement('submit', 'submitbuttonusername', $submitlabel);

        //$mform->addElement('header', 'searchbyemail', get_string('searchbyemail'), '');
        //$mform->addElement('header', 'searchbyemail', 'Enter Your Email', '');


        $purpose = user_edit_map_field_purpose($USER->id, 'email');
        $mform->addElement('text', 'email', get_string('email'), 'maxlength="100" size="30"' . $purpose);
        $mform->setType('email', PARAM_RAW_TRIMMED);

        //$submitlabel = get_string('search');

        $submitlabel = 'Reset Password';
        $mform->addElement('submit', 'submitbuttonemail', $submitlabel);
    }

    /**
     * Validate user input from the forgot password form.
     * @param array $data array of submitted form fields.
     * @param array $files submitted with the form.
     * @return array errors occuring during validation.
     */
    function validation($data, $files) {

        $errors = parent::validation($data, $files);
        $errors += core_login_validate_forgot_password_data($data);

        return $errors;
    }

}
?>
<style>
body{
    background:#fff;
}
#page-login-forgot_password nav ,#page-login-forgot_password #page-header{
    display:none;
}
#page-login-forgot_password #page{
    margin-top:0px;
}
#page-login-forgot_password #region-main-box{
    padding:0px;
}
#page-login-forgot_password form .fitem{
    float:left;
    width: 318px;
    margin-left: 0;
}
#page-login-forgot_password form .fitem .col-md-3{
    width: 140px;
    flex: none;
    display: inline-block;
    max-width: initial;
    margin-bottom: 10px;
    padding-left: 0px;
}
#page-login-forgot_password form .fitem .col-md-9{
    max-width: initial;
    flex: none;
    padding-left: 0px;
}
#page-login-forgot_password form .fitem .col-md-9 input#id_username, #page-login-forgot_password form .fitem .col-md-9 input#id_email{
    width: 100%;
    border-radius: 30px;
    outline: 0;

}
#page-login-forgot_password form .fitem.femtylabel{
    float: left;
    margin-top: 32px;
    margin-left: 0px;
}
#page-login-forgot_password form .fitem.femtylabel .col-md-3{
    display:none;
}
#page-login-forgot_password form .fitem.femtylabel .col-md-9{
    padding-left:0px;
}
#page-login-forgot_password form #id_submitbuttonusername ,#page-login-forgot_password form #id_submitbuttonemail{
    margin-top: 21px;
    margin-left: 10px;
    border-radius: 40px;
}
#page-login-forgot_password form > div:nth-child(2), #page-login-forgot_password form > div:nth-child(3){
    display:none;
}

#page-login-forgot_password form .fitem.femptylabel{
        width:110px;
    }
#page-login-forgot_password .card-body{
    padding-top:5px;
}
    #nav-drawer.closed{
        display:none;
    }
    #page-footer{
        display:none;
    }
    #page-wrapper::after{
        display:none !important;
    }
    #region-main>.card{
        min-height:initial !important;
    }
</style>