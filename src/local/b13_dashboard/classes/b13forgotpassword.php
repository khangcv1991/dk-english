<?php

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/user/editlib.php';
require_once $CFG->libdir . '/authlib.php';
require_once $CFG->dirroot.'/auth/manual/auth.php';
require_once $CFG->dirroot . '/login/lib.php';
use local_b13_dashboard\util\json;
use \auth_plugin_manual;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
/**
 * Class b13forgotpassword
 * @package local_b13_dashboard
 */
class b13forgotpassword
{
    public function resend_password(){
        global $DB;
     
        $email = required_param('email', PARAM_RAW);
        $user = $DB->get_record('user', array('email'=>$email));
        $authplugin = new auth_plugin_manual();
        $newpwd = $this->randomPassword();
        if($user){
            $passwordisupdated = $authplugin->user_update_password($user, $newpwd);
            $user->password = $newpwd;
            $this->send_mail(array(0=>$user->email), $newpwd);
            json::encode(array(0=>$user));
        }else{
            json::error("Sorry, this email does not exist!!");
        }
      
    }

    public  function  send_mail($recipents, $appended_content,$log = null)
    {
        global $CFG, $DB;
        $user = $DB->get_record('user', array('email'=>$recipents[0]));
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 2; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host = 'ssl://smtp.gmail.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth = true; // Enable SMTP authentication
            $mail->Username = 'b13test123@gmail.com'; // SMTP username
            $mail->Password = '1ForB13Work'; // SMTP password
            $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom($mail->Username, 'noreply');
            foreach ($recipents as $recipent) {
                $mail->addAddress($recipent); // Add a recipient
            }
          
            //Attachments
            // $mail->addAttachment($attached_file); // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'Learnlock password reset';
            $mail->Body = "Your username " .$user->username." with a new LearnLock password is ".$appended_content.' 
                            <br>Log in here '. $CFG->wwwroot;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
            
            if (file_exists($attached_file)) {
                unlink($attached_file);
            }
            echo "sended successfully";
            return true;
        } catch (Exception $e) {
            // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            // json::error(json_encode($e));
            print_r($e);
        }
    }
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    function test_sendmail(){
        $this->send_mail(array("khangcv1991@gmail.com"), "abc");
    }
}