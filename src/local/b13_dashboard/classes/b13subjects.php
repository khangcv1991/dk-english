<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/classes/editcategory_form.php';
require_once $CFG->dirroot . '/course/classes/category.php';
require_once $CFG->dirroot . '/course/externallib.php';
include 'fileUpload.php';
use core_course_category;
use local_b13_dashboard\b13topics;
use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use \stdClass;

/**
 * Class b13subjects
 * @package local_b13_dashboard
 */
class b13subjects
{

    /**
     * @throws \dml_exception
     * @throws \coding_exception
     */

    public function dashboard()
    {
        global $CFG;
        $subjects = b13subjects::load_all_subjects(false); ?>
            <ul class="breadcrumb custombreadcrumb">
                <li>
                    <a href="?classname=b13subjects&amp;method=dashboard">B13</a>
                </li>
                <li>
                    <span>Subject</span>
                </li>
                <li>
                    <span>Subject List</span>
                </li>
            </ul>
            <ul class="profile-user">
                <li>
                    <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
                </li>
            </ul>
            <!-- Modal -->
            <div class="modal fade" id="logoutModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirm</h4>
                        </div>
                        <div class="modal-body">
                        <p>Do you really want to log out? </p>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            dashboard_util::add_breadcrumb(get_string_b13('subject_title'));
            dashboard_util::add_breadcrumb(get_string_b13('new_subject_list_title'));
            dashboard_util::start_page();
            ?>
            <h3 class="element-header">Subjects</h3>
            <div class="add-new-b13-button">
                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addNewB13Model">
                    Add New Subject
                  </button>
                  <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#ActionB13Model">
                       <i class="fa fa-cog"></i> Action
                  </button>
            </div>
            <div class="element-box b13-list-page">
                <div class="container-fuild">
                    <div class="row b13-list-view">
                    <?php 
                    $array = json_decode(json_encode($subjects), true);
                    foreach ($array as $value) {
                    ?>
                        <div class="col-md-2 col-sm-6 visible_<?php echo $value['visible']?>">
                            <div class="b13-box">
                                    <a href="javascript:void(0)" onclick="getTopic(<?php echo $value['id']?>)">
                                        <?php if( !empty($value['icon'])){?>
                                        <img src="../b13_dashboard/classes/<?php echo $value['icon']?>"?>
                                        <?php }else {?>
                                        <i class="fa fa-book"></i>
                                        <?php }?>
                                        <h3 title="<?php echo $value['name']?>"><?php echo $value['name']?></h3>
                                    </a>
                            </div>
                        </div>
                    <?php }?>
                    </div>
                </div>
             </div>
             <div class="modal fade enterPress" id="addNewB13Model" tabindex="-1" role="dialog" aria-labelledby="addNewB13Model">
                <div class="modal-dialog" role="document">
                    <form id="createNewSubject" class="createNewSubject createSubjecIcon" method="POST">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add New Subject</h4>
                            </div>
                            <div class="modal-body">
                                    <!-- NAME -->
                                    <div id="name-group" class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control checkMaxLenth subjectname" maxlength="50" name="name" placeholder="Subject Name">
                                        <i class="label label-danger">Field must be Alphabetical or Numerical characters only</i>
                                        <!-- errors will go here -->
                                    </div>

                                    <!-- Description -->
                                    <div id="description-group" class="form-group">
                                        <label for="description">Description</label>
                                        <input type="text" class="form-control" name="description" placeholder="Description">
                                        <!-- errors will go here -->
                                    </div>
                                    <div class="form-group">
                                            <div class="selectImage">
                                                <label>Select Your Image</label>
                                                <br/>
                                                <input type="file" name="subjectIcon" class="iconFile" required />
                                            </div>
                                            <div class="image_preview">
                                                <img class="previewing" src="../b13_dashboard/assets/images/no-image.png" />
                                            </div>
                                            <br/>
                                            <input type="hidden" class="inputUrlIcon" name="icon" value=""/>
                                            <input type="submit" class="submitSubjectIconBtn hide" style="display:none;" value="Upload" />
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default closeModal" >Close</button>
                                <a href="javascript:void(0)" class="btn btn-success createNewSubjectBtn submitButton">Create New Subject </a>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>

            <div class="modal fade" id="ActionB13Model" tabindex="-1" role="dialog" aria-labelledby="ActionB13Model">
             <div class="modal-dialog modal-lg" role="document">
              <form id="ActionSubject" method="POST">
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   <h4 class="modal-title" id="myModalLabel">Action</h4>
                 </div>
                 <div class="modal-body">
                        <div class="loadingAjaxIcon" style="display: none;">
                             <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif">
                         </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><i class="fa fa-book margin-right-10"></i>Subject Name</th>
                                <th>Edit</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($array as $value) { ?>
                                    <tr class="subject_<?php echo $value['id']?>">
                                        <td class="visible_<?php echo $value['visible']?>"><i class="fa fa-book margin-right-10"></i><?php echo $value['name']?></td>
                                        <td class="visible_<?php echo $value['visible']?>"><a href="javascript:void(0)" data-toggle="modal" onclick="detailSubject(<?php echo $value['id'] ?>)" data-target="#updateSubject">Edit</a></td>
                                        <td>
                                        <?php if ($value['visible'] == 0) { ?>
                                            <a href="javascript:void(0)" onclick="enableSubject(<?php echo $value['id'] ?>)">Enable Subject</a>
                                        <?php } else { ?>
                                            <a href="javascript:void(0)" onclick="disableSubject(<?php echo $value['id'] ?>)">Disable Subject</a>
                                        <?php } ?>
                                        </td>
                                    </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
             </form>
               </div>
             </div>
             <div class="alert alert-success" id="success-alert">
                <strong>"successfully"! </strong>
            </div>

        <!-- update subtopics -->
             <div class="modal fade enterPress " id="updateSubject" tabindex="-1" role="dialog" aria-labelledby="updateSubject">
                <div class="modal-dialog modal-lg" role="document">
                 <form id="updateSubjectForm" class="updateSubjectForm" method="POST">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close closeCurrentModel" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Edit Subject</h4>
                    </div>
                    <div class="modal-body"></div>
                    </form>
                  </div>
                </div>
        <?php
        dashboard_util::end_page();}

    public static function load_all_subjects($isjson = true){
        global $DB;
        $data = $DB->get_records_sql(
            "SELECT cc.*, suic.icon
              FROM {course_categories} cc
              LEFT JOIN {b13_subject_icon} suic
              ON suic.subjectid = cc.id
             WHERE cc.depth = 1"
        );
        foreach($data as &$s){
            $topics = $DB->get_records("course_categories" , array("parent"=>$s->id));
            $tmparry = array();
            foreach($topics as $t){
                array_push($tmparry, $t);
            }
            $s->topics = json_encode($tmparry);
        }
        if ($isjson == true) {
            json::encode($data);
        } else {
            return $data;
        }
    }

    public function disable_subject($subjectid = null)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        if ($DB->record_exists('b13_examboards_courses', array('subjectid' => $subjectid))) {
            if ($isjson) {
                json::error("this subject can not be disabled as its subtopic is belong to an examboard");
            } else {
                return false;
            }

        }
        $updatedata = array();
        $updatedata['id'] = $subjectid;
        $updatedata['visible'] = 0;

        if ($DB->record_exists('course_categories', array('id' => $subjectid))) {
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('course_categories', $updatedata);
            $topics = $DB->get_records('course_categories', array('parent' => $subjectid));
            foreach ($topics as $topic) {
                b13topics::disable_topic($topic->id, false);
            }
            $transaction->allow_commit();

            json::encode("disable successfully");
        } else {
            json::error("subject does not exist");
        }

    }

    public function enable_subject($subjectid = null)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        $updatedata = array();
        $updatedata['id'] = $subjectid;
        $updatedata['visible'] = 1;

        if ($DB->record_exists('course_categories', array('id' => $subjectid))) {
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('course_categories', $updatedata);
            $topics = $DB->get_records('course_categories', array('parent' => $subjectid));
            foreach ($topics as $topic) {
                b13topics::enable_topic($topic->id, false);
            }
            $transaction->allow_commit();
            json::encode("enable successfully");
        } else {
            json::error("subject does not exist");
        }

    }

    public function create_new_subject()
    {
        global $DB;
        $name = required_param('name', PARAM_RAW);
        $description = required_param('description', PARAM_RAW);
        $icon = required_param('icon', PARAM_RAW);
        $category = new stdClass();
        $category->parent = 0;
        $category->name = $name;
        $category->description = $description;
        $data = core_course_category::create($category);
        // print_r($data);
        if ($data) {
            $subjecticon = new stdClass();
            $subjecticon->subjectid = $data->id;
            $subjecticon->icon = $icon;
            $transaction = $DB->start_delegated_transaction();
            $DB->insert_record('b13_subject_icon', $subjecticon);
            $transaction->allow_commit();

            $category = new stdClass();
            $category->parent = $data->parent;
            $category->name = $data->name;
            $category->description = $data->description;
            $tmparray = array();
            array_push($tmparray, $category);
            json::encode($tmparray);
        }
    }
    

    public function update_subject($subjectid = null)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        $name = required_param('name', PARAM_RAW);
        $description = required_param('description', PARAM_RAW);
        $icon = required_param('icon', PARAM_RAW);
        $updatedata = array();
        $updatedata['id'] = $subjectid;
        $updatedata['name'] = $name;
        $updatedata['description'] = $description;

        if ($DB->record_exists('course_categories', array('id' => $subjectid))) {
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('course_categories', $updatedata);
            $subjecticon = $DB->get_record('b13_subject_icon', array('subjectid'=>$subjectid));
            if($subjecticon){
                $subjecticon->icon = $icon;
                $DB->update_record('b13_subject_icon', $subjecticon);
            }else{
                $subjecticon = new stdClass('b13_subject_icon', array());
                $subjecticon->subjectid = $subjectid;
                $subjecticon->icon = $icon;
                $DB->insert_record('b13_subject_icon', $subjecticon);
            }
            $transaction->allow_commit();
            json::encode("update successfully");
        } else {
            json::error("update fail!!!");

        }
    }

    public function detail_subject($subjectid = null)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($DB->record_exists('course_categories', array('id' => $subjectid))) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->get_record('course_categories', array('id' => $subjectid));
            $icon = $DB->get_record('b13_subject_icon', array('subjectid' => $subjectid));
            $data->icon = isset($icon) ? $icon->icon : null;

            $transaction->allow_commit();
            json::encode(array(0=>$data));
        } else {
            json::error("not have subject");

        }
    }

    public function upload_image($subjectid){
        // Simple validation (max file size 2MB and only two allowed mime types)
        $validator = new FileUpload\Validator\Simple('2M', ['image/png', 'image/jpg']);

        // Simple path resolver, where uploads will be put
        $pathresolver = new FileUpload\PathResolver\Simple('/my/uploads/dir');

        // The machine's filesystem
        $filesystem = new FileUpload\FileSystem\Simple();

        // FileUploader itself
        $fileupload = new FileUpload\FileUpload($_FILES['files'], $_SERVER);

        // Adding it all together. Note that you can use multiple validators or none at all
        $fileupload->setPathResolver($pathresolver);
        $fileupload->setFileSystem($filesystem);
        $fileupload->addValidator($validator);

        // Doing the deed
        list($files, $headers) = $fileupload->processAll();

        // Outputting it, for example like this
        foreach($headers as $header => $value) {
            header($header . ': ' . $value);
        }

        echo json_encode(['files' => $files]);

        foreach($files as $file){
            //Remeber to check if the upload was completed
            if ($file->completed) {
                echo $file->getRealPath();
                
                // Call any method on an SplFileInfo instance
                var_dump($file->isFile());
            }
        }
    }

}

?>