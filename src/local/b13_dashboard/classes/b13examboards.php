<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//.
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();
require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->libdir . '/filelib.php';
require_once $CFG->libdir . '/gradelib.php';
require_once $CFG->libdir . '/completionlib.php';
require_once $CFG->libdir . '/plagiarismlib.php';
require_once $CFG->dirroot . '/course/modlib.php';
require_once $CFG->dirroot . '/mod/book/mod_form.php';
use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use local_b13_dashboard\b13util;

use stdClass;

/**
 * Class b13examboards
 * @package local_b13_dashboard
 */
class b13examboards
{

    public function dashboard()
    {
        global $CFG;
        $subjects = b13subjects::load_all_subjects(false);
        //$subjectid = required_param('subjectid', PARAM_INT);
        //$topics = b13topics::load_all_topics($subjectid);
        $examboards = b13examboards::load_all_examboards(false);
        ?>

        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a target="_top" href="/">B13</a>
            </li>
            <li>
                <span>Exam Boards</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb(get_string_b13('examboard_title'));
        dashboard_util::add_breadcrumb(get_string_b13('examboard_title'));
        dashboard_util::start_page();?>
            <h3 class="element-header">
                Exam Board
            </h3>
        <?php
        echo '<div class="add-new-b13-button">
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addNewB13Model">
                      Create Exam Board
                    </button>
                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#ActionB13Model">
                       <i class="fa fa-cog"></i> Action
                    </button>
              </div>
              <div class="element-box b13-list-page">
                  <div class="container-fuild">
                      <div class="row b13-list-view">';
                        $arrayExemBoard = json_decode(json_encode($examboards), true);
                        foreach ($arrayExemBoard as $value) {
                            echo '<div class="col-md-2 col-sm-6">
                                  <div class="b13-box">
                                      <a onclick="view_detail_examboards(' . $value['id'] . ')">
                                          <i class="fa fa-book"></i>
                                          <h3>' . $value['title'] . '</h3>
                                      </a>
                                  </div>
                            </div>';
                        }
                        echo '</div>
                  </div>
               </div>
               <div class="modal fade enterPress" id="addNewB13Model" tabindex="-1" role="dialog" aria-labelledby="addNewB13Model">
                  <div class="modal-dialog modal-lg b13-list-page" role="document">
                    <form id="createNewExamboard" class="createNewExamboard" method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add New Exam Board</h4>
                        </div>
                        <div class="modal-body">
                            <div id="name-group" class="form-group">
                                <label for="title">Examboard Name</label>
                                <input type="text" class="form-control checkMaxLenth examboardtitle" maxlength="50" name="title" placeholder="Examboard Name">
                                <i class="label label-danger">Field must be Alphabetical or Numerical characters only</i>
                                <!-- errors will go here -->
                            </div>

                            <div id="name-group" class="form-group hide">
                                <label for="idnumber">Id Number</label>
                                <input type="text" class="form-control idnumberexamboard" name="idnumber" placeholder="Unique Id Number">
                                <i class="label label-danger">Id Number must unique.</i>
                                <!-- errors will go here -->
                            </div>

                            <div id="name-group" class="form-group">
                                <label for="description">Description</label>
                                <input type="text" class="form-control" name="description" placeholder="Description">
                                <!-- errors will go here -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default closeModal">Close</button>
                            <a href="javascript:void(0)" class="btn btn-success createNewExamboardBtn submitButton"> Create New Exam Board </a>
                        </div>
                        </div>
                    </div>
                    </form>
                </div>
                </div>
               ';
               // Edit subject      
            echo '<div class="modal fade" id="ActionB13Model" tabindex="-1" role="dialog" aria-labelledby="ActionB13Model">
            <div class="modal-dialog modal-lg" role="document">
             <form id="ActionExamboard" method="POST">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Action</h4>
                </div>
                <div class="modal-body">
                       <div class="loadingAjaxIcon" style="display: none;">
                            <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif">
                        </div>
                   <table class="table table-bordered">
                       <thead>
                           <tr>
                               <th><i class="fa fa-book margin-right-10"></i>Examboard Name</th>
                               <th>Edit</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                           ';
                           foreach ($arrayExemBoard as $value) {
                           echo '<tr class="examboard_'.$value['id'].'">
                                   <td><i class="fa fa-book margin-right-10"></i>'.$value['title'].'</td>
                                   <td><a href="javascript:void(0)" data-toggle="modal" onclick="detailExamboard('.$value['id'].')" data-target="#updateExamboard">Edit</a></td>
                                   <td><a href="javascript:void(0)" onclick="deleteExamboard('.$value['id'].')">Delete Examboard</a></td>
                               </tr>';
                               }
                           echo '</tbody>
                   </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
              </div>
            </div>
            ';

            //update Examboard

            echo '
            <div class="modal fade enterPress " id="updateExamboard" tabindex="-1" role="dialog" aria-labelledby="updateExamboard">
               <div class="modal-dialog modal-lg" role="document">
                <form id="updateExamboardForm" class="updateExamboardForm" method="POST">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close closeCurrentModel" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title" id="myModalLabel">Edit Examboard</h4>
                   </div>
                   <div class="modal-body"></div>
                   </form>
                 </div>
               </div>
            ';
        dashboard_util::end_page();
    }

    public function view_detail_examboards()
    {
        global $CFG;
        $subjects = b13subjects::load_all_subjects(false);
        //$subjectid = required_param('subjectid', PARAM_INT);
        //$topics = b13topics::load_all_topics($subjectid);
        //$examboards = b13examboards::load_all_examboards(false);
        $examboards = b13examboards::load_all_examboards(false);
        $examboardid = required_param('examboardid', PARAM_INT);
        $getSubjectByExamboard = b13examboards::load_subject_by_examboard($examboardid, false);
        $getExamboardId = $_GET['examboardid'];
        $loadSubtopicByExamboard = b13examboards::load_subtopic_by_examboard($getExamboardId, false);
        ?>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="?classname=b13subjects&amp;method=dashboard">B13</a>
            </li>
            <li>
            <a href="javascript:void(0)" onclick="window.history.go(-1); return false;">Examboard</a>
            </li>
            <?php 
            $arrayExemBoard = json_decode(json_encode($examboards), true);
            foreach ($arrayExemBoard as $value) { ?>
                    <?php if($value['id'] == $getExamboardId){
                        echo '<li><span>'.$value['title'].'</span></li>';
                    }?>
            <?php } ?>
   
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        dashboard_util::add_breadcrumb(get_string_b13('examboard_title'));
        dashboard_util::add_breadcrumb(get_string_b13('examboard_title'));
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">
        <?php 
            $arrayExemBoard = json_decode(json_encode($examboards), true);
            foreach ($arrayExemBoard as $value) { ?>
                    <?php if($value['id'] == $getExamboardId){
                        echo $value['title'];
                    }?>
            <?php } ?>
         </h3>
        <?php
        echo '
            <div class="examboard-page">
                    <div class="row">
                        <div class="col-md-3" style="width:25%">
                            <div class="examboard-box">
                                <input type="hidden" id="examboardid" name="examboardid" value="'. $getExamboardId .'" />
                                <h2>Subjects</h2>
                               <div class="container-examboard-box">
                                    <ul id="allSubjects"></ul>
                               </div>
                            </div>
                        </div>
                        <div class="col-md-1" style="text-algin:center;width:6%;">
                            <div class="switch-subtopic-box">
                                <div class="switch-subtopic">
                                        <i class="fa fa-angle-double-right"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3" style="width:31.5%">
                            <div class="examboard-box">
                                <h2>Subtopics</h2>
                                <div class="container-examboard-box selectSubtopicInExamboard">
                                <form id="registerSubtopicsToExamboard" method="POST">
                                    <input type="hidden" name="examboardid" value="'. $getExamboardId .'" />
                                        <div class="loadingAjax">
                                            <div class="loading-subtopic-box">
                                                <div class="loading-subtopic">
                                                    <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif"/>
                                                </div>
                                            </div>
                                        </div>
                                    <ul id="allSubtopics"></ul>
                                </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1" style="text-align:center;width:6%;">
                                <div class="switch-subtopic-box">
                                    <div class="switch-subtopic">
                                            <i class="fa fa-angle-double-right"></i>
                                            <i class="fa fa-angle-double-left"></i> 
                                    </div>
                                </div>
                        </div>

                        <div class="col-md-3" style="width:31.5%">
                            <div class="examboard-box">
                                <h2>Selected Subtopics</h2>
                                <div class="container-examboard-box selectedSubtopicInExamboard">
                                <form id="unregisterSubtopicsToExamboard" class="selectedSubtopicsToExamboard" method="POST">
                                    <input type="hidden" name="examboardid" value="'. $getExamboardId .'" />
                                        <div class="loadingAjax">
                                            <div class="loading-subtopic-box">
                                                <div class="loading-subtopic">
                                                    <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif"/>
                                                </div>
                                            </div>
                                        </div>
                                        <ul id="unregisterSubtopics">
                                            <button type="submit" class="hide btn btn-success">Remove Selected Subtopic</button>
                                        </ul>
                                        </div>
                                </form>
                                </div>
                                </div>
                        </div>
                    </div>
                    
                    <a href="javascript:void(0)" class="btn btn-primary"  onclick="window.history.go(-1); return false;">Back</a>
            </div>
        ';
        dashboard_util::end_page();
    }
    /**
     * @throws \dml_exception
     * @throws \coding_exception
     */
    public static function load_all_examboards($isjson = true)
    {
        global $DB;
        $data = $DB->get_records("b13_examboards");
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }

    public static function load_examboard_by_subject($subjectid = null,$isjson = true){
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        $data = $DB->get_records_sql("SELECT e.*
                                    FROM {course_categories} coca
                                    JOIN {course_categories} coca1
                                    ON coca.id = coca1.parent
                                    JOIN {course} c
                                    ON c.category = coca1.id
                                    JOIN {b13_examboards_courses} ec
                                    ON ec.courseid = c.id
                                    JOIN {b13_examboards} e
                                    ON e.id = ec.examboardid
                                    WHERE coca.id = $subjectid") ;
        if ($isjson) {
        json::encode($data);
        } else {
        return $data;
        }

    }
    public static function detail_examboard($examboardid = null, $isjson = true){
        global $DB;
        if($examboardid == null){
            $examboardid = required_param('examboardid', PARAM_INT);
        }
        $ex = $DB->get_record('b13_examboards', array('id' => $examboardid ));
        if($isjson){
            json::encode(array(0=>$ex));
        }else{
            return $ex;
        }
    }

    public static function update_examboard($examboardid = null, $isjson = true){
        global $DB;
        if($examboardid == null){
            $examboardid = required_param('examboardid', PARAM_INT);
        }
        $title = required_param('title', PARAM_RAW);
        $idnumber = required_param('idnumber', PARAM_ALPHANUMEXT);
        $description = required_param('description', PARAM_RAW);

        $updatedata = array();
        $updatedata['id'] = $examboardid;
        $updatedata['title'] = $title;
        $updatedata['idnumber'] = $idnumber;
        $updatedata['description'] = $description;
        // if (!$DB->record_exists('b13_examboards', array('id' => $examboardid))) {
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('b13_examboards', $updatedata);
            $transaction->allow_commit();
            if($isjson){
                json::encode("updated successfully");
            }else{
                return true;
            }
        // }else{
        //     if($isjson){
        //         $idnumberError = "ID number is already used for another examboard (".$idnumber.")";
        //         json::error($idnumberError);
        //     }else{
        //         return true;
        //     }
        // }
    }

    public static function delete_examboard($examboardid = null, $isjson = true){
        global $DB;
        if($examboardid == null){
            $examboardid = required_param('examboardid', PARAM_INT);
        }

        $transaction = $DB->start_delegated_transaction();
        $DB->delete_records('b13_examboards', array('id' => $examboardid));
        $DB->delete_records('b13_examboards_courses', array('examboardid' => $examboardid));
        $transaction->allow_commit();
        if ($transaction) {
            json::encode(array(array("message" => "delete successfully")));
        } else {
            json::error("can not delete");
        }
    }

    public static function load_options_by_examboard($examboardid = null,$subjectid = null, $isjson = true){
        global $DB;
        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $data = $DB->get_records_sql("SELECT o.*
                                    FROM {b13_option} o
                                    JOIN {b13_examboards_options} exop
                                    ON o.id = exop.optionid
                                    WHERE exop.examboardid = $examboardid AND exop.subjectid = $subjectid");
        
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }
    public function load_subtopic_by_examboard($examboardid = null, $isjson = true)
    {
        global $DB;
        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }

        $data = $DB->get_records_sql("SELECT  c.*, coca2.name as subject, coca1.name as topic
                                        FROM {b13_examboards_courses} ec
                                        JOIN {course} c
                                        ON c.id = ec.courseid
                                        JOIN {course_categories} coca1
                                        ON c.category = coca1.id
                                        JOIN {course_categories} coca2
                                        ON coca1.parent = coca2.id
                                        WHERE ec.examboardid = " . $examboardid);

        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }
    }

    public function load_subtopic_by_examboard_topic($examboardid = null,$topicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        $data = $DB->get_records_sql("SELECT  c.*, coca2.name as subject, coca1.name as topic, ststsu.bookstatus, ststsu.quizstatus,ststsu.readedpage,ststsu.readedquiz
                                        FROM {b13_examboards_courses} ec
                                        JOIN {course} c
                                        ON c.id = ec.courseid
                                        JOIN {course_categories} coca1
                                        ON c.category = coca1.id
                                        JOIN {course_categories} coca2
                                        ON coca1.parent = coca2.id
                                        LEFT JOIN {b13_status_student_subtopic} ststsu
                                        ON ststsu.courseid = c.id
                                        WHERE ec.examboardid = " . $examboardid . " AND coca1.id =" .$topicid. " AND ststsu.userid = $userid");
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }
    }

    public function load_subject_by_examboard($examboardid = null, $isjson = true)
    {
        global $DB;
        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }

        $data = $DB->get_records_sql("SELECT  coca1.*
                                        FROM {b13_examboards_courses} ec
                                        JOIN {course} c
                                        ON c.id = ec.courseid
                                        JOIN {course_categories} coca
                                        ON c.category = coca.id
                                        JOIN {course_categories} coca1
                                        ON coca.parent = coca1.id
                                        WHERE ec.examboardid = " . $examboardid);
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }

    public function load_topic_by_examboard($examboardid = null, $isjson = true)
    {
        global $DB;
        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }

        $data = $DB->get_records_sql("SELECT  coca.*
                                        FROM {b13_examboards_courses} ec
                                        JOIN {course} c
                                        ON c.id = ec.courseid
                                        JOIN {course_categories} coca
                                        ON c.category = coca.id

                                        WHERE ec.examboardid = " . $examboardid);

        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }

    public function load_topic_by_examboard_subject ($examboardid = null,$subjectid = null, $isjson = true)
    {
        global $DB;
        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }

        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $data = $DB->get_records_sql("SELECT  coca.*
                                        FROM {b13_examboards_courses} ec
                                        JOIN {course} c
                                        ON c.id = ec.courseid
                                        JOIN {course_categories} coca
                                        ON c.category = coca.id
                                        WHERE ec.examboardid = " . $examboardid ."AND coca.parent =  $subjectid"
                                    );

        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }

    public function create_examboard()
    {
        global $DB;
        $title = required_param('title', PARAM_RAW);
        $idnumber = required_param('idnumber', PARAM_ALPHANUMEXT);
        $description = required_param('description', PARAM_RAW);

        $examboard = new \stdClass();
        $examboard->title = $title;
        $examboard->idnumber = $idnumber;
        $examboard->description = $description;

        // if (!$DB->record_exists('b13_examboards', array('idnumber' => $idnumber))) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->insert_record('b13_examboards', $examboard);
            $examboard2 = $DB->get_records('b13_examboards', array('id' => $data));
            $transaction->allow_commit();
            json::encode($examboard2);
        // } else {
        //     json::error("the examboard exists!!!");
        // }

    }

    public function register_subtopic_examboard($subtopicid = null, $examboardid = null)
    {
        global $DB;
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }

        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }
        $subtopic = $DB->get_record('course',array('id'=>$subtopicid));
        $topic = $DB->get_record('course_categories',array('id'=>$subtopic->category));
        $coex = new \stdClass();
        $coex->examboardid = $examboardid;
        $coex->courseid = $subtopicid;
        $coex->topicid = $subtopic->category;
        $coex->subjectid = $topic->parent;

        if ($subtopicid != null && $examboardid != null && !$DB->record_exists('b13_examboards_courses', array('courseid' => $subtopicid, 'examboardid' => $examboardid))) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->insert_record('b13_examboards_courses', $coex);
            $transaction->allow_commit();
            b13util::refresh_examboard_options(false);
            json::encode(array($data));
        } else {
            b13util::refresh_examboard_options(false);
            json::encode(array(array("message" => "the setting exists!!!")));
        }

    }

    public function unregister_subtopic_examboard($subtopicid = null, $examboardid = null)
    {
        global $DB;
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }

        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }

        if ($DB->record_exists('b13_examboards_courses', array('courseid' => $subtopicid, 'examboardid' => $examboardid))) {
            $data = $DB->delete_records('b13_examboards_courses', array('courseid' => $subtopicid, 'examboardid' => $examboardid));
            b13util::refresh_examboard_options(false);
            json::encode(array($data));
        } else {
            b13util::refresh_examboard_options(false);
            json::encode(array(array("message" => "the setting not exists!!!")));
        }

    }
}

?>