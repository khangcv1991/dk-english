<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/classes/editcategory_form.php';
require_once $CFG->dirroot . '/course/classes/category.php';

use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use local_b13_dashboard\b13students;
use \stdClass;
/**
 * Class b13log
 * @package local_b13_dashboard
 */
class b13log
{
    
   public static function log_user_sign_in($userid = null){
        global $DB;
        if($userid == null){
            $userid = required_param('userid', PARAM_INT);
        }
        $transaction = $DB->start_delegated_transaction();
        $uv = new stdClass();
        $uv->userid = $userid;
        $uv->time = time();
        $DB->insert_record('b13_report_user_visit', $uv);
        $transaction->allow_commit();
   }

   public static function log_user_access_subject($userid, $subjectid){
        global $DB;
        $transaction = $DB->start_delegated_transaction();
        $uv = new stdClass();
        $uv->userid = $userid;
        $uv->subjectid = $subjectid;
        $uv->time = time();
        $DB->insert_record('b13_report_user_ativity', $uv);
        $transaction->allow_commit();
   }
   public static function log_user_access_topic($userid, $topicid){
        global $DB;
        $transaction = $DB->start_delegated_transaction();
        $uv = new stdClass();
        $uv->userid = $userid;
        $uv->topicid = $topicid;
        $uv->time = time();
        $DB->insert_record('b13_report_user_ativity', $uv);
        $transaction->allow_commit();
    }
    public static function log_user_access_learning_subtopic($userid, $subtopicid){
        global $DB;
        if($DB->record_exists('b13_report_user_ativity', array('userid'=>$userid, 'subtopicid'=>$subtopicid ))){
            return;
        }
        $transaction = $DB->start_delegated_transaction();
        $uv = new stdClass();
        $uv->userid = $userid;
        $uv->subtopicid = $subtopicid;
        $uv->time = time();
        $DB->insert_record('b13_report_user_ativity', $uv);
        $transaction->allow_commit();
    }
}
