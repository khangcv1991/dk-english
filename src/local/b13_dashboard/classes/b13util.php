<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/classes/editcategory_form.php';
require_once $CFG->dirroot . '/course/classes/category.php';

use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use local_b13_dashboard\b13students;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use \stdClass;
/**
 * Class b13util
 * @package local_b13_dashboard
 */
class b13util
{
    

    public static function send_mail($recipents, $title, $body){
        global $CFG;
        $mail = new PHPMailer(true); // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 2; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host = 'ssl://smtp.gmail.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth = true; // Enable SMTP authentication
            $mail->Username = 'b13test123@gmail.com'; // SMTP username
            $mail->Password = '1ForB13Work'; // SMTP password
            $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465; // TCP port to connect to
            
            //Recipients
            $mail->setFrom($mail->Username, 'noreply');
            foreach ($recipents as $recipent) {
                $mail->addAddress($recipent); // Add a recipient
            }
          
            //Attachments
            // $mail->addAttachment($attached_file); // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = $title;
            $mail->Body = $body;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
            
            return true;
        } catch (Exception $e) {
            // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            // json::error(json_encode($e));
            print_r($e);
        }
    }
    public static function refresh_examboard_options($isjson = true)
    {
        global $DB;
      
        $examboards = $DB->get_records('b13_examboards');
        $transaction = $DB->start_delegated_transaction();
        $DB->delete_records('b13_examboards_options');
        foreach($examboards as $examboard){
            $subtopics = $DB->get_records_sql('SELECT c.* FROM {course} c JOIN {b13_examboards_courses} exco ON c.id = exco.courseid WHERE exco.examboardid = :examboardid', array('examboardid'=>$examboard->id));
            foreach($subtopics as $subtopic){
                $sutopicoption = $DB->get_record('b13_topic_option', array('categoryid'=>$subtopic->category));
                $topic = $DB->get_record('course_categories', array('id'=>$subtopic->category));
                if(!$DB->record_exists('b13_examboards_options', array('examboardid'=>$examboard->id, 'optionid'=>$sutopicoption->optionid))){
                    $tmpexop = new stdClass();
                    $tmpexop->examboardid = $examboard->id;
                    $tmpexop->optionid = $sutopicoption->optionid;
                    $tmpexop->subjectid = $topic->parent;
                    $DB->insert_record('b13_examboards_options', $tmpexop);
                }
            }
        }

        // foreach ($exops as $exop) {
        //     $tmpexop = new stdClass();
        //     $tmpexop->examboardid = $exop->examboardid;
        //     $tmpexop->optionid = $exop->optionid;
        //     $tmpexop->subjectid = $exop->subjectid;
        //     $DB->insert_record('b13_examboards_options', $tmpexop);
        // }
        $exops = $DB->get_records('b13_examboards_options');
        $transaction->allow_commit();
        if ($isjson) {
            json::encode($exops);
        } else {
            return true;
        }

    }

    public static function refresh_b13_student_quiz_box($isjson = true)
    {
        global $DB;
        $transaction = $DB->start_delegated_transaction();
        $qbs = $DB->get_records('b13_student_quiz_box');
        foreach ($qbs as $qb) {
            $tmp_subtopic = $DB->get_record('course', array('id'=>$qb->subtopicid));
            $qb->topicid = $tmp_subtopic->category;
            $DB->update_record('b13_student_quiz_box', $qb);
        }
        $transaction->allow_commit();
        if ($isjson) {
            json::encode($qbs);
        } else {
            return true;
        }
    }

    public static function refresh_b13($isjson = true)
    {
        global $DB;
        //refresh_examboard_options
      b13util::refresh_examboard_options(false);
        //refresh_b13_student_quiz_box
      b13util::refresh_b13_student_quiz_box(false);
       

        //refresh enroll user
        $users = $DB->get_records('user');
        $sutopics = $DB->get_records('course');
        foreach($users as $user){
            foreach($courses as $course){
                b13students::enroll_user_subtopic($user->id, $course->id, false);
            }
        }

        if ($isjson) {
            json::encode("refreshed b13");
        } else {
            return true;
        }

      
    }

}
