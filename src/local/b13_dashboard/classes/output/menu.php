<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    17/11/2018 12:37
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard\output;

use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\menu_util;

/**
 * Class menu
 * @package local_b13_dashboard\output
 */
class menu {
    /**
     * @return string
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public static function create_menu() {
        global $DB, $CFG;

        $menu = "<ul class=\"main-menu block_tree list menu-b13\">";

        // $menu .= dashboard_util::add_menu(
        //     (new menu_util())->set_classname('dashboard')
        //     ->set_methodname('start')
        //     ->set_icon('dashboard')
        //     ->set_name(get_string_b13('dashboard')));
        $menu .= dashboard_util::add_menu(
            (new menu_util())->set_classname('b13examboards')
            ->set_methodname('dashboard')
            ->set_icon('examboard')
            ->set_name(get_string_b13('examboard_title')));
        $menu .= dashboard_util::add_menu(
                (new menu_util())->set_classname('b13subjects')
                ->set_methodname('dashboard')
                ->set_icon('newsubjectlist')
                ->set_name(get_string_b13('subject_title')));
        $menu .= dashboard_util::add_menu(
            (new menu_util())->set_classname('reports')
                ->set_methodname('dashboard')
                ->set_icon('report')
                ->set_name('Analytics'));
                // ->set_submenus(\local_b13_dashboard\reports::global_menus()));
            
        // if (has_capability('moodle/site:config', \context_system::instance()) && $CFG->dbtype == 'mysqli') {
        //     $menu .= dashboard_util::add_menu(
        //         (new menu_util())
        //         ->set_classname('backup')
        //         ->set_methodname('dashboard')
        //         ->set_icon('data')
        //         ->set_name(get_string_b13('backup_title')));
        // }

        // $menu .= dashboard_util::add_menu(
        //     (new menu_util())->set_classname('about')
        //     ->set_methodname('dashboard')
        //     ->set_icon('about')
        //     ->set_name(get_string_b13('about_title')));
        
        // $menu .= dashboard_util::add_menu(
        //     (new menu_util())->set_classname('dashboard')
        //     ->set_methodname('site_administration')
        //     ->set_icon('settings')
        //     ->set_name(get_string_b13('dashboard')));

        $menu .= "</ul>";

        return $menu;
    }
}