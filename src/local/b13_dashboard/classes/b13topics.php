<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/classes/editcategory_form.php';
require_once $CFG->dirroot . '/course/classes/category.php';

use core_course_category;
use local_b13_dashboard\b13subtopics;
use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use \stdClass;

/**
 * Class b13subtopics
 * @package local_b13_dashboard
 */
class b13topics
{
    /**
     * @throws \dml_exception
     * @throws \coding_exception
     */
    public function dashboard()
    {
        global $CFG;
        $subjectid = required_param('subjectid', PARAM_INT);
        $topics = b13topics::load_all_topics($subjectid, false);
        $getSubjectId = $_GET['subjectid'];
        $subjects = b13subjects::load_all_subjects(false);
        $arraysubjects = json_decode(json_encode($subjects), true);
        $array = json_decode(json_encode($topics), true);
        $subjectOptions = b13subjectoptions::load_all_options_by_subject($getSubjectId, false);
        $arraytopics = json_decode(json_encode($topics), true);
        $subjectOptions = b13subjectoptions::load_all_options_by_subject($getSubjectId, false);
        $load_all_topic_option_by_subject = b13subjectoptions::load_all_topic_option_by_subject($getSubjectId, false);
        $arrayAllTopicOptionBySubject = json_decode(json_encode($load_all_topic_option_by_subject), true);
        $arraySubjectOptions = json_decode(json_encode($subjectOptions), true);

        foreach ($arraytopics as &$topic) {
            foreach ($arrayAllTopicOptionBySubject as $optionsubject) {

                if ($topic['id'] == $optionsubject['categoryid']) {
                    $topic['seloption'] = $optionsubject['optionid'];

                }
            }
            if (!isset($topic['seloption'])) {
                $topic['seloption'] = -1;
            }
            $topic['options'] = $arraySubjectOptions;
        }
        ?>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="?classname=b13subjects&amp;method=dashboard">B13</a>
            </li>
            <li>
                <a href="?classname=b13subjects&amp;method=dashboard">Subject</a>
            </li>
            <li>
                <span>
                    <?php
                        foreach ($arraysubjects as $subject) {
                                    if ($subject['id'] == $getSubjectId) {
                                        echo $subject['name'];
                                    }
                        }
                    ?>
               </span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        dashboard_util::add_breadcrumb(get_string_b13('subject_title'));
        dashboard_util::add_breadcrumb(get_string_b13('topic_title'));
        dashboard_util::add_breadcrumb(get_string_b13('topic_list_title'));
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">
        <?php
        foreach ($arraysubjects as $subject) {
                    if ($subject['id'] == $getSubjectId) {
                        echo $subject['name'];
                    }
        }
        ?>
                    </h3>
        <?php
echo '<div class="add-new-b13-button">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addNewB13Model">
                                    Add New Topic
                                </button>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addNewSubjectOption">
                                            Add New Subject Option
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" style="width:100%;" class="btn btn-success" data-toggle="modal" data-target="#addTopicToOption">
                                            Add Topic Option
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#viewListOption">
                                            View Subject Options
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#ActionB13Model">
                                            <i class="fa fa-cog"></i> Action
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="element-box b13-list-page">
                <div class="container-fuild">
                    <div class="row b13-list-view">
                    ';
        if ($topics) {
            foreach ($arraytopics as $value) {
                echo '<div class="col-md-2 col-sm-6 visible_' . $value['visible'] . '">
                                <div class="b13-box">
                                    <a href="javascript:void(0)" onclick="getSubTopic(' . $value['parent'] . ',' . $value['id'] . ')">
                                        <i class="fa fa-book"></i>
                                        <h3 title=' . $value['name'] . '>' . $value['name'] . '</h3>
                                    </a>
                                </div>
                            </div>';
            }
        } else {
            echo '<div class="col-md-12"><p> Not Found </p></div>';
        }
        echo
            '</div>
                </div>
             </div>
             <div class="modal fade enterPress" id="addNewB13Model" tabindex="-1" role="dialog" aria-labelledby="addNewB13Model">
                <div class="modal-dialog" role="document">
                <form id="createNewTopic" class="createNewTopic" method="POST">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Add New Topic</h4>
                    </div>
                    <div class="modal-body">
                        <!-- NAME -->
                        <input type="hidden" value="' . $getSubjectId . '" name="subjectList"/>
                        <div id="name-group" class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control checkMaxLenth topicname" maxlength="50" name="name" placeholder="Topic Name">
                            <i class="label label-danger">Field must be Alphabetical or Numerical characters only</i>
                            <!-- errors will go here -->
                        </div>

                        <!-- Description -->
                        <div id="description-group" class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" placeholder="Description">
                            <!-- errors will go here -->
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default closeModal">Close</button>
                      <a href="javascript:void(0)" class="btn btn-success createNewTopicBtn submitButton">Create New Topic </a>
                    </div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="modal fade enterOptionPress" id="addNewSubjectOption" tabindex="-1" role="dialog" aria-labelledby="addNewSubjectOption">
                <div class="modal-dialog" role="document">
                <form id="createNewSubjectOption" class="createNewSubjectOption" method="POST">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Add New Subject Option</h4>
                    </div>
                    <div class="modal-body">
                        <!-- NAME -->
                        <input type="hidden" value="' . $getSubjectId . '" name="subjecid"/>
                        <div id="name-group" class="form-group">
                            <label for="name">Title</label>
                            <input type="text" class="form-control checkMaxLenth subjectoptiontitle" maxlength="50" name="title" placeholder="Option title">
                            <i class="label label-danger">Field must be Alphabetical or Numerical characters only</i>
                            <!-- errors will go here -->
                        </div>
                        <!-- description -->
                        <div id="description-group" class="form-group">
                            <label for="descriptionOption">Description</label>
                            <input type="text" class="form-control" name="descriptionOption" placeholder="Description">
                            <!-- errors will go here -->
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default closeModal">Close</button>
                      <a href="javascript:void(0)" class="btn btn-success createNewSubjectOptionBtn submitButton">Create New Subject Option </a>
                    </div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="addTopicToOption" tabindex="-1" role="dialog" aria-labelledby="addTopicToOption">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Add Topic Option</h4>
                    </div>
                    <div class="modal-body">
                    <div class="loadingAjax">
                        <div class="loading-subtopic-box">
                            <div class="loading-subtopic">
                                <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif"/>
                            </div>
                        </div>
                    </div>
                        <table class="table table-bordered" style="width:100%; table-layout:fixed;" cellpadding="2" cellspacing="0">
                            <thead>
                                <tr>
                                    <th width="60%"><i class="fa fa-book margin-right-10"></i>Topic Name</th>
                                    <th width="40%">Subject Option</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach ($arraytopics as $value) {
            echo '<tr class="subtopic_' . $value['id'] . '">
                                                    <td width="60%"><i class="fa fa-book margin-right-10"></i>' . $value['name'] . '</td>
                                                    <td width="40%">'; ?>
                                           <fieldset id="group<?php echo $value['id'] ?>" style="padding: 0;border: none;margin: 0;">
                                           <label class="defaultRadio hide" style="margin-right:5px;" onclick="addTopicToOption(<?php echo $value['id'] ?>,'0')" ><input name="group<?php echo $value['id'] ?>" type="radio" style="margin-right:5px;" value="0" > Default</label>
                            <?php
//print_r($value);
            foreach ($value['options'] as $valueOption) {
                //print_r($valueOption);
                if ($valueOption['id'] != $value['seloption']) {?>
                                            <label style="margin-right:5px;" ><input name="groupd<?php echo $value['id'] ?>" type="radio" style="margin-right:5px;" value="<?php echo $valueOption['id'] ?>" onclick="addTopicToOption(<?php echo $value['id'] ?>, <?php echo $valueOption['id'] ?>)"> <?php echo $valueOption['title'] ?></label>
                                        <?php
} else {?>
                                      <label style="margin-right:5px;" > <input name="groupd<?php echo $value['id'] ?>" type="radio" style="margin-right:5px;" checked value="<?php echo $valueOption['id'] ?>" onclick="addTopicToOption(<?php echo $value['id'] ?>, <?php echo $valueOption['id'] ?>)"><?php echo $valueOption['title'] ?></label>
                                    <?php }
            }
            echo '</fieldset>
                                </td>
                            </tr>';
        }
        echo '</tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>


              <div class="modal fade" id="viewListOption" tabindex="-1" role="dialog" aria-labelledby="viewListOption">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">View List Options</h4>
                    </div>
                    <div class="modal-body">
                    <div class="loadingAjax">
                        <div class="loading-subtopic-box">
                            <div class="loading-subtopic">
                                <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif"/>
                            </div>
                        </div>
                    </div>
                        <table class="table table-bordered" style="width:100%; table-layout:fixed;" cellpadding="2" cellspacing="0">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-book margin-right-10"></i>Option Name</th>
                                    <th style="width:20%">Edit</th>
                                    <th style="width:20%">Delete</th>
                                </tr>
                            </thead>
                            <tbody>';
        foreach ($arraySubjectOptions as $valueOption) {
            echo '<tr class="removeOption' . $valueOption['id'] . '">
                                <td ><i class="fa fa-book margin-right-10"></i>' . $valueOption['title'] . '</td>
                                <td>
                                    <a href="javascript:void(0)" class="editSubjectOption" onclick = "editSubjectOption(' . $valueOption['id'] . ')" data-toggle="modal" data-target="#editSubjectOption">Edit</a>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" class="deleteSubjectOption" onclick = "deleteSubjectOption(' . $getSubjectId . ',' . $valueOption['id'] . ')">Delete</a>
                                </td>
                            </tr>';
        }
        echo '</tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
             ';
        //edit new subject option

        echo '
                <div class="modal fade enterEditOptionPress" id="editSubjectOption" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                <form class="edit updateSubjectOption" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close closeCurrentModel" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Subject Option</h4>
                    </div>
                    <div class="modal-body"></div>
                    </form>
                </div>
                </div>
            </div>
            ';

        // Edit topic
        echo '<div class="modal fade " id="ActionB13Model" tabindex="-1" role="dialog" aria-labelledby="ActionB13Model">
            <div class="modal-dialog" role="document">
             <form id="ActionTopic" method="POST">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Action</h4>
                </div>
                <div class="modal-body">
                       <div class="loadingAjaxIcon" style="display: none;">
                            <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif">
                        </div>
                   <table class="table table-bordered">
                       <thead>
                           <tr>
                               <th><i class="fa fa-book margin-right-10"></i>Topic Name</th>
                               <th>Edit</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                           ';
        foreach ($arraytopics as $value) {
            echo '<tr class="topic_' . $value['id'] . '">
                               <td class="visible_' . $value['visible'] . '"><i class="fa fa-book margin-right-10"></i>' . $value['name'] . '</td>
                               <td class="visible_' . $value['visible'] . '"><a href="javascript:void(0)" data-toggle="modal" onclick="detailTopic(' . $value['id'] . ')" data-target="#updateTopic">Edit</a></td>
                               <td>'?>
                                      <?php if ($value['visible'] == 0) {
                echo '<a href="javascript:void(0)" onclick="enableTopic(' . $value['id'] . ')">Enable Topic</a>';
            } else {
                echo '<a href="javascript:void(0)" onclick="disableTopic(' . $value['id'] . ')">Disable Topic</a>';
            }?>
                                    <?php '</td>
                            </tr>';
        }
        echo '</tbody>
                   </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
              </div>
            </div>
            ';

        //update subtopics

        echo '
            <div class="modal fade enterPress" id="updateTopic" tabindex="-1" role="dialog" aria-labelledby="updateTopic">
               <div class="modal-dialog modal-lg" role="document">
                <form id="updateTopicForm" class="updateTopicForm" method="POST">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close closeCurrentModel" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title" id="myModalLabel">Edit Topic</h4>
                   </div>
                   <div class="modal-body"></div>
                   </form>
                 </div>
               </div>
            ';

        dashboard_util::end_page();
    }
    public static function load_all_topics($subjectid = null, $isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $data = $DB->get_records_sql(
            "SELECT cc.*, cc1.name as subject, cc1.id as subjectid
                FROM {course_categories} cc
                JOIN {course_categories} cc1
                ON cc.parent = cc1.id
                WHERE cc.depth = 2 AND cc.parent = " . $subjectid
        );
        if ($isjson == true) {
            json::encode($data);
        } else {
            return $data;
        }
    }


    public static function get_topic_progress($topicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        $data = $DB->get_record_sql("SELECT
                                    (SELECT SUM(readedpage) 
                                        FROM {b13_status_student_subtopic} 
                                        WHERE userid = $userid AND topicid = $topicid ) /
                                    (SELECT  COUNT(bc.id)
                                        FROM {book} b
                                        JOIN {book_chapters} bc
                                        ON b.id = bc.bookid
                                        JOIN {course} c
                                        ON c.id = b.course
                                        JOIN {course_categories} as coca
                                        ON coca.id = c.category
                                        WHERE coca.id = $topicid) AS topicbookstatus
                                    ");
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }
    }

    public function create_topic()
    {
        $name = required_param('name', PARAM_RAW);
        $description = required_param('description', PARAM_RAW);
        $parent = required_param('parent', PARAM_INT);
        $parentSubject = core_course_category::get($parent);
        if ($parentSubject) {
            $category = new stdClass();
            $category->parent = $parent;
            $category->name = $name;
            $category->description = $description;
            $data = core_course_category::create($category);
            // print_r($data);
            if ($data) {
                $category = new stdClass();
                $category->parent = $data->parent;
                $category->name = $data->name;
                $category->description = $data->description;
                $tmparray = array();
                array_push($tmparray, $category);
                json::encode($tmparray);
            }
        }
    }

    public function update_topic($topicid = null)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }
        $name = required_param('name', PARAM_RAW);
        $description = required_param('description', PARAM_RAW);

        $updatedata = array();
        $updatedata['id'] = $topicid;
        $updatedata['name'] = $name;
        $updatedata['description'] = $description;

        if ($DB->record_exists('course_categories', array('id' => $topicid))) {
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('course_categories', $updatedata);
            $transaction->allow_commit();
            json::encode("update successfully");
        } else {
            json::error("update fail!!!");

        }
    }

    public function detail_topic($topicid = null)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        if ($DB->record_exists('course_categories', array('id' => $topicid))) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->get_records('course_categories', array('id' => $topicid));
            $transaction->allow_commit();
            json::encode($data);
        } else {
            json::error("not have subject");

        }
    }

    public function disable_topic($topicid = null, $isjson = true)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        if ($DB->record_exists('b13_examboards_courses', array('topicid' => $topicid))) {
            if ($isjson) {
                json::error("this topic can not be disabled as its subtopic is belong to an examboard");
            } else {
                return false;
            }

        }

        if ($DB->record_exists('course_categories', array('id' => $topicid))) {
            $transaction = $DB->start_delegated_transaction();
            $updatedata = array();
            $updatedata['id'] = $topicid;
            $updatedata['visible'] = 0;
            $DB->update_record('course_categories', $updatedata);
            $subtopics = $DB->get_records('course', array('category' => $topicid));
            foreach ($subtopics as $subtopic) {
                b13subtopics::disable_subtopic($subtopic->id, false);
            }
            $transaction->allow_commit();
            if ($isjson) {
                json::encode("disable successfully");
            }

        } else {
            if ($isjson) {
                json::error("topic does not exist");
            }
        }
    }

    public function enable_topic($topicid = null, $isjson = true)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        if ($DB->record_exists('course_categories', array('id' => $topicid))) {
            $transaction = $DB->start_delegated_transaction();
            $updatedata = array();
            $updatedata['id'] = $topicid;
            $updatedata['visible'] = 1;
            $DB->update_record('course_categories', $updatedata);
            $subtopics = $DB->get_records('course', array('category' => $topicid));
            foreach ($subtopics as $subtopic) {
                b13subtopics::enable_subtopic($subtopic->id, false);
            }
            $transaction->allow_commit();
            if ($isjson) {
                json::encode("enable successfully");
            }

        } else {
            if ($isjson) {
                json::error("topic does not exist");
            }

        }

    }

}