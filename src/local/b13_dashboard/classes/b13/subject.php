<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard\b13;

defined('MOODLE_INTERNAL') || die();
use local_b13_dashboard\util\json;
/**
 * Class button
 * @package local_b13_dashboard\html
 */
class subject {

    public static function list_all_subjects(){
        global $DB;

        $data = $DB->get_records_sql(
            "SELECT c.id, c.fullname, c.shortname, c.visible,
                     (
                         SELECT COUNT( DISTINCT u.id )
                           FROM {user_enrolments} ue
                           JOIN {role_assignments} ra ON ue.userid = ra.userid
                           JOIN {enrol} e             ON e.id = ue.enrolid
                           JOIN {user} u              ON u.id = ue.userid
                          WHERE e.courseid = c.id
                            AND u.deleted = 0
                     ) AS inscritos
              FROM {course} c
             WHERE c.id > 1"
        );

        json::encode($data);
    }
    public static function list_all_student_subjects(){

    }

    public static function enroll_student_subject(){

    }
    public static function uneroll_student_subject(){

    }
}
