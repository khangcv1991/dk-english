<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();
require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->libdir.'/filelib.php';
require_once $CFG->libdir.'/gradelib.php';
require_once $CFG->libdir.'/completionlib.php';
require_once $CFG->libdir.'/plagiarismlib.php';
require_once $CFG->dirroot . '/course/modlib.php';
require_once $CFG->dirroot.'/mod/book/mod_form.php' ;
require_once $CFG->libdir.'/questionlib.php';

require_once 'b13subtopics.php';
use local_b13_dashboard\html\button;
use local_b13_dashboard\html\data_table;
use local_b13_dashboard\html\form;
use local_b13_dashboard\html\inputs\input_select;
use local_b13_dashboard\html\table_header_item;
use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\header;
use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use local_b13_dashboard\util\title_util;
use local_b13_dashboard\util\data_generator;
use local_b13_dashboard\vo\b13_dashboard_webpages;
use context_course;
use stdClass;
use mod_book_mod_form;
use \completion_info;
use moodle_exception;
use context_helper;
use context_module;
use core_tag_tag;
/**
 * Class b13quizmodule
 * @package local_b13_dashboard
 */
class b13quizmodule{
    public function update_learning_material(){
        global $CFG;
        $quiz = b13quizmodule::load_all_quizmodules(false);
        $subtopics = b13subtopics::load_all_subtopics(null,false);
        $getTopicID = $_GET['topicid'];
        $getSubjectID = $_GET['subjectid'];
        $arraySubtopic = json_decode(json_encode($subtopics), true);
        $getSubTopicId = $_GET['subtopicid'];
        ?>
          <ul class="breadcrumb custombreadcrumb">
              <li>
                  <a href="?classname=b13subjects&amp;method=dashboard">B13</a>
              </li>
              <li>
                  <a href="?classname=b13subjects&amp;method=dashboard"><?php echo current($arraySubtopic)['subject']?></a>
              </li>
              <li>
              <a href="/local/b13_dashboard/open-dashboard.php?classname=b13subtopics&method=dashboard&subjectid=<?php echo $getSubjectID?>"><?php echo current($arraySubtopic)['topic']?></a>
              </li>
              <li>
                    <?php
                    //print_r($arraySubtopic);
                        foreach ($arraySubtopic as $value) {
                            if ($value['id'] ===  $_GET['quizid']) {
                                echo $value['fullname'];
                            }
                        }
                    ?>
              </li>
          </ul>
          <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        dashboard_util::start_page();
        ?>
                    <h3 class="element-header">
                        <?php
                            foreach ($arraySubtopic as $value) {
                                if ($value['id'] ===  $_GET['quizid']) {
                                    echo $value['fullname'];
                                }
                            }
                        ?>
                    </h3>
                        <div id="update_learning_material_page">
                            <iframe id="frame" class="questionIframe" src="/mod/studentquiz/view.php?id=<?php echo $_GET['view']?>" width="100%" height="100%"></iframe>
                        </div>
    <?php    
        dashboard_util::end_page();
    }

    public static function load_all_quizmodules($isjson = true) {
        global $DB;
        $subtopicid = required_param('subtopicid', PARAM_INT);
      
        if (!empty($extrafields)) {
            $extrafields = ", $extrafields";
        }
        $params = array();
        $params['courseid'] = $subtopicid;
    
    
        $data =  $DB->get_records_sql("SELECT  m.*, cm.id as cid, md.*
                                       FROM {course_modules} cm, {modules} md, {studentquiz} m
                                      WHERE cm.course = :courseid AND
                                            cm.instance = m.id AND
                                            md.name = 'studentquiz' AND
                                            md.id = cm.module", $params);
        if($isjson ){                                            
            json::encode($data);
        }else{
            return $data;
        }
    }

    public static function create_quizmodules($subtopic = null, $isjson = true){
        global $DB;
        if($subtopic == null)
            $subtopic = required_param('subtopic', PARAM_INT);
        if($DB->record_exists('studentquiz', array('course'=> $subtopic))){
            if($isjson)
                json::encode("quiz exists");
            else
                return;
        }

        $moduleinfo =  new \stdClass();
        $moduleinfo->modulename = 'studentquiz';
        $moduleinfo->section = 0;
        $moduleinfo->course =  $subtopic ;
        $moduleinfo->visible = 1;
        $moduleinfo->visibleoncoursepage = 1;
        $moduleinfo->allowedqtypes = 'multichoice,truefalse,match,shortanswer,numerical,calculated,calculatedmulti,calculatedsimple,description,ddwtos,ddmarker,ddimageortext,multianswer,gapselect';
        $moduleinfo->excluderoles = '';
        $moduleinfo->introeditor = array('text' => 'This is a module', 'format' => FORMAT_HTML, 'itemid' => 0);

        // require_once($CFG->dirroot . '/course/modlib.php');
    
        // Check manadatory attributs.
        $mandatoryfields = array('modulename', 'course', 'section', 'visible');
        if (plugin_supports('mod', $moduleinfo->modulename, FEATURE_MOD_INTRO, true)) {
            $mandatoryfields[] = 'introeditor';
        }
        foreach($mandatoryfields as $mandatoryfield) {
            if (!isset($moduleinfo->{$mandatoryfield})) {
                throw new moodle_exception('createmodulemissingattribut', '', '', $mandatoryfield);
            }
        }
    
        // Some additional checks (capability / existing instances).
        $course = $DB->get_record('course', array('id'=>$moduleinfo->course), '*', MUST_EXIST);
        list($module, $context, $cw) = can_add_moduleinfo($course, $moduleinfo->modulename, $moduleinfo->section);
    
        // Add the module.
        $moduleinfo->module = $module->id;
        $moduleinfo = self::add_moduleinfo($moduleinfo, $course, null);
        $data =  $moduleinfo;
        if($isjson)
            json::encode(array( 0 => $data));
        else
            return $data;

    }
    function create_module($moduleinfo) {
        global $DB, $CFG;
    
        
    
        // Check manadatory attributs.
        $mandatoryfields = array('modulename', 'course', 'section', 'visible');
        if (plugin_supports('mod', $moduleinfo->modulename, FEATURE_MOD_INTRO, true)) {
            $mandatoryfields[] = 'introeditor';
        }
        foreach($mandatoryfields as $mandatoryfield) {
            if (!isset($moduleinfo->{$mandatoryfield})) {
                throw new moodle_exception('createmodulemissingattribut', '', '', $mandatoryfield);
            }
        }
    
        // Some additional checks (capability / existing instances).
        $course = $DB->get_record('course', array('id'=>$moduleinfo->course), '*', MUST_EXIST);
        list($module, $context, $cw) = can_add_moduleinfo($course, $moduleinfo->modulename, $moduleinfo->section);
    
        // Add the module.
        $moduleinfo->module = $module->id;
        $moduleinfo = $this->add_moduleinfo($moduleinfo, $course, null);
    
        return $moduleinfo;
    }
    
    public static function load_quiz_module($subtopicid = null, $isjson = true) {
        global $DB;
        if($subtopicid == null)
            $subtopicid = required_param('subtopicid', PARAM_INT);
      
        if (!empty($extrafields)) {
            $extrafields = ", $extrafields";
        }
        $params = array();
        $params['courseid'] = $subtopicid;
    
    
        $data =  $DB->get_records_sql("SELECT  m.*, cm.id as cid, md.*
                                       FROM {course_modules} cm, {modules} md, {studentquiz} m
                                      WHERE cm.course = :courseid AND
                                            cm.instance = m.id AND
                                            md.name = 'studentquiz' AND
                                            md.id = cm.module", $params);
        
        if(count($data) == 0){
            self::create_quizmodules($subtopicid, false);
        }
        if($isjson ){                                            
            json::encode($data);
        }else{
            return $data;
        }
    }


    private static function add_moduleinfo($moduleinfo, $course, $mform = null) {
        global $DB, $CFG;
        
        // Attempt to include module library before we make any changes to DB.
        include_modulelib($moduleinfo->modulename);
    
        $moduleinfo->course = $course->id;
        $moduleinfo = set_moduleinfo_defaults($moduleinfo);
    
        if (!empty($course->groupmodeforce) or !isset($moduleinfo->groupmode)) {
            $moduleinfo->groupmode = 0; // Do not set groupmode.
        }
    
        // First add course_module record because we need the context.
        $newcm = new stdClass();
        $newcm->course           = $course->id;
        $newcm->module           = $moduleinfo->module;
        $newcm->instance         = 0; // Not known yet, will be updated later (this is similar to restore code).
        $newcm->visible          = $moduleinfo->visible;
        $newcm->visibleold       = $moduleinfo->visible;
        $newcm->visibleoncoursepage = $moduleinfo->visibleoncoursepage;
        if (isset($moduleinfo->cmidnumber)) {
            $newcm->idnumber         = $moduleinfo->cmidnumber;
        }
        $newcm->groupmode        = $moduleinfo->groupmode;
        $newcm->groupingid       = $moduleinfo->groupingid;
        $completion = new completion_info($course);
        if ($completion->is_enabled()) {
            $newcm->completion                = $moduleinfo->completion;
            $newcm->completiongradeitemnumber = $moduleinfo->completiongradeitemnumber;
            $newcm->completionview            = $moduleinfo->completionview;
            $newcm->completionexpected        = $moduleinfo->completionexpected;
        }
        if(!empty($CFG->enableavailability)) {
            // This code is used both when submitting the form, which uses a long
            // name to avoid clashes, and by unit test code which uses the real
            // name in the table.
            $newcm->availability = null;
            if (property_exists($moduleinfo, 'availabilityconditionsjson')) {
                if ($moduleinfo->availabilityconditionsjson !== '') {
                    $newcm->availability = $moduleinfo->availabilityconditionsjson;
                }
            } else if (property_exists($moduleinfo, 'availability')) {
                $newcm->availability = $moduleinfo->availability;
            }
            // If there is any availability data, verify it.
            if ($newcm->availability) {
                $tree = new \core_availability\tree(json_decode($newcm->availability));
                // Save time and database space by setting null if the only data
                // is an empty tree.
                if ($tree->is_empty()) {
                    $newcm->availability = null;
                }
            }
        }
        if (isset($moduleinfo->showdescription)) {
            $newcm->showdescription = $moduleinfo->showdescription;
        } else {
            $newcm->showdescription = 0;
        }
        // From this point we make database changes, so start transaction.
        $transaction = $DB->start_delegated_transaction();
    
        if (!$moduleinfo->coursemodule = add_course_module($newcm)) {
            print_error('cannotaddcoursemodule');
        }
    
        if (plugin_supports('mod', $moduleinfo->modulename, FEATURE_MOD_INTRO, true) &&
                isset($moduleinfo->introeditor)) {
            $introeditor = $moduleinfo->introeditor;
            unset($moduleinfo->introeditor);
            $moduleinfo->intro       = $introeditor['text'];
            $moduleinfo->introformat = $introeditor['format'];
        }
    
        $addinstancefunction    = $moduleinfo->modulename."_add_instance";
       
        try {
           
            // $returnfromfunc = $addinstancefunction($moduleinfo, $mform);
          
            $moduleinfo->timecreated = time();
            $moduleinfo->timemodified = $moduleinfo->timecreated;
            if (!isset($moduleinfo->customtitles)) {
                $moduleinfo->customtitles = 0;
            }
            $returnfromfunc = $DB->insert_record('studentquiz', $moduleinfo);
            $completiontimeexpected = !empty($moduleinfo->completionexpected) ? $moduleinfo->completionexpected : null;
            \core_completion\api::update_completion_date_event($moduleinfo->coursemodule, 'studentquiz', $id, $completiontimeexpected);
        } catch (moodle_exception $e) {
       
            $returnfromfunc = $e;
        }
        if (!$returnfromfunc or !is_number($returnfromfunc)) {
            // Undo everything we can. This is not necessary for databases which
            // support transactions, but improves consistency for other databases.
            context_helper::delete_instance(CONTEXT_MODULE, $moduleinfo->coursemodule);
            $DB->delete_records('course_modules', array('id'=>$moduleinfo->coursemodule));
    
            if ($returnfromfunc instanceof moodle_exception) {
                throw $returnfromfunc;
            } else if (!is_number($returnfromfunc)) {
                print_error('invalidfunction', '', course_get_url($course, $moduleinfo->section));
            } else {
                print_error('cannotaddnewmodule', '', course_get_url($course, $moduleinfo->section), $moduleinfo->modulename);
            }
        }
    
        $moduleinfo->instance = $returnfromfunc;
    
        $DB->set_field('course_modules', 'instance', $returnfromfunc, array('id'=>$moduleinfo->coursemodule));
    
        // Update embedded links and save files.
        $modcontext = context_module::instance($moduleinfo->coursemodule);
        if (!empty($introeditor)) {
            // This will respect a module that has set a value for intro in it's modname_add_instance() function.
            $introeditor['text'] = $moduleinfo->intro;
    
            $moduleinfo->intro = file_save_draft_area_files($introeditor['itemid'], $modcontext->id,
                                                          'mod_'.$moduleinfo->modulename, 'intro', 0,
                                                          array('subdirs'=>true), $introeditor['text']);
            $DB->set_field($moduleinfo->modulename, 'intro', $moduleinfo->intro, array('id'=>$moduleinfo->instance));
        }
    
        // Add module tags.
        if (core_tag_tag::is_enabled('core', 'course_modules') && isset($moduleinfo->tags)) {
            core_tag_tag::set_item_tags('core', 'course_modules', $moduleinfo->coursemodule, $modcontext, $moduleinfo->tags);
        }
    
        // Course_modules and course_sections each contain a reference to each other.
        // So we have to update one of them twice.
        $sectionid = course_add_cm_to_section($course, $moduleinfo->coursemodule, $moduleinfo->section);
    
        // Trigger event based on the action we did.
        // Api create_from_cm expects modname and id property, and we don't want to modify $moduleinfo since we are returning it.
        $eventdata = clone $moduleinfo;
        $eventdata->modname = $eventdata->modulename;
        $eventdata->id = $eventdata->coursemodule;
        $event = \core\event\course_module_created::create_from_cm($eventdata, $modcontext);
        $event->trigger();
    
        $moduleinfo = edit_module_post_actions($moduleinfo, $course);
        $transaction->allow_commit();
    
        return $moduleinfo;
    }

    function delete_module(){
        $cmid = required_param('cmid', PARAM_INT);
        if(course_delete_module($cmid ) ){
            json::encode(array( array("message"=> "success")));
        }else{
            json::error();
        }
    }

    
    
}