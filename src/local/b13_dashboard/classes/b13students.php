<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/lib/enrollib.php';
require_once $CFG->dirroot . '/lib/questionlib.php';
require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/classes/editcategory_form.php';
require_once $CFG->dirroot . '/course/classes/category.php';
require_once $CFG->dirroot . '/course/externallib.php';

use context_course;
use context_module;
use local_b13_dashboard\b13examboards;
use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use local_b13_dashboard\b13log;
use local_b13_dashboard\b13util;
use local_b13_dashboard\b13srl;
use \stdClass;

/**
 * Class b13students
 * @package local_b13_dashboard
 */
class b13students
{
    /**
     * @throws \dml_exception
     * @throws \coding_exception
     */

    const MAX_QUESTION = 20;

    public function get_all_enrolled_subjects($userid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        // $data = $DB->get_records_sql(
        //     "SELECT coca.*, suic.icon
        //       FROM {b13_student_subject} stsu
        //       JOIN {course_categories} coca
        //       ON coca.id = stsu.categoryid
        //       LEFT JOIN {b13_subject_icon} suic
        //       ON suic.subjectid = coca.id
        //       WHERE  stsu.userid = " . $userid
        // );
        $data = $DB->get_records_sql(
            "SELECT coca.*
              FROM {course_categories} coca
              WHERE  coca.depth = 1"
        );

        // foreach ($data as &$su) {
        //     $su->bookstatus = $this->get_subject_progress($userid, $su->id, false);
        //     $su->quizstatus = b13srl::get_subject_quiz_progression($su->id, $userid, false);
        // }
        if ($isjson == true) {
            json::encode($data);

        } else {
            return $data;
        }

    }

    public function get_all_enrolled_subtopics_by_subject($subjectid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        $topics = $this->load_topics_by_student($subjectid, $userid, false);
        $resubtopics = array();
        foreach ($topics as $topic) {
            $resubtopics = array_merge($this->load_all_subtopics_by_student($topic->id, $userid, false), $resubtopics);
        }
        if ($isjson) {
            json::encode($resubtopics);
        } else {
            return $resubtopics;
        }
    }
    public function get_student_unenrolled_subjects($userid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        $data = $DB->get_records_sql(
            "SELECT coca.*, suic.icon
              FROM {course_categories} coca
              LEFT JOIN (
                  SELECT *
                  FROM {b13_student_subject} su
                  WHERE su.userid = $userid )stsu
              ON coca.id = stsu.categoryid
              LEFT JOIN {b13_subject_icon} suic
              ON suic.subjectid = coca.id
              WHERE  stsu.userid IS NULL " . " AND coca.depth = 1"
        );
        foreach($data as &$subject){
            $examboards = b13examboards::load_examboard_by_subject($subject->id, false);
            if( $examboards ==null || count($examboards) == 0){
                $subject->noexamboards = 0 ;
            }else{
                $subject->noexamboards = count($examboards) ;
            }
            
        }
        if ($isjson == true) {
            json::encode($data);
            // return $data;
        } else {
            return $data;
        }

    }
    public function load_all_subtopics_by_student($topicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        $topic = $DB->get_record("course_categories", array('id' => intval($topicid)));
        $subjectid = $topic->parent;
        $subject = $DB->get_record("course_categories", array('id' => intval($subjectid)));

        if ($subjectid) {
                $subtopics = $DB->get_records_sql("SELECT  c.*
                                                    FROM {course} c
                                                    WHERE c.visible = 1 AND c.category =" . $topicid);
                if ($isjson == true) {
                    json::encode($subtopics);
                } else {
                    return $subtopics;
                }
        } else {
            if ($isjson) {
                json::error("load_all_subtopics_by_student - subtioiccan not find subjectid");
            } else {
                return false;
            }

        }

    }

    public function load_topics_by_student($subjectid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        
       
            $data = $DB->get_records_sql(
                "SELECT cc.id, cc.name, cc.parent as subjectid
                    FROM {course_categories} cc
                    WHERE  cc.parent = $subjectid"
            );
        
        if ($isjson == true) {
            json::encode($data);
        } else {
            return $data;
        }
    }

    public function get_student_configuration_by_subject($userid = null, $subjectid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $examboards = b13examboards::load_examboard_by_subject($subjectid, false);
        $studentselections = $DB->get_records_sql(
            "SELECT  *
            FROM {b13_student_selection} ss
            WHERE ss.userid = $userid  AND ss.subjectid = $subjectid
            "
        );
        $selexamboards = array();
        $selex = current($studentselections)->examboardid;
        $options = b13examboards::load_options_by_examboard($selex, $subjectid, false);

        foreach ($examboards as $ex) {
            $ex->selected = 0;
            if ($selex == $ex->id) {
                $ex->selected = 1;
            }
            array_push($selexamboards, $ex);
        }
        $studentcofiguration = new \stdClass();
        $studentcofiguration->selexamboard = $selexamboards;
        $seloptions = array();
        foreach ($options as $option) {
            $option->selected = 0;
            foreach ($studentselections as $susel) {

                if ($option->id == $susel->optionid) {
                    $option->selected = 1;
                }

            }
            array_push($seloptions, $option);
        }
        $studentcofiguration->seloptions = $seloptions;
        $student_teacher = $DB->get_record('b13_students_teachers', array('userid' => $userid, 'subjectid' => $subjectid));
        $studentcofiguration->teacher = $student_teacher;
        // print_r($seloptions);
        if ($isjson) {
            json::encode(array(0 => $studentcofiguration));
        } else {
            return $studentcofiguration;
        }
    }

    // student learning material
    public static function get_status_student_subtopic($userid = null, $subtopicid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }

        $data = $DB->get_record('b13_status_student_subtopic', array('userid' => $userid, 'courseid' => $subtopicid));
        if ($data) {
            if ($isjson) {
                json::encode(array(0 => $data));
            } else {
                return $data;
            }

        } else {
            if ($isjson) {
                json::error("not have this configuration yet");
            } else {
                return null;
            }

        }
    }

    public static function get_book_page($userid = null, $subtopicid = null, $pageno = null)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }

        if ($pageno == null) {
            $pageno = required_param('pageno', PARAM_INT);
        }
        b13log::log_user_access_learning_subtopic($userid ,$subtopicid);
        b13students::enroll_user_subtopic($userid , $subtopicid, false);
        // $transaction = $DB->start_delegated_transaction();
        $selchapter = $DB->get_record_sql(
            "SELECT  bc.content,bc.title,  coca.id as topicid, coca.parent as subjectid, bc.id as chapterid, b.id as bookid, c.fullname as subtopic
            FROM {book} b
            JOIN {book_chapters} bc
            ON b.id = bc.bookid
            JOIN {course} c
            ON c.id = b.course
            JOIN {course_categories} as coca
            ON coca.id = c.category
            WHERE b.course = $subtopicid AND bc.pagenum = $pageno
            "
        );
        if ($selchapter == null) {
            json::error("This page does not exist. Please try a different one.");
        }
        $totalchapters = $DB->count_records_sql("SELECT COUNT(bc.id)
                                                    FROM {book} b
                                                    JOIN {book_chapters} bc
                                                    ON b.id = bc.bookid
                                                    JOIN {course} c
                                                    ON c.id = b.course
                                                    JOIN {course_categories} as coca
                                                    ON coca.id = c.category
                                                    WHERE b.course = :subtopicid", array('subtopicid' => $subtopicid));

        $subjectid = $selchapter->subjectid;
        $topicid = $selchapter->topicid;
        $modulecontext = $DB->get_record_sql("SELECT c.* 
                                        FROM {book} b 
                                        JOIN {course_modules} cm 
                                        ON cm.instance = b.id 
                                        JOIN {context} c 
                                        ON c.instanceid = cm.id 
                                        WHERE b.course = :subtopicid AND c.contextlevel = 70 AND cm.deletioninprogress = 0
                                         ", array('subtopicid' => $subtopicid));
        // $modulecontext = context_module::instance($selchapter->bookid);
        $selchapter->content = file_rewrite_pluginfile_urls($selchapter->content, 'pluginfile.php', $modulecontext->id, 'mod_book', 'chapter', $selchapter->chapterid, null);
        $selchapter->totalpages = $totalchapters;
        // if(!$DB->record_exists('b13_status_student_subtopic', array('userid' => $userid, 'courseid' => $subtopicid))){
        $ex_userbook = $DB->get_record('b13_status_student_subtopic', array('userid' => $userid, 'courseid' => $subtopicid));
        if ($ex_userbook) {
            $current_page = $ex_userbook->readedpage;
            if ($current_page < $pageno) {
                $ex_userbook->readedpage = $pageno;
                $DB->update_record('b13_status_student_subtopic', $ex_userbook);
            }
        } else {
            $userbook = new \stdClass();
            $userbook->userid = $userid;
            $userbook->courseid = $subtopicid;
            $userbook->subjectid = $subjectid;
            $userbook->topicid = $topicid;
            $userbook->moduleid = -1;
            $userbook->readedpage = $pageno;
            $userbook->readedquiz = 0;
            $userbook->quizstatus = 0;
            $userbook->totalpages = $totalchapters;
            $userbook->bookstatus = floatval($pageno / $totalchapters);
            $DB->insert_record('b13_status_student_subtopic', $userbook);
            // $transaction->allow_commit();

        }
        json::encode(array(0 => $selchapter));

    }

    public  function enroll_user_subject($userid = null, $subjectid = null)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $usersubject = new \stdClass();
        $usersubject->userid = $userid;
        $usersubject->categoryid = $subjectid;

        if (!$DB->record_exists('b13_student_subject', array('userid' => $userid, 'categoryid' => $subjectid)) && $DB->record_exists('course_categories', array('id' => $subjectid))) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->insert_record('b13_student_subject', $usersubject);
            $topics = $DB->get_records('course_categories', array('parent' => $subjectid));
            foreach ($topics as $topic) {
                b13students::enroll_user_topic($userid, $topic->id, false);
            }
            $usersubject2 = $DB->get_records('b13_student_subject', array('id' => $data));
            $transaction->allow_commit();
            json::encode($usersubject2);
        } else {
            json::error("the student already enrolled or subject does not exist!!!");
        }
    }

    public  function unenroll_user_subject($userid = null, $subjectid = null)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($DB->record_exists('b13_student_subject', array('userid' => $userid, 'categoryid' => $subjectid))) {
            $transaction = $DB->start_delegated_transaction();

            $DB->delete_records('b13_student_subject', array('userid' => $userid, 'categoryid' => $subjectid));
            $DB->delete_records('b13_student_selection', array('userid' => $userid, 'subjectid' => $subjectid));
            $topics = $DB->get_records('course_categories', array('parent' => $subjectid));
            foreach ($topics as $topic) {
                b13students::unenroll_user_topic($userid, $topic->id, false);
            }
            $transaction->allow_commit();

            b13srl::refresh_subject_progress($userid, $subjectid, false);
            json::encode(array(array("message" => "unenrolled successfully")));
        } else {
            json::error("the student already unenrolled or subject does not exist!!!");
        }
    }

    public static function enroll_user_topic($userid = null, $topicid = null, $isjson = true)
    {
        global $DB;

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        $usertopic = new \stdClass();
        $usertopic->userid = $userid;
        $usertopic->categoryid = $topicid;

        if (!$DB->record_exists('b13_student_subject', array('userid' => $userid, 'categoryid' => $subjectid)) && $DB->record_exists('course_categories', array('id' => $subjectid))) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->insert_record('b13_student_topic', $usertopic);
            $usertopic2 = $DB->get_records('b13_student_topic', array('id' => $data));
            $subtopics = $DB->get_records('course', array('category' => $topicid));
            foreach ($subtopics as $subtopic) {
                b13students::enroll_user_subtopic($userid, $subtopic->id, false);
            }
            $transaction->allow_commit();
            if ($isjson) {
                json::encode($usertopic2);
            } else {
                return true;
            }

        } else {
            if ($isjson) {
                json::error("the student already enrolled or topic does not exist!!!");
            } else {
                return false;
            }

        }
    }

    public static function unenroll_user_topic($userid = null, $topicid = null, $isjson = true)
    {
        global $DB;

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        if (!$DB->record_exists('b13_student_subject', array('userid' => $userid, 'categoryid' => $topicid)) && $DB->record_exists('course_categories', array('id' => $topicid))) {
            $transaction = $DB->start_delegated_transaction();
            $DB->delete_records('b13_student_topic', array('userid' => $userid, 'categoryid' => $topicid));
            $subtopics = $DB->get_records('course', array('category' => $topicid));
            foreach ($subtopics as $subtopic) {
                b13students::unenroll_user_subtopic($userid, $subtopic->id, false);
            }
            $transaction->allow_commit();
            if ($isjson) {
                json::encode(array(array("message" => "unenrolled successfully")));
            } else {
                return true;
            }

        } else {
            if ($isjson) {
                json::error("the student already enrolled or topic does not exist!!!");
            } else {
                return false;
            }

        }
    }

    public static function enroll_user_subtopic($userid = null, $subtopicid = null, $isjson = true)
    {
        global $DB;

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }


        $subtopic = $DB->get_record('course', array('id'=>$subtopicid));
        $manualplugin = enrol_get_plugin('manual');
        $studentroleid = $DB->get_field('role', 'id', ['shortname' => 'student'], MUST_EXIST);
        $instanceid = null;
        $instances = enrol_get_instances($subtopic->id, true);
        foreach ($instances as $inst) {
            if ($inst->enrol == 'manual') {
                $instanceid = (int)$inst->id;
                break;
            }
        }
        if (empty($instanceid)) {
            $instanceid = $manualplugin->add_default_instance($subtopic);
            if (empty($instanceid)) {
                $instanceid = $manualplugin->add_instance($subtopic);
            }
        }
        $instance = $DB->get_record('enrol', ['id' => $instanceid], '*', MUST_EXIST);
        $manualplugin->enrol_user($instance, $userid, $studentroleid, 0, 0, ENROL_USER_ACTIVE);

    }

    public static function unenroll_user_subtopic($userid = null, $subtopicid = null, $isjson = true)
    {
        global $DB;

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }

        $enrolplugin = enrol_get_plugin('manual');
        $instances = enrol_get_instances($subtopicid, true);

        foreach ($instances as $instance) {
            if ($instance->enrol === 'manual') {
                break;
            }
        }
        if ($instance->enrol !== 'manual') {
            throw new coding_exception('No manual enrol plugin in course');
        }

        $coursecontext = context_course::instance($subtopicid);
        if (!is_enrolled($coursecontext, $userid)) {
            if ($isjson) {
                json::encode("user not enrolled subtopic");
            } else {
                return true;
            }

        } else {
            $role = $DB->get_record('role', array('shortname' => 'student'), '*', MUST_EXIST);
            $enrolplugin->unenrol_user($instance, $userid);
            if ($isjson) {
                json::encode("enrolled subtopic successfully");
            } else {
                return false;
            }

        }

    }

    public function add_student_selection($userid = null, $subjectid = null, $options = null, $examboardid = null, $teacheremail = null, $teachername = null)
    {
        global $DB;

        if ($examboardid == null) {
            $examboardid = required_param('examboardid', PARAM_INT);
        }

        if ($subjectid == null) {
            $subjectid = $_POST['subjectid'] == null ? -1 : $_POST['subjectid'];
            // $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($options == null) {
            $options = $_POST['options'] == null ? -1 : $_POST['options'];
            // $optionid = required_param('optionid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = $_POST['userid'] == null ? -1 : $_POST['userid'];
            // $userid = required_param('userid', PARAM_INT);
        }
        if ($teacheremail == null) {
            $teacheremail = $_POST['teacheremail'] == null ? '' : $_POST['teacheremail'];
            // $optionid = required_param('optionid', PARAM_INT);
        }

        if ($teachername == null) {
            $teachername = $_POST['teachername'] == null ? '' : $_POST['teachername'];
            // $optionid = required_param('optionid', PARAM_INT);
        }
        $this->add_student_teacher($userid, $subjectid, $teachername, $teacheremail, null, false);
        $transaction = $DB->start_delegated_transaction();
        $DB->delete_records('b13_student_selection', array('userid' => $userid, 'subjectid' => $subjectid));
        if (isset($options) && $options != -1 && count($options) > 0) {
            foreach ($options as $option) {
                $studentselection = new \stdClass();
                $studentselection->userid = $userid;
                $studentselection->subjectid = $subjectid;
                $studentselection->examboardid = $examboardid;
                $studentselection->optionid = $option;
                $data = $DB->insert_record('b13_student_selection', $studentselection);

            }
        } else {
            $studentselection = new \stdClass();
            $studentselection->userid = $userid;
            $studentselection->subjectid = $subjectid;
            $studentselection->examboardid = $examboardid;
            $studentselection->optionid = -1;
            $data = $DB->insert_record('b13_student_selection', $studentselection);
        }

        $transaction->allow_commit();
        json::encode("successfully updated");
    }
    public function remove_student_selection($userid = null, $subjectid = null, $optionid = null, $examboardid = null)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }

        if (!$DB->record_exists('b13_student_selection', array('userid' => $userid, 'subjectid' => $subjectid, 'optionid' => $optionid, 'examboardid' => $examboardid))) {

            $transaction = $DB->start_delegated_transaction();
            $data = $DB->delete_record('b13_student_selection', array('userid' => $userid, 'subjectid' => $subjectid, 'optionid' => $optionid, 'examboardid' => $examboardid));
            $transaction->allow_commit();

            json::encode("remove successfully");
        } else {
            json::error("the setting does not exist!!!");
        }
    }

    public function add_student_teacher($userid = null, $subjectid = null, $teachername = null, $teacheremail = null, $teacherphone = null, $isjson = true)
    {
        global $DB;

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        if ($teacheremail == null) {
            $teacheremail = $_POST['teacheremail'];
        }
        if ($teachername == null) {
            $teachername = $_POST['teachername'];
        }

        if ($teacherphone == null) {
            $teacherphone = $_POST['teacherphone'];
        }
      

        $student_teacher = $DB->get_record('b13_students_teachers', array('userid' => $userid, 'subjectid' => $subjectid));
        if ($student_teacher) {
            if ($teacheremail != null) {
                $student_teacher->teacheremail = $teacheremail;
            }else{
                $student_teacher->teacheremail = "";
            }
            if ($teacherphone != null) {
                $student_teacher->teacherphone = $teacherphone;
            }
            if ($teachername != null) {
                $student_teacher->teachername = $teachername;
            }else{
                $student_teacher->teachername = "";
            }
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('b13_students_teachers', $student_teacher);
            $transaction->allow_commit();
            if ($isjson) {
                json::encode("Record updated successfully");
            } else {
                return true;
            }
        } else {
            $student_teacher = new \stdClass();
            $student_teacher->userid = $userid;
            $student_teacher->subjectid = $subjectid;
            $student_teacher->teacheremail = $teacheremail;
            $student_teacher->teacherphone = $teacherphone;
            $student_teacher->teachername = $teachername;

            $transaction = $DB->start_delegated_transaction();
            $student_teacher = $DB->insert_record('b13_students_teachers', $student_teacher);
            $transaction->allow_commit();
            if ($isjson) {
                json::encode(array(0 => $student_teacher));
            } else {
                return true;
            }
        }

    }

    public function list_schools($text = null, $isjson = true)
    {
        global $DB;
        $text = $_GET['text'];
        $data = $DB->get_records_sql("SELECT * FROM {b13_school} WHERE schoolname like '%$text%'");
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }
    }

   

    // public static function complete_quiz_test($userid = null, $subtopicid = null, $quizlist = null, $isjson = true)
    // {
    //     global $DB;
    //     if ($userid == null) {
    //         $userid = required_param('userid', PARAM_INT);
    //     }
    //     if ($subtopicid == null) {
    //         $subtopicid = required_param('subtopicid', PARAM_INT);
    //     }
    //     if ($quizlist == null) {
    //         $quizlist = required_param('quizlist', PARAM_RAW);
    //     }
    //     $selsubtopic = $DB->get_record('course', array('id'=>$subtopicid));
    //     $seltopicsubject = $DB->get_record_sql("SELECT coca.* FROM {course} c JOIN {course_categories} coca ON c.category = coca.id WHERE c.id = $subtopicid");
    //     $selexamboard = $DB->get_record('b13_student_selection', array('userid'=>$userid, 'subjectid'=>$seltopicsubject->parent ));
        
    //     $quizlist = json_decode($quizlist);

    //     $status_student_quiz = $DB->get_record('b13_student_progression_quiz', array('examboardid'=>$selexamboard->examboardid,'userid' => $userid, 'subtopicid' => $subtopicid));
    //     if ($status_student_quiz) {
         
    //         $DB->update_record('b13_student_progression_quiz', $status_student_quiz);
    //     } else {
    //         $status_student_quiz = new \stdClass();
    //         $status_student_quiz->userid = $userid;
    //         $status_student_quiz->subtopicid = $subtopicid;
    //         $status_student_quiz->quizlist = json_encode($quizlist);
    //         $status_student_quiz->time = 1;

    //         $transaction = $DB->start_delegated_transaction();
    //         $data = $DB->insert_record('b13_student_progression_quiz', $status_student_quiz);
    //         $transaction->allow_commit();
    //     }
    //     if ($isjson) {
    //         json::encode($correcttimes / count($quizlist));
    //     } else {
    //         return true;
    //     }
    // }

    public function generate_subtopic_quiz_list($subtopic = null, $userid = null, $isjson = true)
    {
        b13srl::generate_subtopic_quiz_list($subtopic, $userid, $isjson);
    }

    public function get_subtopic_quiz_progression($subtopicid = null, $userid = null, $isjson = true)
    {
       b13srl::get_subtopic_quiz_progression($subtopicid,$userid,$isjson);
    }

    public function get_topic_quiz_progression($topicid = null, $userid = null, $isjson = true)
    {
        b13srl::get_topic_quiz_progression($topicid, $userid , $isjson);
    }

    public function get_subject_quiz_progression($subjectid = null, $userid = null, $isjson = true)
    {
        b13srl::get_subject_quiz_progression($subjectid, $userid, $isjson);
    }

    public function get_subject_progress($userid = null, $subjectid = null, $isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        $subtopics = $this->get_all_enrolled_subtopics_by_subject($subjectid, $userid, false);

        if ($subtopics) {
            $condition = null;
            foreach ($subtopics as $subtopic) {
                if ($condition == null) {
                    $condition = "c.id = $subtopic->id ";
                } else {
                    $condition .= " OR c.id = $subtopic->id ";
                }
            }
            $data = $DB->get_record_sql("SELECT
                                        (SELECT SUM(readedpage)
                                            FROM {b13_status_student_subtopic} ststsu
                                            JOIN {course} c
                                            ON c.id = ststsu.courseid
                                            WHERE ($condition)  AND ststsu.userid = $userid ) /
                                        (SELECT  COUNT(bc.id)
                                            FROM {book} b
                                            JOIN {book_chapters} bc
                                            ON b.id = bc.bookid
                                            JOIN {course} c
                                            ON c.id = b.course
                                            WHERE $condition) AS subjectbookstatus
                                        ");

        } else {
            $data = $DB->get_record_sql("SELECT
                                        (SELECT SUM(readedpage)
                                            FROM {b13_status_student_subtopic} stsu
                                            WHERE stsu.userid = $userid AND stsu.subjectid = $subjectid ) /
                                        (SELECT  COUNT(bc.id)
                                            FROM {book} b
                                            JOIN {book_chapters} bc
                                            ON b.id = bc.bookid
                                            JOIN {course} c
                                            ON c.id = b.course
                                            JOIN {course_categories} as coca
                                            ON coca.id = c.category
                                            WHERE coca.parent = $subjectid) AS subjectbookstatus
                                        ");
        }
        if ($isjson) {
            json::encode($data->subjectbookstatus == null ? 0 : $data->subjectbookstatus);
        } else {
            return $data->subjectbookstatus;
        }

    }

    public function get_topic_progress($topicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        $subtopics = $this->load_all_subtopics_by_student($topicid, $userid, false);

        // print_r($subtopics);
        // return;

        if ($subtopics) {
            $condition = null;
            foreach ($subtopics as $subtopic) {
                if ($condition == null) {
                    $condition = "c.id = $subtopic->id ";
                } else {
                    $condition .= " OR c.id = $subtopic->id ";
                }
            }
            $data = $DB->get_record_sql("SELECT
                                        (SELECT SUM(readedpage)
                                            FROM {b13_status_student_subtopic} ststsu
                                            JOIN {course} c
                                            ON c.id = ststsu.courseid
                                            WHERE ($condition)  AND ststsu.userid = $userid ) /
                                        (SELECT  COUNT(bc.id)
                                            FROM {book} b
                                            JOIN {book_chapters} bc
                                            ON b.id = bc.bookid
                                            JOIN {course} c
                                            ON c.id = b.course
                                            WHERE $condition) AS topicbookstatus
                                        ");
        } else {
            $data = $DB->get_record_sql("SELECT
                                        (SELECT SUM(readedpage)
                                            FROM {b13_status_student_subtopic}
                                            WHERE userid = $userid AND topicid = $topicid ) /
                                        (SELECT  COUNT(bc.id)
                                            FROM {book} b
                                            JOIN {book_chapters} bc
                                            ON b.id = bc.bookid
                                            JOIN {course} c
                                            ON c.id = b.course
                                            JOIN {course_categories} as coca
                                            ON coca.id = c.category
                                            WHERE coca.id = $topicid) AS topicbookstatus
                                        ");
        }
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }
    }

    public function get_subtopic_progress($subtopicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        $data = $DB->get_record_sql("SELECT
        (SELECT readedpage
            FROM {b13_status_student_subtopic}
            WHERE userid = $userid AND courseid = $subtopicid ) /
        (SELECT  COUNT(bc.id)
            FROM {book} b
            JOIN {book_chapters} bc
            ON b.id = bc.bookid
            WHERE b.course = $subtopicid) AS subtopicbookstatus
        ");

        if ($isjson) {
            if ($data) {
                json::encode($data->subtopicbookstatus);
            } else {
                json::encode(0);
            }

        } else {
            if ($data) {
                return $data->subtopicbookstatus;
            } else {
                return 0;
            }

        }
    }
    public function create_content_correction(){
        global $DB;
        $mailto  = required_param('mailto', PARAM_RAW);
        $title = required_param('title', PARAM_RAW);
        $body = required_param('body', PARAM_RAW);
        $url = required_param('url', PARAM_RAW);
        $userid = required_param('userid', PARAM_INT);
        $subjectid = required_param('subjectid', PARAM_INT);
        $topicid = required_param('topicid', PARAM_INT);
        $subtopicid = required_param('subtopicid', PARAM_INT);
        $quizid = $_POST['quizid'];
        $questionid = $_POST['questionid'];
        $bookid = $_POST['bookid'];
        $chapterid = $_POST['bookid'];

            $correction = new stdClass();
            $correction->title = $data->id;
            $correction->body = $body;
            $correction->url = $url;
            $correction->userid = $userid;
            $correction->subjectid = $subjectid;
            $correction->topicid = $topicid;
            $correction->subtopicid = $subtopicid;
            $correction->quizid = $quizid;
            $correction->questionid = $questionid;
            $correction->bookid = $bookid;
            $correction->chapterid = $chapterid;

            $transaction = $DB->start_delegated_transaction();
            $DB->insert_record('b13_correction_report', $correction);
            $transaction->allow_commit();
            
        b13util::send_mail(array($mailto),$title, $body);
        json::encode("success");
    }
    public function update_content_correction($isjson = true){
        global $DB;
     
        $correctionid = required_param('correctionid', PARAM_INT);
        $status = required_param('status', PARAM_RAW);
        
        $updatedata = $DB->get_record('b13_correction_report', array('id'=>$correctionid));
        $updatedata->status = $status;

        $transaction = $DB->start_delegated_transaction();
        $DB->update_record('b13_correction_report', $updatedata);
        $transaction->allow_commit();
        if($isjson){
            json::encode(array(0=>$updatedata));
        }else{
            return true;
        }
    }
    public function get_content_corrections($isjson = true){
        global $DB;
        $issues = $DB->get_records("b13_correction_report");
        if($isjson){
            json::encode($issues);
        }else{
            return $issues;
        }
    }
}
