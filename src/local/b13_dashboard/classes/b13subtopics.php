<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/classes/editcategory_form.php';
require_once $CFG->dirroot . '/course/classes/category.php';
require_once $CFG->dirroot . '/course/externallib.php';
use local_b13_dashboard\b13learningmodules;
use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use \stdClass;

/**
 * Class b13subtopics
 * @package local_b13_dashboard
 */
class b13subtopics
{
    /**
     * @throws \dml_exception
     * @throws \coding_exception
     */
    public function dashboard()
    {
        global $CFG;
        $subjectid = required_param('subjectid', PARAM_INT);
        $topics = b13topics::load_all_topics($subjectid, false);
        $topicid = required_param('topicid', PARAM_INT);
        $topics = b13topics::load_all_topics($topicid, false);
        $subtopics = b13subtopics::load_all_subtopics($topicid, false);
        $getTopicID = $_GET['topicid'];
        $getSubjectID = $_GET['subjectid'];
        $array = json_decode(json_encode($subtopics), true);
        ?>
          <ul class="breadcrumb custombreadcrumb">
              <li>
                  <a href="?classname=b13subjects&amp;method=dashboard">B13</a>
              </li>
              <li>
                 <a href="?classname=b13subjects&amp;method=dashboard">Subject</a>
              </li>
              <li>
                <a href="/local/b13_dashboard/open-dashboard.php?classname=b13topics&method=dashboard&subjectid=<?php echo $getSubjectID?>"><?php echo current($array)['subject']?></a>
              </li>
              <li><span><?php echo current($array)['topic']?></span></li>
          </ul>
          <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        dashboard_util::add_breadcrumb(get_string_b13('subject_title'));
        dashboard_util::add_breadcrumb(get_string_b13('topic_title'));
        dashboard_util::add_breadcrumb(get_string_b13('topic_detail_title'));
        dashboard_util::start_page();
        ?>
            <h3 class="element-header">
                 <?php echo current($array)['topic'] ?>
            </h3>
        <?php
echo '<div class="add-new-b13-button">
                  <button type="button" class="btn btn-warning" data-toggle="modal" id="addNewSubtopicButton" data-target="#addNewB13Model">
                    Add New Sub Topic
                  </button>

                  <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#ActionB13Model">
                       <i class="fa fa-cog"></i> Action
                  </button>
            </div>
            <div class="element-box b13-list-page">
                <div class="container-fuild">
                    <div class="row b13-list-view">
                    ';
        //print_r($array);
        if ($subtopics) {
            //print_r($array);
            foreach ($array as $value) {
                echo '<div class="col-md-2 col-sm-6 subtopic_' . $value['id'] . '">
                                <div class="b13-box">
                                    <a href="javascript:void(0)" onclick="getLearningMaterial(' . $value['id'] . ',' . $getTopicID . ',' . $getSubjectID . ')">
                                        <i class="fa fa-book"></i>
                                        <h3 title=' . $value['fullname'] . '>' . $value['fullname'] . '</h3>
                                    </a>
                                </div>
                            </div>';
            }
        } else {
            echo '<div class="col-md-12"><p> Not Found </p></div>';
        }
        echo '</div>
                </div>
             </div>
             <div class="modal fade enterPress" id="addNewB13Model" tabindex="-1" role="dialog" aria-labelledby="addNewB13Model">
                <div class="modal-dialog" role="document">
                 <form id="createNewCourse" class="createNewSubtopic" method="POST">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Add New Subtopic</h4>
                    </div>
                    <div class="modal-body">
                        <!-- NAME -->
                        <input type="hidden" value="' . $getTopicID . '" id="getTopicId" name="topicid"/>

                        <div id="name-group" class="form-group">
                            <label for="fullname">Full Name</label>
                            <input type="text" class="form-control checkMaxLenth subtopicname" name="fullname" maxlength="50" placeholder="Full Name">
                            <i class="label label-danger">Field must be Alphabetical or Numerical characters only</i>
                            <!-- errors will go here -->
                        </div>

                        <!-- shortname -->
                        <div id="description-group" class="form-group">
                            <label for="shortname">Shortname</label>
                            <input type="text" class="form-control" name="shortname" placeholder="shortname">
                            <!-- errors will go here -->
                        </div>

                        <!-- code  -->
                        <div id="description-group" class="form-group">
                            <label for="idnumber">Code</label>
                            <input type="text" class="form-control" name="idnumber" placeholder="Unique Code">
                            <!-- errors will go here -->
                        </div>

                        <!-- code  -->
                        <div id="description-group" class="form-group">
                            <label for="summary">Summary</label>
                            <textarea class="form-control" name="summary" placeholder="summary"></textarea>
                            <!-- errors will go here -->
                        </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default closeModal">Close</button>
                      <a href="javascript:void(0)" class="btn btn-success createNewSubtopicBtn submitButton">Create New Subtopic </a>
                    </div>
                    </form>
                  </div>
                </div>
              </div>

            <div class="modal fade " id="ActionB13Model" tabindex="-1" role="dialog" aria-labelledby="ActionB13Model">
             <div class="modal-dialog" role="document">
              <form id="ActionSubtopic" method="POST">
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   <h4 class="modal-title" id="myModalLabel">Action</h4>
                 </div>
                 <div class="modal-body">
                        <div class="loadingAjaxIcon" style="display: none;">
                             <img src="/local/b13_dashboard/assets/dashboard/img/loading-ajax.gif">
                         </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><i class="fa fa-book margin-right-10"></i>Sub Topic Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            ';
        foreach ($array as $value) {
            echo '<tr class="subtopic_' . $value['id'] . '">
                                                    <td><i class="fa fa-book margin-right-10"></i>' . $value['fullname'] . '</td>
                                                    <td><a href="javascript:void(0)" data-toggle="modal" onclick="detailSubTopic(' . $getTopicID . ',' . $value['id'] . ')" data-target="#updateSubtopic">Edit</a></td>
                                                    <td><a href="javascript:void(0)" onclick="deleteSubTopic(' . $value['id'] . ')">Delete</a></td>
                                                </tr>';
        }
        echo '</tbody>
                    </table>
                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 </div>
                 </form>
               </div>
             </div>
             <div class="alert alert-success" id="success-alert">
                <strong>"successfully"! </strong>
            </div>
             ';

        //update subtopics

        echo '
             <div class="modal fade enterPress" id="updateSubtopic" tabindex="-1" role="dialog" aria-labelledby="updateSubtopic">
                <div class="modal-dialog modal-lg" role="document">
                 <form id="updateSubtopicForm" class="updateSubtopicForm" method="POST">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close closeCurrentModel" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Edit Sub Topic</h4>
                    </div>
                    <div class="modal-body"></div>
                    </form>
                  </div>
                </div>
             ';
        // print_r($subtopics);

        dashboard_util::end_page();
    }

    public static function load_all_subtopics($topicid = null, $isjson = true)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        $data = $DB->get_records_sql(
            "SELECT c.id, c.fullname, c.shortname, c.visible, coca2.name as subject,coca2.id as subjectid, coca1.name as topic, coca1.id as topicid
              FROM {course} c
              JOIN {course_categories} coca1
              ON c.category = coca1.id
              JOIN {course_categories} coca2
              ON coca1.parent = coca2.id
              WHERE  coca1.id = " . $topicid
        );
        if ($isjson == true) {
            json::encode($data);
            // return $data;

        } else {
            return $data;
        }
    }

    public static function load_all_subtopics_by_subject($subjectid = null, $isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $subtopics = $DB->get_records_sql(
            "SELECT c.id, c.fullname, c.shortname, c.visible, coca2.name as subject, coca1.name as topic
              FROM {course} c
              JOIN {course_categories} coca1
              ON c.category = coca1.id
              JOIN {course_categories} coca2
              ON coca1.parent = coca2.id
              WHERE  coca2.id = $subjectid"
        );

        foreach ($subtopics as $subtopic) {
            b13learningmodules::create_learningmodules($subtopic->id, false);
        }

        if ($isjson == true) {
            json::encode($subtopics);
            // return $data;

        } else {
            return $subtopics;
        }
    }

    public function create_subtopic()
    {
        global $DB;
        $topicid = required_param('topicid', PARAM_INT);
        $fullname = required_param('fullname', PARAM_RAW);
        $shortname = required_param('shortname', PARAM_ALPHANUMEXT);
        $idnumber = required_param('idnumber', PARAM_ALPHANUMEXT);
        $summary = required_param('summary', PARAM_RAW);

        $defaultcategory = $DB->get_field_select('course_categories', "MIN(id)", "parent=0");
        $course = new \stdClass();
        $course->fullname = $fullname;
        $course->shortname = $shortname;
        $course->idnumber = $idnumber;
        $course->summary = $summary;
        $course->summaryformat = FORMAT_PLAIN;
        $course->format = 'topics';
        $course->newsitems = 0;
        $course->category = $topicid;
        $created = create_course($course);
        b13learningmodules::create_learningmodules($created->id, false);
        json::encode(array(array('id' => $created->id, 'fullname' => $created->fullname, 'idnumber' => $created->idnumber, 'shortname' => $created->shortname, 'summary' => $created->summary, 'topicid' => $created->category)));

    }

    public function delete_subtopic()
    {
        global $DB;
        $subtopicid = required_param('subtopicid', PARAM_INT);

        if ($DB->record_exists('b13_examboards_courses', array('courseid' => $subtopicid))) {
            if ($isjson) {
                json::error("this subtopic can not be disabled as it is belong to an examboard");
            } else {
                return false;
            }

        }
        // Remove the actual course.
        $data = delete_course($subtopicid, false);
        if ($data) {
            json::encode(array(array("message" => "delete successfully")));
        } else {
            json::error("can not delete");
        }
    }

    public function update_subtopic()
    {
        global $DB;
        $topicid = required_param('topicid', PARAM_INT);
        $subtopicid = required_param('subtopicid', PARAM_INT);
        $fullname = required_param('fullname', PARAM_RAW);
        $shortname = required_param('shortname', PARAM_RAW);
        $idnumber = required_param('idnumber', PARAM_ALPHANUMEXT);
        $summary = required_param('summary', PARAM_RAW);

        if (!$DB->record_exists('course', array('id' => $subtopicid))) {
            json::error("subtopic does not exist");
        }
        $course = new \stdClass();
        $course->id = $subtopicid;
        $course->fullname = $fullname;
        $course->shortname = $shortname;
        $course->idnumber = $idnumber;
        $course->summary = $summary;
        $course->summaryformat = FORMAT_PLAIN;
        $course->format = 'topics';
        $course->newsitems = 0;
        $course->category = $topicid;
        update_course($course);
        $redata = $DB->get_records('course', array("id" => $subtopicid));
        json::encode($redata);
    }

    public static function disable_subtopic($subtopicid = null, $isjson = true)
    {
        global $DB;
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }


        if ($DB->record_exists('b13_examboards_courses', array('courseid' => $subtopicid))) {
            if ($isjson) {
                json::error("this subtopic can not be disabled as it is belong to an examboard");
            } else {
                return false;
            }

        }
        $course = new \stdClass();
        $course->id = $subtopicid;
        $course->visible = 0;
        update_course($course);

        if ($isjson) {
            $redata = $DB->get_records('course', array("id" => $subtopicid));
            json::encode($redata);
        }
    }

    public static function enable_subtopic($subtopicid = null, $isjson = true)
    {
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }
      
        $course = new \stdClass();
        $course->id = $subtopicid;
        $course->visible = 1;
        update_course($course);
        if ($isjson) {
            $redata = $DB->get_records('course', array("id" => $subtopicid));
            json::encode($redata);
        }
    }

    public function detail_subtopic()
    {
        global $DB;
        $subtopicid = required_param('subtopicid', PARAM_INT);

        if (!$DB->record_exists('course', array('id' => $subtopicid))) {
            json::error("subtopic does not exist");
        } else {
            $redata = $DB->get_records('course', array("id" => $subtopicid));
            json::encode($redata);
        }
    }
}
