<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/lib/enrollib.php';
require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->dirroot . '/course/classes/editcategory_form.php';
require_once $CFG->dirroot . '/course/classes/category.php';
require_once $CFG->dirroot . '/course/externallib.php';
require_once $CFG->dirroot . '/lib/enrollib.php';
require_once $CFG->dirroot . '/lib/questionlib.php';

use local_b13_dashboard\util\html;
use local_b13_dashboard\b13students;
use local_b13_dashboard\util\json;
use local_b13_dashboard\b13log;
use local_b13_dashboard\b13subtopics;
use \stdClass;
/**
 * Class b13students
 * @package local_b13_dashboard
 */
class b13srl
{
    const MAX_QUESTION = 20;
    public static function generate_subtopic_quiz_list($subtopic = null, $userid = null,$numberquestions = null, $isjson = true)
    {
        global $DB;

        if ($subtopic == null) {
            $subtopic = required_param('subtopic', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($numberquestions == null) {
            $numberquestions = required_param('numberquestions', PARAM_INT);
        }
        $data = $DB->get_records_sql("SELECT q.*, co.id as subtopicid, co.fullname as subtopic, cc.id as topicid, cc.name as topic, cc1.id as subjectid, cc1.name as subject, (SELECT 0) as answered, (SELECT 0) as attempted
                                        FROM {question} q
                                        JOIN {question_categories} qc
                                        ON q.category = qc.id
                                        JOIN {context} c
                                        ON qc.contextid = c.id
                                        JOIN {course_modules} cm
                                        ON cm.id = c.instanceid
                                        JOIN {course} co
                                        ON co.id = cm.course
                                        JOIN {course_categories} cc
                                        ON co.category = cc.id
                                        JOIN {course_categories} cc1
                                        ON cc.parent = cc1.id
                                        WHERE co.id = " . $subtopic. " ORDER BY RAND() LIMIT $numberquestions" );
      
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }
    public static function list_questions_by_subtopic($subtopic = null, $userid = null, $numberquestions = null,$isjson = true)
    {
        global $DB;
       
        if ($subtopic == null) {
            $subtopic = required_param('subtopic', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($numberquestions == null) {
            $numberquestions = required_param('numberquestions', PARAM_INT);
        }
        if($numberquestions <= 0){
            $numberquestions = 10;
        }
        $data = b13srl::generate_subtopic_quiz_list($subtopic, $userid,$numberquestions, false);
       
        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }
    }
    public static function save_quiz_progression($userid = null, $subtopicid = null, $quizlist = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }
        if ($quizlist == null) {
            $quizlist = required_param('quizlist', PARAM_RAW);
        }

        $selsubtopic = $DB->get_record('course', array('id'=>$subtopicid));
        $seltopicsubject = $DB->get_record_sql("SELECT coca.* FROM {course} c JOIN {course_categories} coca ON c.category = coca.id WHERE c.id = $subtopicid");
        $selexamboard = $DB->get_record('b13_student_selection', array('userid'=>$userid, 'subjectid'=>$seltopicsubject->parent ));
    
        $status_student_quiz = $DB->get_record('b13_student_progression_quiz', array('examboardid'=>$selexamboard->examboardid,'userid' => $userid, 'subtopicid' => $subtopicid));
        if ($status_student_quiz) {
            $status_student_quiz->quizlist = $quizlist;
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('b13_student_progression_quiz', $status_student_quiz);
            $transaction->allow_commit();
        } else {
            $status_student_quiz = new \stdClass();
            $status_student_quiz->userid = $userid;
            $status_student_quiz->subtopicid = $subtopicid;
            $status_student_quiz->quizlist = $quizlist;
            $status_student_quiz->time = 1;
            $status_student_quiz->completed = 0;
            $status_student_quiz->examboardid =  $selexamboard->examboardid;
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->insert_record('b13_student_progression_quiz', $status_student_quiz);
            $transaction->allow_commit();
        }

        if ($isjson) {
            json::encode("save successfully");
        } else {
            return true;
        }

    }
    public static function refresh_subject_progress($userid = null, $subjectid = null, $isjson = true){
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        $transaction = $DB->start_delegated_transaction();
        $subtopiclearningstatuses = $DB->get_records('b13_status_student_subtopic', array('userid'=>$userid,'subjectid' => $subjectid));
        foreach($subtopiclearningstatuses as &$subtopicstatus){
            $subtopicstatus->bookstatus = 0;
            $subtopicstatus->readedpage = 0;
            $DB->update_record('b13_status_student_subtopic', $subtopicstatus);
        }


        $subtopics = b13subtopics::load_all_subtopics_by_subject($subjectid, false);
        
        foreach ($subtopics as $sutopic) {
            $DB->delete_records('b13_student_progression_quiz', array("subtopicid"=>$sutopic->id, "userid"=>$userid));
        }

        $DB->delete_records('b13_student_qgroup_box', array("subjectid"=>$subjectid, "userid"=>$userid));
        $transaction->allow_commit();
        if($isjson ){
            return json::encode("success");
        }else{
            return true;
        }
    }

    public function detail_question($questionid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($questionid == null) {
            $questionid = required_param('questionid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        $question = $DB->get_record('question', array('id' => $questionid));
        
        
        $questioncategory = $DB->get_record('question_categories', array('id' => $question->category));
                
        $question->questiontextformat  = question_rewrite_question_preview_urls($question->questiontext, $question->id,
                                   $questioncategory->contextid, 'question', 'questiontext', $question->id,
                                   $questioncategory->contextid, 'quiz_statistics');
        $answers = $DB->get_records('question_answers', array('question' => $questionid), null, 'id,answer,answerformat,feedback, feedbackformat');
        shuffle($answers);
        $data = new \stdClass();
        $data->question = $question;
        $data->answers = array_values($answers);
        if ($isjson) {
            json::encode(array(0 => $data));
        } else {
            return $data;
        }
    }
    public static function check_question_answers($questionid = null, $answers = null, $userid = null, $subtopicid = null, $attempted = 0, $json = true)
    {
        global $DB;
        if ($questionid == null) {
            $questionid = required_param('questionid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }
        if ($answers == null) {
            $answers = $_POST['answers'];
        }
        if ($attempted == null) {
            $attempted = required_param('attempted', PARAM_INT);
        }


        if ($answers == null) {
            if ($json) {
                json::encode(0);
            } else {
                return false;
            }

        }
        $question = $DB->get_record('question', array('id' => $questionid));
        $questiongroup = $DB->get_record('b13_questiongroup_question', array('questionid'=>$question->id));
        // MCQ
        if ($question->qtype === "multichoice") {
            $qanswers = $DB->get_records('question_answers', array('question' => $questionid));
            $correct_answer = array();
            foreach ($qanswers as $answer) {
                if ($answer->fraction > 0) {
                    array_push($correct_answer, $answer->id);
                }
            }
            sort($correct_answer);
            sort($answers);
            if ($correct_answer == $answers) {

                // if ($attempted > 0) {
                //     b13srl::move_groupquestion_to_box($questiongroup->questiongroupid, $userid, $subtopicid, false);
                // } else {
                //     b13srl::move_groupquestion_to_box($questiongroup->questiongroupid, $userid, $subtopicid, true);
                // }
                if ($json) {
                    json::encode(1);
                } else {
                    return true;
                }

            } else {
                if ($json) {
                    json::encode(0);
                } else {
                    return false;
                }

            }
        }
        //True false
        if ($question->qtype === "truefalse") {
            $qanswers = $DB->get_records('question_answers', array('question' => $questionid));
            $correct_answer = array();
            foreach ($qanswers as $answer) {
                if ($answer->fraction == 1) {
                    array_push($correct_answer, $answer->id);
                }
            }
            if ($correct_answer == $answers) {
                // if ($attempted > 0) {
                //     b13srl::move_groupquestion_to_box($questiongroup->questiongroupid, $userid, $subtopicid, false);
                // } else {
                //     b13srl::move_groupquestion_to_box($questiongroup->questiongroupid, $userid, $subtopicid, true);
                // }
                if ($json) {
                    json::encode(1);
                } else {
                    return true;
                }

            } else {
                // b13srl::move_groupquestion_to_box($questiongroup->questiongroupid, $userid, $subtopicid, false);
                if ($json) {
                    json::encode(0);
                } else {
                    return false;
                }

            }
        }
        // shortanswer

        if ($question->qtype === "shortanswer") {
            $qanswers = $DB->get_records('question_answers', array('question' => $questionid));
            $regex = "";
            foreach ($qanswers as $qa) {
                $strs = explode(" ",$qa->answer);
                foreach($strs  as $str){
                    $regex .= "(?=.*".strtolower(trim( $str)). ")";
                }
            }
            
            if(count(preg_grep('/'.$regex.'/', explode("\n", strtolower($answers[0])))) > 0){
                // if ($attempted > 0) {
                //     b13srl::move_groupquestion_to_box($questiongroup->questiongroupid, $userid, $subtopicid, false);
                // } else {
                //     b13srl::move_groupquestion_to_box($questiongroup->questiongroupid, $userid, $subtopicid, true);
                // }
                if ($json) {
                    json::encode(1);
                } else {
                    return true;
                }
            }    
            if ($json) {
                json::encode(0);
            } else {
                return false;
            }
        }
    }
    public static function move_groupquestion_to_box($questiongroupid = null, $userid = null, $subtopicid = null, $correct = false)
    {
        // global $DB;
        // $qgquestion = $DB->get_record('b13_questiongroup', array('id'=>$questiongroupid));
        
        // $selexamboard = $DB->get_record('b13_student_selection', array('userid'=>$userid, 'subjectid'=>$qgquestion->subjectid ));
    
        // $bquiz = $DB->get_record('b13_student_qgroup_box', array('examboardid'=> $selexamboard->examboardid, 'userid' => $userid, 'subtopicid' => $subtopicid, 'questiongroupid' => $questiongroupid));

        // if ($bquiz) {
        //     if ($correct) {
        //         $bquiz->boxnumber < 5 ? $bquiz->boxnumber = intval($bquiz->boxnumber) + 1 : 5;
        //     } else if($bquiz->boxnumber == 5){
        //         $bquiz->boxnumber = 5;
        //     }else{
        //         $bquiz->boxnumber = 0;
        //     }
        //     $transaction = $DB->start_delegated_transaction();
        //     $data = $DB->update_record('b13_student_qgroup_box', $bquiz);
        //     $transaction->allow_commit();
        //     return $data;
        // } else {
        //     $student_quiz_box = new \stdClass();
        //     $student_quiz_box->userid = $userid;
        //     $student_quiz_box->subtopicid = $subtopicid;
        //     $student_quiz_box->topicid = $qgquestion->topicid;
        //     $student_quiz_box->subjectid = $qgquestion->subjectid;
        //     $student_quiz_box->subjectid = $qgquestion->subjectid;
        //     $student_quiz_box->examboardid =  $selexamboard->examboardid;
        //     $student_quiz_box->questiongroupid =  $qgquestion->id;
        //     if ($correct) {
        //         $student_quiz_box->boxnumber = 1;
        //     } else {
        //         $student_quiz_box->boxnumber = 0;
        //     }
           

        //     $transaction = $DB->start_delegated_transaction();
        //     $DB->insert_record('b13_student_qgroup_box', $student_quiz_box);
        //     $transaction->allow_commit();
        // }

    }
    public static function get_subtopic_quiz_progression($subtopicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }

        $selsubtopic = $DB->get_record('course', array('id'=>$subtopicid));
        $seltopicsubject = $DB->get_record_sql("SELECT coca.* FROM {course} c JOIN {course_categories} coca ON c.category = coca.id WHERE c.id = $subtopicid");
        $selexamboard = $DB->get_record('b13_student_selection', array('userid'=>$userid, 'subjectid'=>$seltopicsubject->parent ));
    

        $data = $DB->get_records("b13_student_qgroup_box", array('userid' => $userid, 'subtopicid' => $subtopicid, 'examboardid'=>$selexamboard->examboardid));
        $dataprogression = $DB->get_record("b13_student_progression_quiz", array('userid' => $userid, 'subtopicid' => $subtopicid, 'examboardid'=>$selexamboard->examboardid));
        $quizlist = json_decode($dataprogression->quizlist, true);
        if (isset($data) && isset($dataprogression)&& isset($quizlist) && count($quizlist) > 0) {
           
            $progress = 0;
            foreach ($data as $quiz) {
                $progress += $quiz->boxnumber;
            }
            if ($isjson) {
                json::encode($progress / (count($quizlist) * 5));
            } else {
                return $progress / (count($quizlist) * 5);
            }
        } else {
            if ($isjson) {
                json::error("There is no questions");
            } else {
                return 0;
            }
        }
    }
    public static function get_topic_quiz_progression($topicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        $seltopicsubject = $DB->get_record('course_categories', array('id'=>$topicid));
        $selexamboard = $DB->get_record('b13_student_selection', array('userid'=>$userid, 'subjectid'=>$seltopicsubject->parent ));
    


        $subtopics = b13srl::load_all_studentsubtopics_by_topic($topicid, $userid, false);
        if ($subtopics) {
            $condition = null;
            foreach ($subtopics as $subtopic) {
                if ($conditon == null) {
                    $conditon = "c.id = $subtopic->id ";
                } else {
                    $conditon .= " OR c.id = $subtopic->id ";
                }
            }
            $data = $DB->get_records_sql("SELECT stqb.*
                                            FROM {b13_student_qgroup_box} stqb
                                            JOIN {course} c
                                            ON stqb.subtopicid = c.id
                                            WHERE stqb.examboardid = $selexamboard->examboardid AND stqb.userid = $userid AND ($conditon)
                                            ");
        } else {
            $data = $DB->get_records_sql("SELECT stqb.*
                                            FROM {b13_student_quiz_box} stqb
                                            JOIN {course} c
                                            ON stqb.subtopicid = c.id
                                            WHERE stqb.examboardid = $selexamboard->examboardid AND stqb.userid = $userid AND c.category = $topicid
                                            ");
        }
        if ($data) {
            $progress = 0;
            foreach ($data as $quiz) {
                $progress += $quiz->boxnumber;
            }

            if ($isjson) {
                json::encode($progress / (count($data) * 5));
            } else {
                return $progress / (count($data) * 5);
            }
        } else {
            if ($isjson) {
                json::encode(0);
            } else {
                return 0;
            }
        }
    }
    public static function get_subject_quiz_progression($subjectid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $selexamboard = current($DB->get_records('b13_student_selection', array('userid'=>$userid, 'subjectid'=>$subjectid )));
    
        $subtopics = b13srl::load_studentsubtopics_by_subject($subjectid, $userid, false);
        
        foreach ($subtopics as $sutopic) {
                b13srl::generate_subtopic_quiz_list($sutopic->id,$userid, false);
        }
        if ($subtopics != null) {
            $condition = null;
            foreach ($subtopics as $sutopic) {
                
                if ($condition == null) {
                    $condition = "co.id = $sutopic->id ";
                } else {
                    $condition .= " OR co.id = $sutopic->id ";
                }
            }

            $data = $DB->get_records_sql("SELECT stqb.*
                FROM {b13_student_qgroup_box} stqb
                JOIN {course} co
                ON co.id = stqb.subtopicid
                WHERE  stqb.examboardid = $selexamboard->examboardid AND stqb.userid = $userid AND ($condition)
                ");
        } else {
            $data = $DB->get_records_sql("SELECT stqb.*
                                            FROM {b13_student_qgroup_box} stqb
                                            JOIN {course} c
                                            ON stqb.subtopicid = c.id
                                            JOIN {course_categories} coca
                                            ON coca.id = c.category
                                            WHERE  stqb.examboardid = :examboardid AND stqb.userid = :userid AND coca.parent = :subjectid
                                            ", array('userid' => $userid, 'subjectid' => $subjectid, 'examboardid'=>$selexamboard->examboardid));
        }
        if ($data) {
            $progress = 0;
            foreach ($data as $quiz) {
                $progress += $quiz->boxnumber;
            }

            if ($isjson) {
                json::encode($progress / (count($data) * 5));
            } else {
                return $progress / (count($data) * 5);
            }
        } else {
            if ($isjson) {
                json::encode(0);
            } else {
                return 0;
            }
        }
    }

    public static function get_student_configuration_by_subject($userid = null, $subjectid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $examboards = b13examboards::load_examboard_by_subject($subjectid, false);
        $studentselections = $DB->get_records_sql(
            "SELECT  *
            FROM {b13_student_selection} ss
            WHERE ss.userid = $userid  AND ss.subjectid = $subjectid
            "
        );
        $selexamboards = array();
        $selex = current($studentselections)->examboardid;
        $options = b13examboards::load_options_by_examboard($selex, $subjectid, false);

        foreach ($examboards as $ex) {
            $ex->selected = 0;
            if ($selex == $ex->id) {
                $ex->selected = 1;
            }
            array_push($selexamboards, $ex);
        }
        $studentcofiguration = new \stdClass();
        $studentcofiguration->selexamboard = $selexamboards;
        $seloptions = array();
        foreach ($options as $option) {
            $option->selected = 0;
            foreach ($studentselections as $susel) {

                if ($option->id == $susel->optionid) {
                    $option->selected = 1;
                }

            }
            array_push($seloptions, $option);
        }
        $studentcofiguration->seloptions = $seloptions;
        $student_teacher = $DB->get_record('b13_students_teachers', array('userid' => $userid, 'subjectid' => $subjectid));
        $studentcofiguration->teacher = $student_teacher;
        // print_r($seloptions);
        if ($isjson) {
            json::encode(array(0 => $studentcofiguration));
        } else {
            return $studentcofiguration;
        }
    }


    private static function get_questions_by_subtopic($subtopicid ){
        global $CFG, $DB;
        $data = $DB->get_records_sql('SELECT q.*
                                        FROM {question} q
                                        JOIN {question_categories} qc
                                        ON q.category = qc.id
                                        JOIN {context} c
                                        ON qc.contextid = c.id
                                        JOIN {course_modules} cm
                                        ON cm.id = c.instanceid
                                        JOIN {course} co
                                        ON co.id = cm.course
                                        WHERE  co.id = ' . $subtopicid );
        return $data;
    }
    
    public static function update_questiongroup($questionid) {
        global $CFG, $DB;
        $question = $DB->get_record('question', array('id'=> $questionid));
        $questioncategory = $DB->get_record('question_categories', array('id' => $question->category));
        $subtopic = $DB->get_record_sql("SELECT co.* 
                                        FROM {question_categories} qc
                                        JOIN {context} c
                                        ON qc.contextid = c.id
                                        JOIN {course_modules} cm
                                        ON cm.id = c.instanceid
                                        JOIN {course} co
                                        ON co.id = cm.course 
                                        WHERE qc.id  = $questioncategory->id ");
        $topic = $DB->get_record("course_categories", array('id' => $subtopic->category));
        $questiontitle = str_replace(' ', '',strtolower($question->name));
    
       
        if(! $DB->record_exists_select('b13_questiongroup', "title like '$questiontitle' AND subtopicid = $subtopic->id ")){
            $transaction = $DB->start_delegated_transaction();
            $updatedata = new \stdClass();
            $updatedata->title = $questiontitle;
            $updatedata->subtopicid = $subtopic->id;
            $updatedata->topicid = $topic->id;
            $updatedata->subjectid = $topic->parent;
            $DB->insert_record('b13_questiongroup', $updatedata);
            $transaction->allow_commit();
        }
        $questiongroup = $DB->get_record_sql("SELECT * FROM {b13_questiongroup} WHERE title like '$questiontitle'  AND subtopicid = $subtopic->id ");
    
        if(!$DB->record_exists('b13_questiongroup_question', array('questionid'=>$questionid, 'questiongroupid'=> $questiongroup->id))){
            $transaction = $DB->start_delegated_transaction();
            $questiongroupquestion = new \stdClass();
            $questiongroupquestion->questiongroupid = $questiongroup->id;
            $questiongroupquestion->questionid = $questionid;
            $questiongroupquestion->qtype = $question->qtype;
            $DB->insert_record('b13_questiongroup_question', $questiongroupquestion);
            $transaction->allow_commit();
            return true;
        }
    
        return false;
    }
    
    public static function refresh_questiongroup_question(){
        global $DB;
        $subtopics = $DB->get_records('course');
        foreach($subtopics as $subtopic){
            $questions = b13srl::get_questions_by_subtopic($subtopic->id);
            foreach($questions as $question){
                b13srl::update_questiongroup($question->id);
            }
        }
        
        json::encode("ok");
    }

    private static  function load_all_studentsubtopics_by_topic($topicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        $subject = $DB->get_records("course_categories", array('id' => intval($topicid)));

        $subjectid = current($subject)->parent;
        if ($subjectid) {
            $studentselections = $DB->get_records_sql(
                "SELECT  *
                FROM {b13_student_selection} ss
                WHERE ss.userid = $userid  AND ss.subjectid = $subjectid
                "
            );
            $ex = current($studentselections)->examboardid;
            if ($ex) {
                $subtopics = $DB->get_records_sql("SELECT  c.*, coca2.name as subject, coca1.name as topic, ststsu.bookstatus, ststsu.quizstatus,ststsu.readedpage,ststsu.readedquiz,ststsu.userid
                                            FROM {b13_examboards_courses} ec
                                            JOIN {course} c
                                            ON c.id = ec.courseid
                                            JOIN {course_categories} coca1
                                            ON c.category = coca1.id
                                            JOIN {course_categories} coca2
                                            ON coca1.parent = coca2.id
                                            LEFT JOIN {b13_status_student_subtopic} ststsu
                                            ON ststsu.courseid = c.id
                                            WHERE c.visible = 1 AND  ec.examboardid = " . $ex . " AND coca1.id =" . $topicid);

                if ($isjson == true) {
                    json::encode($subtopics);
                } else {
                    return $subtopics;
                }
            } else {
                if ($isjson) {
                    json::error("load_all_subtopics_by_student - can not find examboard");
                } else {
                    return false;
                }

            }
        } else {
            if ($isjson) {
                json::error("load_all_subtopics_by_student - subtioiccan not find subjectid");
            } else {
                return false;
            }

        }

    }

    public static function get_subtopic_book_progress($subtopicid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($subtopicid == null) {
            $subtopicid = required_param('subtopicid', PARAM_INT);
        }
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }

        $data = $DB->get_record_sql("SELECT
        (SELECT readedpage
            FROM {b13_status_student_subtopic}
            WHERE userid = $userid AND courseid = $subtopicid ) /
        (SELECT  COUNT(bc.id)
            FROM {book} b
            JOIN {book_chapters} bc
            ON b.id = bc.bookid
            WHERE b.course = $subtopicid) AS subtopicbookstatus
        ");

        if ($isjson) {
            if ($data) {
                json::encode($data->subtopicbookstatus);
            } else {
                json::encode(0);
            }

        } else {
            if ($data) {
                return $data->subtopicbookstatus;
            } else {
                return 0;
            }

        }
    }
    private static function load_studentsubtopics_by_subject($subjectid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        $topics = b13srl::load_studenttopics_by_subject($subjectid, $userid, false);
        $resubtopics = array();
        foreach ($topics as $topic) {
            $resubtopics = array_merge(b13srl::load_all_studentsubtopics_by_topic($topic->id, $userid, false), $resubtopics);
        }
        if ($isjson) {
            json::encode($resubtopics);
        } else {
            return $resubtopics;
        }
    }
    private static function load_studenttopics_by_subject($subjectid = null, $userid = null, $isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($userid == null) {
            $userid = required_param('userid', PARAM_INT);
        }
        $studentselections = $DB->get_records_sql(
            "SELECT  *
            FROM {b13_student_selection} ss
            WHERE ss.userid = $userid  AND ss.subjectid = $subjectid
            "
        );
        $condition = null;
        $i = 0;
        foreach ($studentselections as $susel) {
            $examboardid = $susel->examboardid;
            if ($i == 0 && $susel->optionid != -1) {
                $condition = " AND toop.optionid = " . $susel->optionid;
            } else if ($susel->optionid != -1) {
                $condition .= " OR toop.optionid = " . $susel->optionid;
            }
            $i++;
        }
        if ($examboardid != null){
            $examboard_condition = "exco.examboardid = $examboardid AND";
        }else {
            $examboard_condition = "";
        }
        $options = b13examboards::load_options_by_examboard($examboardid, $subjectid, false);
        if(($options != null || count($options) > 0 )&& $condition == null){
            if ($isjson == true) {
                json::encode(array());
            } else {
                return false;
            } 
        }
        if ($condition != null) {
            $data = $DB->get_records_sql(
                "SELECT  cc.id, cc.name, cc1.name as subject, cc.parent as subjectid
                            FROM {course_categories} cc
                            JOIN {course_categories} cc1
                            ON cc.parent = cc1.id
                            JOIN {b13_topic_option} toop
                            ON toop.categoryid = cc.id
                            JOIN {b13_examboards_courses} exco
                            ON exco.topicid = toop.categoryid
                            WHERE $examboard_condition cc.visible = 1 AND cc.depth = 2 AND cc.parent = " . $subjectid . $condition
            );
        } else {
            $data = $DB->get_records_sql(
                "SELECT cc.id, cc.name, cc1.name as subject, cc.parent as subjectid
                    FROM {course_categories} cc
                    JOIN {course_categories} cc1
                    ON cc.parent = cc1.id
                    JOIN {b13_examboards_courses} exco
                    ON exco.topicid = cc.id
                    WHERE  $examboard_condition cc.visible = 1 AND cc.depth = 2 AND cc.parent = " . $subjectid
            );
        }
        
        
        if ($isjson == true) {
            json::encode($data);
        } else {
            return $data;
        }
    }
}
