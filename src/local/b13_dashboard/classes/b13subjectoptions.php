<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//.
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    31/01/17 05:32
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();
require_once $CFG->dirroot . '/course/lib.php';
require_once $CFG->libdir . '/filelib.php';
require_once $CFG->libdir . '/gradelib.php';
require_once $CFG->libdir . '/completionlib.php';
require_once $CFG->libdir . '/plagiarismlib.php';
require_once $CFG->dirroot . '/course/modlib.php';
require_once $CFG->dirroot . '/mod/book/mod_form.php';
use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\html;
use local_b13_dashboard\util\json;
use local_b13_dashboard\b13util;
use stdClass;

/**
 * Class b13subjectoptions
 * @package local_b13_dashboard
 */
class b13subjectoptions
{   
    /**
     * @throws \dml_exception
     * @throws \coding_exception
     */
    public static function load_all_options_by_subject($subjectid = null,$isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
       
        $data = $DB->get_records_sql("SELECT  o.* 
                                        FROM {b13_subject_option} so
                                        JOIN {b13_option} o
                                        ON o.id = so.optionid
                                        WHERE so.categoryid = " . $subjectid);

        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }
    public static function  load_all_topic_option_by_subject($subjectid = null,$isjson = true){
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        $data = $DB->get_records_sql("SELECT  toop.*, o.title,o.id as optionid
                                        FROM {b13_topic_option} toop
                                        JOIN {b13_option} o
                                        ON toop.optionid = o.id
                                        JOIN {course_categories} coca
                                        ON coca.id = toop.categoryid
                                        WHERE coca.parent = " . $subjectid);

        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }
    }

    public static function load_all_topic_option($subjectid = null, $isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        
        $data = $DB->get_records_sql("SELECT  so.* 
                                        FROM {b13_subject_option} o
                                       
                                        WHERE so.categoryid = " . $subjectid 
                                        );

        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }

    public static function load_all_topics($subjectid = null,$optionid = null, $isjson = true)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }
       
        $data = $DB->get_records_sql("SELECT  coca.* 
                                        FROM {b13_option} o
                                        JOIN {b13_subject_option} so
                                        ON o.id = so.optionid
                                        JOIN {course_category} coca
                                        ON coca.id = so.categoryid
                                        WHERE so.categoryid = " . $subjectid . " AND o.id =  $optionid"
                                        );

        if ($isjson) {
            json::encode($data);
        } else {
            return $data;
        }

    }
    public function create_option($subjectid = null)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }
        $title = required_param('title', PARAM_RAW);
        $description = required_param('description', PARAM_RAW);

        $option = new \stdClass();
        $option->title = $title;
        $option->description = $description;
    if (!$DB->record_exists_sql("SELECT * FROM {b13_option} o 
                                JOIN {b13_subject_option} so 
                                ON o.id = so.categoryid 
                                WHERE o.title = '$title'
                                 AND so.categoryid = $subjectid")) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->insert_record('b13_option', $option);
            $optionsubject = new \stdClass();
            $b13_student_subject->optionid = $data;
            $b13_student_subject->categoryid = $subjectid;
            $data2 = $DB->insert_record('b13_subject_option', $b13_student_subject);

            $option2 = $DB->get_records('b13_option', array('id' => $data));
            $transaction->allow_commit();
            json::encode($option2);
        } else {
            json::error("the examboard exists!!!");
        }

    }

   
    public function remove_option($subjectid = null, $optionid = null)
    {
        global $DB;
        if ($subjectid == null) {
            $subjectid = required_param('subjectid', PARAM_INT);
        }

        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }

        if ($DB->record_exists('b13_subject_option', array('categoryid' => $subjectid, 'optionid' => $optionid))) {
            $transaction = $DB->start_delegated_transaction();
            //b13_student_learning_status
            $DB->delete_records('b13_subject_option', array('categoryid' => $subjectid, 'optionid' => $optionid));
            $transaction->allow_commit();
            json::encode("delete successfully");
        } else {
            json::encode("the setting not exists!!!");
        }

    }

    public function update_option( $optionid = null){
        global $DB;
        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }
        $title = required_param('title', PARAM_RAW);
        $description = required_param('description', PARAM_RAW);

        $updatedata = array();
        $updatedata['id'] = $optionid ;
        $updatedata['title'] = $title ;
        $updatedata['description'] = $description ;

       
        if ($DB->record_exists('b13_option', array('id'=>$optionid ))) {
            $transaction = $DB->start_delegated_transaction();
            $DB->update_record('b13_option', $updatedata);
            $transaction->allow_commit();  
            json::encode("update successfully");                               
         }else{
            json::error("update fail!!!");

         }

    }

    public function detail_option( $optionid = null){
        global $DB;
        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }
        $data = $DB->get_records('b13_option', array('id'=> $optionid));
        json::encode($data);       
    }
    public function add_option_topic($topicid = null, $optionid = null)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }

        $topicoption = new \stdClass();
        $topicoption->categoryid = $topicid;
        $topicoption->optionid = $optionid;

            $transaction = $DB->start_delegated_transaction();
            $DB->delete_records('b13_topic_option', array('categoryid' => $topicid));
            $data = $DB->insert_record('b13_topic_option', $topicoption);
            $topicoption2 = $DB->get_records('b13_topic_option', array('categoryid' => $topicid, 'optionid' => $optionid));
            b13util::refresh_examboard_options(false);
            $transaction->allow_commit();
            json::encode($topicoption2);

    }

    public function remove_option_topic($topicid = null, $optionid = null)
    {
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }
        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }

        if (!$DB->record_exists('b13_topic_option', array('topicid' => $topicid, 'optionid' => $optionid))) {
            $transaction = $DB->start_delegated_transaction();
            $data = $DB->delete_records('b13_topic_option', array('topicid' => $topicid, 'optionid' => $optionid));
            b13util::refresh_examboard_options(false);
            $transaction->allow_commit();
            json::encode("delete successfully");
        } else {
            json::error("the topic option not exists!!!");
        }

    }



    public function add_student_selection($userid = null, $subjectid = null, $optionids = null){
        global $DB;
        if ($topicid == null) {
            $topicid = required_param('topicid', PARAM_INT);
        }

        if ($optionid == null) {
            $optionid = required_param('optionid', PARAM_INT);
        }


    }

}
