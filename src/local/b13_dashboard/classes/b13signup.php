<?php

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/user/editlib.php';
require_once $CFG->libdir . '/authlib.php';
require_once $CFG->dirroot . '/login/lib.php';
require_once $CFG->dirroot.'/auth/manual/auth.php';
use local_b13_dashboard\util\json;
use \auth_plugin_manual;
/**
 * Class b13signup
 * @package local_b13_dashboard
 */
class b13signup
{
    public function signup(){
        global $DB;
        $fullname = required_param('fullname', PARAM_RAW);
        $username = required_param('username', PARAM_ALPHANUMEXT);
        $password = required_param('password', PARAM_RAW);
        $email = required_param('email', PARAM_RAW);
        $school = required_param('school', PARAM_RAW);

        $user = array(
            'username' =>  $username,
            'password' => $password,
            'idnumber' => $username,
            'firstname' => $username,
            'lastname' => $username,
            'middlename' => $username,
            'alternatename' => $fullname,
            'email' => $email,
            'description' => "This is a description for $username",
            'city' => 'London',
            'country' => 'UK',
            'institution' => $school,
            'mnethostid' => 1,
            'auth' => 'manual',
            'confirmed' => 1
            );
        // $authplugin = signup_is_enabled();
        $authplugin = new auth_plugin_manual();
        if ($DB->record_exists('user', array('username' => $user['username']))) {
            json::error("Sorry this username already exists!!");
        }else if($DB->record_exists('user', array('email' => $user['email']))){
            json::error("Sorry this email address already exists!!");
        }   
        else{
            // $authplugin->user_signup((object)$user, false); 
            $userid = $DB->insert_record('user', (object)$user);
            $user = $DB->get_record('user', array('id'=>$userid));
            $authplugin->user_update_password($user, $password);
            json::encode("successfully");
        }
      
    }
}