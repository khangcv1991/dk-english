<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @created    13/05/17 13:29
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_b13_dashboard;

defined('MOODLE_INTERNAL') || die();

use local_b13_dashboard\html\data_table;
use local_b13_dashboard\html\table_header_item;
use local_b13_dashboard\html\button;
use local_b13_dashboard\util\dashboard_util;
use local_b13_dashboard\util\header;
use local_b13_dashboard\util\json;
use local_b13_dashboard\util\submenu_util;
use local_b13_dashboard\util\title_util;
use local_b13_dashboard\util\export;
use local_b13_dashboard\vo\b13_dashboard_reportcat;
use local_b13_dashboard\vo\b13_dashboard_reports;
use local_b13_dashboard\b13students;


/**
 * Class reports
 * @package local_b13_dashboard
 */
class reports {

    /**
     * @return array of submenu_util
     * @throws \dml_exception
     * @throws \coding_exception
     */
    public static function global_menus() {
        global $DB, $CFG;

        $menus = array();

        $b13reportcats = $DB->get_records('b13_dashboard_reportcat', array('enable' => 1));
      
        $icon = "{$CFG->wwwroot}/local/b13_dashboard/assets/dashboard/img/icon/report.svg";
        $menus[] = (new submenu_util())
                ->set_classname('reports')
                ->set_methodname('teacher_report_dashboard')
                ->set_title("Teacher email")
                ->set_icon($icon);

        return $menus;
    }


    /**
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public function dashboard() {
        global $CFG, $DB;
        ?>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="?classname=b13subjects&amp;method=dashboard">B13</a>
            </li>
            <li>
                <span>Analytics</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        dashboard_util::add_breadcrumb(get_string_b13('reports_title'));
        dashboard_util::start_page();?>
        <h3 class="element-header">Analytics</h3>
        <div class="element-box report-page">
            <div class="container-fluid">
                <div class="row report-button-box">
                    <div class="col-md-6">
                        <!-- <button class="btn btn-success" onClick="getUserSignedUp()">Users signed-up</button>
                        <button class="btn btn-success" onClick="getUserFromEachSchool()">Users from each school</button>
                        <button class="btn btn-success" onClick="getUserVisited()">Users revisited</button>
                        <button class="btn btn-success" onClick="getUserAccessedLearningContent()">Users accessed learning content</button>
                        <button class="btn btn-success" onClick="getUserCompletedTest()">Users completed test</button> -->

                        <button class="btn btn-success"><a href="?classname=reports&method=download_user_signed_up_report" style="color:#fff">Users signed-up</a></button>
                        <button class="btn btn-success"><a href="?classname=reports&method=download_user_from_each_school_report" style="color:#fff">Users from each school</a></a>
                        <button class="btn btn-success" onClick="getUserVisited()">Users revisited</button>
                        <button class="btn btn-success"><a href="?classname=reports&method=download_users_accessed_learning_content_report" style="color:#fff">Users accessed learning content</a></button>
                        <button class="btn btn-success"><a href="?classname=reports&method=download_users_completed_test_report" style="color:#fff">Users completed test </a></button>
                    </div>
                    <div class="col-md-6">
                        <!-- <button class="btn btn-success" onClick="getQuestionAnsweredIncorrectly()">Questions answered incorrectly over 60%</button> -->
                        <!-- <button class="btn btn-success" onClick="getTopicAccessed()">Topic accessed</button>
                        <button class="btn btn-success" onClick="getSubtopicAccessed()">Subtopic accessed</button>
                        <button class="btn btn-success" onClick="getTeacherEmail()">Teacher's email</button>
                        <button class="btn btn-success" onClick="getBookReport()">Report book's issues from students</button> -->

                        <button class="btn btn-success"><a href="?classname=reports&method=download_topic_accessed_report" style="color:#fff">Topic accessed</a></button>
                        <button class="btn btn-success"><a href="?classname=reports&method=download_subtopic_accessed_report" style="color:#fff">Subtopic accessed </a></button>
                        <button class="btn btn-success"><a href="?classname=reports&method=download_teacher_report" style="color:#fff">Teacher's email</a></button>
                        <button class="btn btn-success" onClick="getBookReport()">Report book's issues from students</button>
                    </div>
                </div>
            </div>
        </div>
    <?php
        dashboard_util::end_page();
    }

    public function teacher_report_dashboard(){
        global $CFG;
        $teacherEmail = reports::load_teachers(false);
        ?>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Teacher's email</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Teacher's email</h3>
            <div class="element-box">
                    <div id="teacherEmail" class="report-box teacherEmail">
                             <h3 style="font-size:20px;padding-right:10px;">Total teacher's email  <small class="count teacheremailItem"></small></h3>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar teacherEmail">
                                <table class="table table-bordered table-striped mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width:40px;text-align:center">No</th>
                                            <th scope="col">Teacher Name</th>
                                            <th scope="col">Teacher Email</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
		                    <div id="pagination"></div>
                            <a href="?classname=reports&method=download_teacher_report" class="btn btn-primary downloadTeacherBtn ">Download</a>
                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

    public function download_teacher_report(){
        export::header('xls', "Teacher report");
        global $DB;
        $teachers = $DB->get_records('b13_students_teachers');
        $columns = array(
            "Teacher name",
            "Teacher email",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        foreach($teachers as $row){
            echo '<tr>';
            echo " <td>".$row->teachername. "</td>";
            echo " <td>".$row->teacheremail. "</td>";
            echo '</tr>';
        }
        echo '</tr>';
        echo '</table>';
        export::close();
    }
    public function download_number_user_access_day($time){
        export::header('xls', "b13 User Visited");
        $time = required_param('time', PARAM_INT);
        $data = $this->report_number_user_access_day($time, false);
        $columns = array(
            "date",
            "number of users",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        echo '<tr>';
        echo " <td>".$data . "</td>";
        echo '</tr>';
        echo '</table>';
        export::close();
    }

    public function download_user_signed_up_report(){
        export::header('xls', "Users signed up'");
        $data = $this->load_users(false);
        $columns = array(
            "username",
            "firstname",
            "lastname",
            "email",
            "school",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        foreach($data as $row){
        echo '<tr>';
        echo " <td>".$row->username . "</td>";
        echo " <td>".$row->firstname . "</td>";
        echo " <td>".$row->lastname . "</td>";
        echo " <td>".$row->email . "</td>";
        echo " <td>".$row->institution . "</td>";
        echo '</tr>';
        }
        echo '</table>';
        export::close();
    }

    public function download_number_user_revisit(){
        $startdate = required_param('startdate', PARAM_INT);
        $numberofdays = required_param('numberofday', PARAM_INT);
        $data = $this->report_number_user_revisit($startdate/1000,$numberofdays, false );
        
        $columns = array(
            "times",
            "number of users"
        );
       
        export::header('xls', "User revisit");
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        foreach($data as $row){
        echo '<tr>';
        echo " <td>". date('d/m/y',  intval($row['time'] + 24*60*60)) . "</td>";
        echo " <td>".$row['nousers'] . "</td>";
        echo '</tr>';
        }
        echo '</table>';
        // export::close();
    }
    public function download_user_from_each_school_report(){
        export::header('xls', "User from each school");
        $data = $this->report_total_users_from_school(false);
        $columns = array(
            "school",
            "number of user",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        foreach($data as $row){
            echo '<tr>';
            echo " <td>".$row->institution . "</td>";
            echo " <td>".$row->totalstudents . "</td>";
            echo '</tr>';
        }
        echo '</table>';
        export::close();
    }

    //download users accessed learning content
    public function download_users_accessed_learning_content_report(){
        export::header('xls', "User accessed learning content");
        $data = $this->load_user_learning_material(false);
        $columns = array(
            "subtopic name",
            "number of user",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        foreach($data as $row){
        echo '<tr>';
        echo " <td>".$row->fullname . "</td>";
        echo " <td>".$row->numberusers . "</td>";
        echo '</tr>';
        }
        echo '</table>';
        export::close();
    }

    //download users completed test
    public function download_users_completed_test_report(){
        export::header('xls', "Users completed test");
        $data = $this->report_number_user_completion(false);
        $columns = array(
            "subtopic name",
            "number of user",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        if(isset($data) || count($data)>0){
            foreach($data as $row){
            echo '<tr>';
            echo " <td>".$row['subtopicname'] . "</td>";
            echo " <td>".$row['numberusers'] . "</td>";
            echo '</tr>';
            }
        }
        echo '</table>';
        export::close();
    }

    //download topic accessed
    public function download_topic_accessed_report(){
        export::header('xls', "Topic accessed");
        $data = $this->report_number_user_access_topic(false);
        $columns = array(
            "topic name",
            "number of user",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        foreach($data as $row){
        echo '<tr>';
        echo " <td>".$row->name . "</td>";
        echo " <td>".$row->nousers . "</td>";
        echo '</tr>';
        }
        echo '</table>';
        export::close();
    }

    //download subtopic accessed
    public function download_subtopic_accessed_report(){
        export::header('xls', "Subtopic accessed");
        $data = $this->report_number_user_access_subtopic(false);
        $columns = array(
            "subtopic name",
            "number of user",
        );
        echo '<table>';
        //print table columns
        echo '<tr>';
        foreach($columns as $column){
            echo "<th>$column</th>";
        }
        echo '</tr>';
        foreach($data as $row){
        echo '<tr>';
        echo " <td>".$row->fullname . "</td>";
        echo " <td>".$row->nousers . "</td>";
        echo '</tr>';
        }
        echo '</table>';
        export::close();
    }


    public function load_teachers($isjson = true){
        global $DB;
        $teachers = $DB->get_records('b13_students_teachers');
        $i = 1;
        foreach($teachers as &$teaccher){
            $teacher->index = $i++;
        }
        if($isjson){
            json::encode($teachers);
        }else
        {
            return $teachers;
        }
    }
    

    public function users_signed_up_dashboard(){
        global $CFG;
        $loadUsers = reports::load_users(false);
       
        ?>
        <style>
            .b13_dashboard_body{
                position:relative !important;
            }
        </style>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Users Signed-up</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Users Signed-up</h3>
            <div class="element-box">
                    <div class="report-box loadUsersBox">
                            <h3 style="font-size:20px;padding-right:10px;">Total Users  <small class="count teacherItem"></small></h3>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar loadUsers">
                                <table class="table table-bordered table-striped mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col" style="text-align:center;">No</th>
                                            <th scope="col">User Name</th>
                                            <th scope="col">First Name</th>
                                            <th scope="col">Last Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">School</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
		                    <div id="pagination"></div>
                            <a href="?classname=reports&method=download_user_signed_up_report" class="btn btn-primary downloadTeacherBtn ">Download</a>
                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

    public function users_from_each_school_dashboard(){
        global $CFG;
        $loadUsers = reports::load_users(false);
        ?>
         <style>
            .b13_dashboard_body{
                position:relative !important;
            }
        </style>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>User from each school</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Users from each school</h3>
            <div class="element-box">
                    <div class="report-box loadUserFromEachSchoolBox">
                            <h3 style="font-size:20px;padding-right:10px;">Total Users  <small class="count schoolItem"></small></h3>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar loadUsers">
                                <table class="table table-bordered table-striped mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col" style="text-align:center;">No</th>
                                            <th scope="col">School</th>
                                            <th scope="col">Number of user</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div id="pagination"></div>
                            <a href="?classname=reports&method=download_user_from_each_school_report" class="btn btn-primary downloadTeacherBtn ">Download</a>
                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

    //get user visited
    public function user_visited_dashboard(){
        global $CFG;
        // $report_number_user_access_day = reports::report_number_user_access_day(false);
        // $report_number_user_access_from_to = reports::report_number_user_access_from_to(false);
        ?>
         <style>
            .b13_dashboard_body{
                position:relative !important;
            }
        </style>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Users revisited</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Users revisited</h3>
            <div class="element-box" style="height:400px;">
                    <div class="report-box">
                    <div class="interval-user-revisit">
                            <div class="tab-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="filter-report">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div id="reportrange">  
                                                                     <input type="text" class="startArangeFromTo form-control" autocomplete="off" style="width: 325px !important;float:left;">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#" id="dwnUserVisit" class="btn btn-primary downloadTeacherBtn ">Download</a>

                                            <div id="tabs" style="display:none;">
                                                <div class="tab-pane active" id="weekly-report">
                                                    <canvas id="fromToReport" width="800" height="450"></canvas>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

    //get user accessed learning content 

    public function users_accessed_learning_content_dashboard(){
        global $CFG;
        $load_user_learning_material = reports::load_user_learning_material(false);
        ?>
         <style>
            .b13_dashboard_body{
                position:relative !important;
            }
        </style>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Users accessed learning content</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Users accessed learning content</h3>
            <div class="element-box">
                    <div class="report-box userAccessLearningContent">
                        <h3 style="font-size:20px;padding-right:10px;">Total Users  <small class="count userAccessContentItem"></small></h3>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar loadUsers">
                                <table class="table table-bordered table-striped mb-0">
                                         <thead>
                                                <tr>
                                                    <th style="text-align:center;width:50px;">No</th>
                                                    <th scope="col">Subtopic name</th>
                                                    <th scope="col">Number of user</th>
                                                </tr>
                                        </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div id="pagination"></div>
                            <a href="?classname=reports&method=download_users_accessed_learning_content_report" class="btn btn-primary downloadTeacherBtn ">Download</a>
                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

    //get user completed Test

    public function users_completed_test_dashboard(){
        global $CFG;
        $load_user_exam_completion = reports::load_user_exam_completion(false);
        ?>
         <style>
            .b13_dashboard_body{
                position:relative !important;
            }
        </style>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Users completed test</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Users completed test</h3>
            <div class="element-box">
                    <div class="report-box userCompletedTest">
                            <h3>Total Users <small class="count userCompletedTestTotal"></small></h3>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar loadUsers">
                                <table class="table table-bordered table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:50px;">No</th>
                                                <th scope="col">Subtopic name</th>
                                                <th scope="col">Number of user</th>
                                            </tr>
                                        </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div id="pagination"></div>
                            <a href="?classname=reports&method=download_users_completed_test_report" class="btn btn-primary downloadTeacherBtn ">Download</a>
                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

    //get user completed Test

    public function topic_accessed_dashboard(){
        global $CFG;
        $report_number_user_access_topic = reports::report_number_user_access_topic(false);
        ?>
         <style>
            .b13_dashboard_body{
                position:relative !important;
            }
        </style>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Topic accessed</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Topic accessed</h3>
            <div class="element-box">
                    <div class="report-box topicAccessed">
                        <h3 style="font-size:20px;padding-right:10px;">Total Users  <small class="count topicAccessedTotal"></small></h3>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar loadUsers">
                                <table class="table table-bordered table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:50px;">No</th>
                                                <th scope="col">Topic name</th>
                                                <th scope="col">Number of user</th>
                                            </tr>
                                        </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div id="pagination"></div>
                            <a href="?classname=reports&method=download_topic_accessed_report" class="btn btn-primary downloadTeacherBtn ">Download</a>

                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

      //get user completed Test

      public function subtopic_accessed_dashboard(){
        global $CFG;
        $report_number_user_access_subtopic = reports::report_number_user_access_subtopic(false);
        ?>
         <style>
            .b13_dashboard_body{
                position:relative !important;
            }
        </style>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Subtopic accessed</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Subtopic accessed</h3>
            <div class="element-box">
                    <div class="report-box subtopicAccessed">
                        <h3 style="font-size:20px;padding-right:10px;">Total Users  <small class="count subtopicAccessedTotal"></small></h3>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar loadUsers">
                                <table class="table table-bordered table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:50px;">No</th>
                                                <th scope="col">Subtopic name</th>
                                                <th scope="col">Number of time</th>
                                            </tr>
                                        </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div id="pagination"></div>
                            <a href="?classname=reports&method=download_subtopic_accessed_report" class="btn btn-primary downloadTeacherBtn ">Download</a>

                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }

    public function getBookReport(){
        global $CFG;
        $issueReport = b13students::get_content_corrections(false);
        ?>
        <ul class="breadcrumb custombreadcrumb">
            <li>
                <a href="javascript:void(0)" onClick="getanalytics()">Analytics</a>
            </li>
            <li>
                <span>Issues from student</span>
            </li>
        </ul>
        <ul class="profile-user">
            <li>
                <a href="javascript:void()" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </li>
        </ul>
        <!-- Modal -->
        <div class="modal fade" id="logoutModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                    <p>Do you really want to log out? </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="<?php echo $CFG->wwwroot?>/login/logout.php?sesskey=<?php echo sesskey()?>" class="btn btn-success">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        dashboard_util::add_breadcrumb("Teacher report");
        dashboard_util::start_page();
        ?>
        <h3 class="element-header">Issues Report</h3></h3>
            <div class="element-box">
                    <div id="issueReport" class="report-box issueReport">
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-bordered table-striped mb-0">
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width:40px;text-align:center">No</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Content</th>
                                            <th scope="col">Url</th>
                                            <th scope="col" style="width:100px;">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
		                    <div class="pagination"></div>
                    </div>
            </div>

        <?php
        //button::add("Download", '?classname=reports&method=download_teacher_report');
        dashboard_util::end_page();
    }


    //report 1-2 
    public function load_users($isjson = true){
        global $DB;
        $users = array_values($DB->get_records('user', null,null, '	username,firstname,	lastname,email,institution'));
        $i = 1;
        foreach($users as &$user){
            $user->index = $i++;
        }
        if($isjson){
            json::encode($users);
        }else
        {
            return $users;
        }
    }

    public function report_total_users_from_school($isjson = true){
        global $DB;
        
        $users = array_values($DB->get_records_sql("SELECT institution, COUNT(id) as totalstudents FROM {user} GROUP BY institution" ) );
        $i = 1;
        foreach($users as &$user){
            $user->index = $i++;
        }
        if($isjson){
            json::encode($users);
        }else
        {
            return $users;
        }
    }


    //report 3
    public function load_user_learning_material($isjson = true){
        global $DB;
        $user_learning = array_values($DB->get_records_sql('SELECT  c.id, c.fullname, COUNT(u.username) as numberusers
                                        FROM {b13_status_student_subtopic} ststsu
                                        JOIN {user} u
                                        ON u.id = ststsu.userid 
                                        JOIN {course} c
                                        ON c.id = ststsu.courseid
                                        GROUP BY c.id, c.fullname'
                                        ) );
        $i = 1;
        foreach($user_learning as &$ul){
            $ul->index = $i++;
        }
        if($isjson){
            json::encode($user_learning);
        }else
        {
            return $user_learning;
        }
    }
    //report 4 - 6
    private  function get_subtopic_quiz_progression($subtopicid , $userid)
    {
        global $DB;
        $selsubtopic = $DB->get_record('course', array('id'=>$subtopicid));
        if($selsubtopic == null)
            return 0;
        $seltopicsubject = $DB->get_record_sql("SELECT coca.* FROM {course} c JOIN {course_categories} coca ON c.category = coca.id WHERE c.id = $subtopicid");
        if($seltopicsubject == null)
            return 0;
        $selexamboard = $DB->get_records('b13_student_selection', array('userid'=>$userid, 'subjectid'=>$seltopicsubject->parent ));
        $selexamboard = current($selexamboard);
        if($selexamboard == null)
            return 0;

        $data = $DB->get_records("b13_student_qgroup_box", array('userid' => $userid, 'subtopicid' => $subtopicid, 'examboardid'=>$selexamboard->examboardid));

        $dataprogression = $DB->get_record("b13_student_progression_quiz", array('userid' => $userid, 'subtopicid' => $subtopicid, 'examboardid'=>$selexamboard->examboardid));
        if($dataprogression == null)
            return 0;
        $quizlist = json_decode($dataprogression->quizlist, true);
        if ($data) {
            $progress = 0;
            foreach ($data as $quiz) {
                $progress += $quiz->boxnumber;
            }
            return $progress / (count($quizlist) * 5);
        } else {
            return 0;
        }
    }

    public function load_user_exam_completion($isjson = true){
        global $DB;
        $users = $DB->get_records('user');
        $subtopics = $DB->get_records('course');
        $data = array();
        $i = 1;
        foreach($users as $user){
            foreach($subtopics as $subtopic){
                $completion = $this->get_subtopic_quiz_progression($subtopic->id,$user->id);
                if($completion > 0){
                    $tmp_array = array();
                    $tmp_array['userid'] = $user->id;
                    $tmp_array['username'] = $user->username;
                    $tmp_array['subtopicid'] = $subtopic->id;
                    $tmp_array['subtopicname'] = $subtopic->fullname;
                    $tmp_array['examcompletion'] = $this->get_subtopic_quiz_progression($subtopic->id,$user->id);
                    $tmp_array['index'] = $i ;
                    array_push($data, $tmp_array);
                }
            }
        }
        if($isjson){
            json::encode($data);
        }else
        {
            return $data;
        }
    }

    public function report_number_user_completion($isjson = true){
        global $DB;
        $users = $DB->get_records('user');
        $subtopics = $DB->get_records('course');
        $data = array();
        foreach($subtopics as $subtopic){
            $count = 0;
            foreach($users as $user){
                $completion = $this->get_subtopic_quiz_progression($subtopic->id,$user->id);
                if($completion == 1){
                    $count++;
                   
                   
                }
            }
            if($count > 0){
                $tmp_array = array();
                $tmp_array['subtopicid'] = $subtopic->id;
                $tmp_array['subtopicname'] = $subtopic->fullname;
                $tmp_array['numberusers'] = $count;
                array_push($data, $tmp_array);
            }
        }
        if($isjson){
            json::encode($data);
        }else
        {
            return $data;
        }
    }

    //interval of user revisits
    public  function report_number_user_access_day($time = null, $isjson = true){
        global $DB;
        if($time == null){
            $time = required_param('time', PARAM_INT);
            $time = $time /1000;
        }

        $data = $DB->count_records_sql("SELECT  DISTINCT COUNT(userid)  
                                        FROM {b13_report_user_visit} ruv
                                        WHERE  DAY(FROM_UNIXTIME(ruv.time)) = DAY(FROM_UNIXTIME($time)) AND  MONTH(FROM_UNIXTIME(ruv.time)) = MONTH(FROM_UNIXTIME($time)) AND YEAR(FROM_UNIXTIME(ruv.time)) = YEAR(FROM_UNIXTIME($time))");

        if($isjson){
            json::encode($data);
        }else{
            return $data;
        }
    }

    public function report_number_user_access_from_to($startdate = null, $enddate = null, $isjson = true){
        global $DB;

        if($startdate == null){
            $startdate = required_param('startdate', PARAM_INT);
            $startdate = $startdate /1000;
        }
        if($enddate == null){
            $enddate = required_param('enddate', PARAM_INT);
            $enddate = $enddate / 1000;
        }

        $data = $DB->count_records_sql("SELECT DISTINCT COUNT(userid) 
                                        FROM {b13_report_user_visit} ruv
                                        WHERE FROM_UNIXTIME(ruv.time) >= FROM_UNIXTIME($startdate) AND FROM_UNIXTIME(ruv.time) < FROM_UNIXTIME($enddate)");

        if($isjson){
            json::encode($data);
        }else{
            return $data;
        }
    }
    public function report_number_user_revisit($startdate = null, $numberofday = null, $isjson = true){
        global $DB;

        if($startdate == null){
            $startdate = required_param('startdate', PARAM_INT);
            $startdate = $startdate /1000;
        }
        if($numberofday == null){
            $numberofday = required_param('numberofday', PARAM_INT);
        }
        $data = array();
        $time = $startdate;
        for($i = 0 ; $i < $numberofday; $i++){
            $tmp = array();
            $time = $startdate + $i * 24*60*60; 
           $numberusers = $DB->count_records_sql("SELECT DISTINCT COUNT(userid) 
            FROM {b13_report_user_visit} ruv
            WHERE  DAY(FROM_UNIXTIME(ruv.time)) = DAY(FROM_UNIXTIME($time)) AND  MONTH(FROM_UNIXTIME(ruv.time)) = MONTH(FROM_UNIXTIME($time)) AND YEAR(FROM_UNIXTIME(ruv.time)) = YEAR(FROM_UNIXTIME($time))");
            array_push($data, array('time'=>$time, 'nousers'=>$numberusers));

        }

        if($isjson){
            json::encode($data);
        }else
        {
            return $data;
        }
    }

    //Number of times each Topic and Subtopic is accessed.
    public function report_number_user_access_topic($isjson = true){
        global $DB;

        $topics = $DB->get_records("course_categories", null,null, 'id,name');
        $i = 1;
        foreach($topics as &$topic){
            $topic->nousers = $DB->count_records_sql("SELECT COUNT(id) FROM {b13_report_user_ativity} WHERE  topicid = $topic->id");
            $topic->index = $i++;
        }

        if($isjson){
            json::encode($topics);
        }else
        {
            return $topics;
        }
    }

    public function report_number_user_access_subtopic($isjson = true){
        global $DB;

        $subtopics = $DB->get_records("course", null,null, 'id,fullname');
        foreach($subtopics as &$subtopic){
            $subtopic->nousers = $DB->count_records_sql("SELECT COUNT(id) FROM {b13_report_user_ativity} WHERE  subtopicid = $subtopic->id");
        }

        if($isjson){
            json::encode($subtopics);
        }else
        {
            return $subtopics;
        }
    }

}

?>

