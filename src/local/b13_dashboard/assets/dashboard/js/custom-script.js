// window loading
$(window).on('load', function(){
	$(".se-pre-con").fadeOut();
});
window.onload=new function() { 
    $(".se-pre-con").fadeOut();
 };
 (function($) {
	$.fn.inputFilter = function(inputFilter) {
	  return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		if (inputFilter(this.value)) {
		  this.oldValue = this.value;
		  this.oldSelectionStart = this.selectionStart;
		  this.oldSelectionEnd = this.selectionEnd;
		} else if (this.hasOwnProperty("oldValue")) {
		  this.value = this.oldValue;
		  this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		}
	  });
	};
  }(jQuery));
  function uploadIconFile(){
	$(".createSubjecIcon").on('submit',(function(e) {
		e.preventDefault();
		$(".inputUrlIcon").empty();
		$.ajax({
		url: "./classes/fileUpload.php", // Url to which the request is send
		type: "POST",             // Type of request to be send, called as method
		data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data)   // A function to be called if request succeeds
		{
			console.log(data);

		$(".inputUrlIcon").attr('value',data);
		}
		});
	}));
	// Function to preview image after validation
	$(".iconFile").change(function() {
		$(".inputUrlIcon").empty(); // To remove the previous error message
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
		$('.previewing').attr('src','noimage.png');
		$(".inputUrlIcon").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
		return false;
		}
		else
		{
		var reader = new FileReader();
		reader.onload = imageIsLoaded;
		reader.readAsDataURL(this.files[0]);
		}
		$('.submitSubjectIconBtn').trigger('click');
	});
	function imageIsLoaded(e) {
		$('.previewing').attr('src', e.target.result);
	};
}
  function updateUploadIconFile(){
	$("#updateSubjectForm").on('submit',(function(e) {
		e.preventDefault();
		$(".updateInputUrlIcon").empty();
		$.ajax({
		url: "./classes/fileUpload.php", // Url to which the request is send
		type: "POST",             // Type of request to be send, called as method
		data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data)   // A function to be called if request succeeds
		{
			console.log(data);
			$(".updateInputUrlIcon").attr('value',data);
		}
		});
	}));
	// Function to preview image after validation
	$(".updateIconFile").change(function() {
		$(".updateInputUrlIcon").empty(); // To remove the previous error message
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
		$('.updatePreviewing').attr('src','noimage.png');
		$(".updateInputUrlIcon").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
		return false;
		}
		else
		{
		var reader = new FileReader();
		reader.onload = imageIsLoaded;
		reader.readAsDataURL(this.files[0]);
		}
		$('.updateSubmitSubjectIconBtn').trigger('click');
	});
	function imageIsLoaded(e) {
		$('.updatePreviewing').attr('src', e.target.result);
	};
}

$(document).ready(function (e) {
	uploadIconFile();
});
var textSpecial = "Field must be Alphabetical or Numerical characters only";
//enter to close
$(document).bind('keypress', function(e) {
	var keycode = (event.keyCode ? event.keyCode : event.which);
	  if(keycode == '13'){
		  $(".swal-modal .swal-button").click();
	  }
});



//not allowing special characters in textbox
function preventSpencialChars($text){
	  var regex = new RegExp("^[a-zA-Z0-9- ]+$");
	  var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	  var keycode = (event.keyCode ? event.keyCode : event.which);
	  if(keycode == '13'){
			  $('.enterPress .submitButton').click();
	  }
	  else if (!regex.test(key)) {
		  event.preventDefault();
		  // return false;
		  swal({
			  icon: 'error',
			  showCloseButton: true,
			  closeModal:true,
			  text: $text,
			  button:'ok'
			  });
	  } 
}
function preventSpencialCharsInSubjectOption($text){
  var regex = new RegExp("^[a-zA-Z0-9- ]+$");
	  var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	  var keycode = (event.keyCode ? event.keyCode : event.which);
	  if(keycode == '13'){
			  $('.enterOptionPress .submitButton').click();
	  }
	  else if (!regex.test(key)) {
		  event.preventDefault();
		  // return false;
		  swal({
			  icon: 'error',
			  showCloseButton: true,
			  closeModal:true,
			  text: $text,
			  button:'ok'
			  });
	  } 
}

function preventSpencialCharsInEditSubjectOption($text){
  var regex = new RegExp("^[a-zA-Z0-9- ]+$");
	  var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	  var keycode = (event.keyCode ? event.keyCode : event.which);
	  if(keycode == '13'){
			  $('.enterEditOptionPress .submitButton').click();
	  }
	  else if (!regex.test(key)) {
		  event.preventDefault();
		  // return false;
		  swal({
			  icon: 'error',
			  showCloseButton: true,
			  closeModal:true,
			  text: $text,
			  button:'ok'
			  });
	  } 
}

function doSuccessfully($text){
  swal({
	  icon: 'success',
	  showCloseButton: true,
	  closeModal:true,
	  text: $text,
	  button:'ok'
	  }).then(function(){
		  location.reload();
	  });
}
function doError($text){
  swal({
	  icon: 'error',
	  showCloseButton: true,
	  closeModal:true,
	  text: $text,
	  button:'ok'
	  }).then(function(){
		  location.reload();
	  });
}
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
	  return false;
  return true;
}

$('input[name="number"]').keyup(function(e)
							  {
if (/\D/g.test(this.value))
{
  // Filter non-digits from input value.
  this.value = this.value.replace(/\D/g, '');
}
});

function enterPressFunction(element,childElement){
  $(element + " input:not('.checkMaxLenth')").bind("keypress",function(){
	  var keycode = (event.keyCode ? event.keyCode : event.which);
	  if(keycode == '13'){
			  $(childElement + ' .submitButton').click();
	  }
  });
}
$(document).ready(function(){
	window.onload=new function() { 
		$(".se-pre-con").fadeOut();
	 };
	//add student menu
	//var $targetStudentLink = $('.main-menu li:first-child');
	//$('<li><a href="/student/#/student-view"><img src="/local/b13_dashboard/assets/dashboard/img/icon/student.svg" class="menu-icon" alt="Icon"><span>Student View</span></a></li>').insertAfter($targetStudentLink);
	// add site administration page
	$('#loadSiteAdministration #frame').css('height',$(window).height() - 60);
	$('#loadSiteAdministration #frame').attr("src",'/admin/search.php');
	$('#update_learning_material_page').css('height',$(window).height() - 145);

	// hackishly force iframe to reload
	var questionIframe = $('.studentQuizIframe');
	questionIframe.attr( 'src', function ( i, val ) { return val; });

 	if($('.b13-list-page').hasClass('b13-list-page')){
		$('.b13-list-page').css('height',$(window).height() - 190);
		new PerfectScrollbar('.b13-list-page');
	}
	$("#success-alert").hide();
	$('.examboard-page .examboard-box .container-examboard-box').css('height',$(window).height() - 250);
	$('.loading-subtopic').css('height',$(window).height() - 270);
	$('.switch-subtopic').css('height',$(window).height() - 170);
	var loading = $(".loadingAjax");
        $(document).ajaxStart(function () {
            loading.show();
        });

        $(document).ajaxStop(function () {
            loading.hide();
    });
	//Create new subject
	$('.createNewSubject button.closeModal').click(function(){
		location.reload();
	});
	$('.createNewSubject .createNewSubjectBtn').click(function(event) {
		if ($.trim($('.createNewSubject input[name=name]').val()) === "") {
			swal({
				icon: 'error',
				showCloseButton: true,
				closeModal:true,
				text: 'The Name is required!',
				button:'ok'
			  });
			return false;
		}else{
			var formData = {
				'name'              : $('.createNewSubject input[name=name]').val(),
				'description'       : $('.createNewSubject input[name=description]').val(),
				'icon'       : $('.createNewSubject input[name=icon]').val(),
			};
			$.ajax({
				type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
				url         : '/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=create_new_subject', // the url where we want to POST
				data        : formData, // our data object
				dataType    : 'json', // what type of data do we expect back from the server
				encode          : true
			})
				.done(function(data) {
					doSuccessfully($text="Created Successfully!");
					//console.log(data); 
				});
			event.preventDefault();
		}
	});
		
	//Create new topic
	$('.createNewTopic button.closeModal').click(function(){
		location.reload();
	});
	$('.createNewTopicBtn').click(function(event) {
		if ($.trim($('.createNewTopic input[name=name]').val()) === "") {
			  swal({
					icon: 'error',
					showCloseButton: true,
					closeModal:true,
					text: 'The Name is required!',
					button:'ok'
			  });
			return false;
		}else{
		var formData = {
			'name'              : $('.createNewTopic input[name=name]').val(),
			'description'       : $('.createNewTopic input[name=description]').val(),
			'parent'						: $('.createNewTopic input[name=subjectList]').val()
		};
		// process the form
		$.ajax({
			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url         : '/local/b13_dashboard/load-ajax.php?classname=b13topics&method=create_topic', // the url where we want to POST
			data        : formData, // our data object
			dataType    : 'json', // what type of data do we expect back from the server
			encode          : true
		})
			.done(function(data) {
				doSuccessfully($text="Created Successfully!");  
			//console.log(data);
			});
		event.preventDefault();
	}
		});

		//Create subject option
		$('.createNewSubjectOption button.closeModal').click(function(){
			location.reload();
		});
	$('.createNewSubjectOptionBtn').click(function(event) {
		if ($.trim($('.createNewSubjectOption input.subjectoptiontitle').val()) === "") {
			swal({
				icon: 'error',
				showCloseButton: true,
				closeModal:true,
				text: 'The title is required!',
				button:'ok'
			  });
			return false;
		}else{
		var formDataSubjectOptions = {
			'subjectid'       : $('.createNewSubjectOption input[name=subjecid]').val(),
			'title'              : $('.createNewSubjectOption input[name=title]').val(),
			'description'						: $('.createNewSubjectOption input[name=descriptionOption]').val()

		};
		// show all subject option
		$.ajax({
			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url         : '/local/b13_dashboard/load-ajax.php?classname=b13subjectoptions&method=create_option', // the url where we want to POST
			data        : formDataSubjectOptions, // our data object
			dataType    : 'json', // what type of data do we expect back from the server
			encode          : true
		})
		.done(function(data) {
			doSuccessfully($text="Created Successfully!");  
			//console.log(data.data);
			});
			event.preventDefault();
		}
		});

		//unselect radio Subject Optino 
		$('#addTopicToOption fieldset label').each(function(){
			if($(this).find('input').is(':checked')) { 
				$(this).click(function() {
						$(this).siblings('.defaultRadio').find('input').trigger('change').trigger('click');
						$(this).find('input').prop('checked', false);
				});
			}
		});
		

		//switch select options
		$("ul.selectOptionList").on("click", ".init", function() {
			$(this).closest("ul.selectOptionList").children('li:not(.init)').toggle();
		});

		var allOptions = $("ul.selectOptionList").children('li:not(.init)');
		$("ul.selectOptionList").on("click", "li:not(.init)", function() {
				allOptions.removeClass('selected');
				$(this).addClass('selected');
				$("ul.selectOptionList").children('.init').html($(this).html());
				allOptions.toggle();
		});
		//
		$("ul.optionList").on("click", ".init", function() {
			$(this).closest("ul.optionList").children('li:not(.init)').toggle();
		});

		var allSubjectOptions = $("ul.optionList").children('li:not(.init)');
		$("ul.optionList").on("click", "li:not(.init)", function() {
				allSubjectOptions.removeClass('selected');
				$(this).addClass('selected');
				$("ul.optionList").children('.init').html($(this).html());
				allSubjectOptions.toggle();
		});
		
		//Create new course
		$('.createNewSubtopic button.closeModal').click(function(){
			location.reload();
		});
	$('.createNewSubtopicBtn').click(function(event) {
		if ($.trim($('.createNewSubtopic input[name=fullname]').val()) === "") {
			swal({
				icon: 'error',
				showCloseButton: true,
				closeModal:true,
				text: 'The Full name is required!',
				button:'ok'
			  })
			return false;
		}else{
		event.preventDefault();
		var formTopicData = {
			'topicid'					: $('.createNewSubtopic input[name=topicid]').val(),
			'fullname'        : $('.createNewSubtopic input[name=fullname]').val(),
			'shortname'       : $('.createNewSubtopic input[name=shortname]').val(),
			'idnumber'				: $('.createNewSubtopic input[name=idnumber]').val(),
			'summary'					: $('.createNewSubtopic textarea[name=summary]').val(),
		};
		// process the form
		$.ajax({
			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url         : '/local/b13_dashboard/load-ajax.php?classname=b13subtopics&method=create_subtopic', // the url where we want to POST
			data        : formTopicData, // our data object
			dataType    : 'json', // what type of data do we expect back from the server
			encode          : true
		})
			.done(function(data) {
				if(data.error){
					swal({
						icon: 'error',
						showCloseButton: true,
						closeModal:true,
						text: data.error,
						button:'ok'
					})
				}else{
					doSuccessfully($text="Created Successfully!");  
				}
				//console.log(data);
			});
		}
		});

	//Create new leaning material
	$('#createNewLearningMaterial').click(function(event) {
		event.preventDefault();
		var formTopicData = {
			'subtopic'						: $('.createNewLearningMaterial input[name=subtopic]').val(),
		};
		$.ajax({
			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url         : '/local/b13_dashboard/load-ajax.php?classname=b13learningmodules&method=create_learningmodules', // the url where we want to POST
			data        : formTopicData, // our data object
			dataType    : 'json', // what type of data do we expect back from the server
			encode          : true
		}).done(function(data) {
			location.reload();  
		});
	});

	//Create new quiz 
	$('#createNewQuiz').click(function(event) {
		event.preventDefault();
		var formTopicData = {
			'subtopic'						: $('.createNewQuiz input[name=subtopic]').val(),
		};
		$.ajax({
			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url         : '/local/b13_dashboard/load-ajax.php?classname=b13quizmodule&method=create_quizmodules', // the url where we want to POST
			data        : formTopicData, // our data object
			dataType    : 'json', // what type of data do we expect back from the server
			encode          : true
		}).done(function(data) {
			location.reload();  
		});
	});

	//Create new examboard
	$('.createNewExamboard button.closeModal').click(function(){
		location.reload();
	});
	$('.createNewExamboardBtn').click(function(event) {
		if ($.trim($('.createNewExamboard input[name=title]').val()) === "") {
			swal({
				icon: 'error',
				showCloseButton: true,
				closeModal:true,
				text: 'Examboard Name is required!',
				button:'ok'
			  })
			return false;
		}else{
		var formExamBoardData = {
			'title'						: $('.createNewExamboard input[name=title]').val(),
			'idnumber'						: $('.createNewExamboard input[name=idnumber]').val(),
			'description'						: $('.createNewExamboard input[name=description]').val(),
		};
		$.ajax({
			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url         : '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=create_examboard', // the url where we want to POST
			data        : formExamBoardData, // our data object
			dataType    : 'json', // what type of data do we expect back from the server
			encode          : true
		})
			.done(function(data) {
				console.log(data);
				swal({
					icon: 'success',
					showCloseButton: true,
					closeModal:true,
					text: 'Created Successfully!',
					button:'ok'
				  }).then(function(){
					location.reload();
				  });
			});
		}
		});

	var getExamboardId = $('input[name="examboardid"]').val();
	selected_subtopics_by_subject(getExamboardId);				
	//get all subjects
	load_all_subjectsInExamboard();
	});

	//delete examboard
	function deleteExamboard(examboardid){
		swal({
			title: "Are you sure?",
			text: "The Examboard will be deleted!",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then(function(willDelete){
			if (willDelete){
				$.ajax({
							type: 'DELETE',
							url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=delete_examboard&examboardid='+examboardid,
							data:{examboard:examboardid}
					}).then(function successCallback(response) {
							swal("The Examboard has been deleted!", {
									icon: "success",
							}).then(function(){
								location.reload();
							});
					}, function errorCallback(response) {
					});
			} else {
				swal("Your Examboard is safe!");
			}
		});
	}	

	//get Examboard detail 
function detailExamboard(examboardid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=detail_examboard&examboardid='+ examboardid,
		type: 'GET',
		success: function(data) {
			console.log(data);
			var detailExamboard = data.data[0];
			console.log(detailExamboard);
			$('#updateExamboardForm .modal-body').html(
				'<input type="hidden" value="'+detailExamboard.id+'" name="subjectid"/>'+
				'<div id="name-group" class="form-group">' +
                '<label for="title">Examboard Name</label>' +
								'<input type="text" class="form-control checkMaxLenth examboardtitle" maxlength="50" name="title" value="'+detailExamboard.title+'">'+
								'<i class="label label-danger">Field must be Alphabetical or Numerical characters only</i>'+
								'</div>'+
				'<div id="name-group" class="form-group hide">'+
								'<label for="idnumber">Id Number</label>'+
								'<input type="number" class="form-control idnumberexamboard" name="idnumber" value="'+detailExamboard.idnumber+'">'+
								'<i class="label label-danger">Id Number must unique.</i>'+
			  '</div>'+
				'<div id="description-group" class="form-group">'+
                '<label for="description">description</label>'+
                '<input class="form-control" name="description" value="'+detailExamboard.description+'">'+
								'</div>'+
								'<div class="modal-footer">'+
                      '<button type="button" class="btn btn-default closeCurrentModel">Close</button>'+
                      '<button type="button" class="btn btn-success submitButton" onclick="updateExamboard('+examboardid+')">Update </button>'+
                    '</div>'
					);
					$('.closeCurrentModel').click(function(){
						$('#updateExamboard').modal('hide');
					});

					//not allowing special charecters in update textbox subject
					$(".updateExamboardForm input.examboardtitle").bind('keypress', function (event) {
						preventSpencialChars($text=textSpecial);
					});
					enterPressFunction(".updateExamboardForm",".enterPress");
		}
	});
}

//update subject
function updateExamboard(examboardid){
	if ($.trim($('#updateExamboardForm input[name=title]').val()) === "") {
		swal({
			icon: 'error',
			showCloseButton: true,
			closeModal:true,
			text: 'The Full Name is required!',
			button:'ok'
			})
		return false;
	}else{
		var updateExamboardData = {
			'examboardid'						: examboardid,
			'title'						: $('#updateExamboardForm input[name=title]').val(),
			'idnumber'						: $('.updateExamboardForm input[name=idnumber]').val(),
			'description'						: $('.updateExamboardForm input[name=description]').val(),
		};
		$.ajax({
			url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=update_examboard',
			data: updateExamboardData,
			type: 'POST',
			success: function(data) {
				console.log(data);
				if(data.data === 'updated successfully'){
					doSuccessfully($text=data.data);
				}else{
					doError($text=data.data);
				}
				//console.log(data);
			}
		});
	}
}

//get all topic
  function getTopic($id){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13topics&method=load_all_topics&subjectid=' + $id,
		data: {subjectid: $id},
		type: 'POST',
		success: function(data) {
			window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13topics&method=dashboard&subjectid=' + $id;
			// console.log(data);
		}
	});
}

//get topic detail by topic 
function detailTopic($topicid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13topics&method=detail_topic&topicid='+ $topicid,
		type: 'GET',
		success: function(data) {
			console.log(data);
			//window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13examboards&method=view_detail_examboards&examboardid='+ $examboardid;
			 var detailTopic = data.data[0];
			 console.log(detailTopic);
			$('#updateTopicForm .modal-body').html(
				'<input type="hidden" value="'+detailTopic.id+'" name="topicid"/>'+
				'<div id="name-group" class="form-group">' +
                '<label for="name">Name</label>' +
                '<input type="text" class="form-control checkMaxLenth topicname" name="name" value="'+detailTopic.name+'"'+
                '</div>'+
								'<div id="description-group" class="form-group">'+
                '<label for="description">description</label>'+
                '<input class="form-control" name="description" value="'+detailTopic.description+'">'+
								'</div>'+
								'<div class="modal-footer">'+
                      '<button type="button" class="btn btn-default closeCurrentModel">Close</button>'+
                      '<button type="button" class="btn btn-success submitButton" onclick="updateTopic('+$topicid+')">Update </button>'+
                    '</div>'
					);
					$('.closeCurrentModel').click(function(){
						$('#updateTopic').modal('hide');
					});
					//not allowing special charecters in update textbox subject
					$(".updateTopicForm input.topicname").bind('keypress', function (event) {
						preventSpencialChars($text=textSpecial);
					});
					enterPressFunction(".updateTopicForm",".enterPress");

		}
	});
}

//update subtopic
function updateTopic($topicid){
	if ($.trim($('.updateTopicForm input[name=name]').val()) === "") {
		swal({
			icon: 'error',
			showCloseButton: true,
			closeModal:true,
			text: 'The Name is required!',
			button:'ok'
			})
		return false;
	}else{
		var updateTopicData = {
			'topicid'						: $topicid,
			'name'						: $('.updateTopicForm input[name=name]').val(),
			'description'						: $('.updateTopicForm input[name=description]').val(),
		};
		$.ajax({
			url: '/local/b13_dashboard/load-ajax.php?classname=b13topics&method=update_topic',
			data: updateTopicData,
			type: 'POST',
			success: function(data) {
				doSuccessfully($text="Updated Successfully!");
			}
		});
	}
}

//disable topic
function disableTopic($topicid){
	swal({
		title: "Are you sure?",
		text: "The Topic will be disabled!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then(function(willDisable){
		if (willDisable){
			$.ajax({
						type: 'POST',
						url: '/local/b13_dashboard/load-ajax.php?classname=b13topics&method=disable_topic&topicid='+$topicid,
				}).then(function successCallback(response) {
						swal("The Topic has been disabled!", {
								icon: "success",
						}).then(function(){
							location.reload();
						});
				}, function errorCallback(response) {
					swal({
						title: "Can not delete this subtopic",
						text: "This subtopic was added into examboard!",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})
				});
		} else {
			swal("Your Topic is disabled!");
		}
	});
}

//disable topic
function enableTopic($topicid){
	swal({
		title: "Are you sure?",
		text: "The Topic will be enabled!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then(function(willDisable){
		if (willDisable){
			$.ajax({
						type: 'POST',
						url: '/local/b13_dashboard/load-ajax.php?classname=b13topics&method=enable_topic&topicid='+$topicid,
				}).then(function successCallback(response) {
						swal("The Topic has been enabled!", {
								icon: "success",
						}).then(function(){
							location.reload();
						});
				}, function errorCallback(response) {
						//alert(response);
				});
		} else {
			swal("Your Topic is safe!");
		}
	});
}


function getSubTopic($subjectid, $topicid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13subtopics&method=load_all_topics&subjectid='+ $subjectid + '&topicid=' + $topicid,
		data: {subjectid: $subjectid, topicid: $topicid},
		type: 'POST',
		success: function(data) {
			window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13subtopics&method=dashboard&subjectid='+ $subjectid + '&topicid=' + $topicid;
			// console.log(data);
		}
	});
}

//delete subtopic
function deleteSubTopic($subtopicid){
	swal({
		title: "Are you sure?",
		text: "The Subtopic will be deleted!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then(function(willDelete){
		if (willDelete) {
			$.ajax({
				url: '/local/b13_dashboard/load-ajax.php?classname=b13subtopics&method=delete_subtopic&subtopicid='+ $subtopicid,
				data: {subtopicid: $subtopicid},
				type: 'DELETE',
				success: function(data) {
					swal("The Subtopic has been deleted!",{
						icon: "success",
					}).then(function(){
						$('.subtopic_'+$subtopicid).remove();  
					})
				},
				error: function(error){
					console.log(error);
					swal({
						title: "Can not delete this subtopic",
						text: "This subtopic was added into examboard!",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})
				}
			});
		} else {
			swal("Your Subtopic is safe!");
		}
	});
}
//get Subtopic detail by subtopicid 
function detailSubTopic($topicid, $subtopicid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13subtopics&method=detail_subtopic&subtopicid='+ $subtopicid,
		// data: {subtopicid: $subtopicid},
		type: 'GET',
		success: function(data) {
			//window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13examboards&method=view_detail_examboards&examboardid='+ $examboardid;
			 var detailSubtopic = data.data[0];
			 console.log(detailSubtopic);
			$('#updateSubtopicForm .modal-body').html(
				'<input type="hidden" value="'+detailSubtopic.category+'" name="topicid"/>'+
				'<input type="hidden" value="'+detailSubtopic.id+'" name="subtopicid"/>'+
				'<div id="name-group" class="form-group">' +
                '<label for="fullname">Full Name</label>' +
                '<input type="text" class="form-control checkMaxLenth subtopicname" name="fullname" value="'+detailSubtopic.fullname+'"'+
                '</div>'+
				'<div id="description-group" class="form-group">'+
                '<label for="shortname">Shortname</label>'+
                '<input type="text" class="form-control" name="shortname" value="'+detailSubtopic.shortname+'">'+
                '</div>'+
				'<div id="description-group" class="form-group">'+
                '<label for="idnumber">Code</label>'+
                '<input type="text" class="form-control" name="idnumber" value="'+detailSubtopic.idnumber+'">'+
                '</div>'+
				'<div id="description-group" class="form-group">'+
                '<label for="summary">Summary</label>'+
                '<textarea class="form-control" name="summary" value="'+detailSubtopic.summary+'">'+detailSubtopic.summary+'</textarea>'+
								'</div>'+
								'<div class="modal-footer">'+
                      '<button type="button" class="btn btn-default closeCurrentModel">Close</button>'+
                      '<button type="button" class="btn btn-success submitButton" onclick="updateSubTopic('+$topicid+','+$subtopicid+')">Update </button>'+
                    '</div>'
					);
					$('.closeCurrentModel').click(function(){
						$('#updateSubtopic').modal('hide');
					});
					//not allowing special charecters in textbox sub topic
					$(".updateSubtopicForm input.subtopicname").bind('keypress', function (event) {
						preventSpencialChars($text=textSpecial);
					});
					enterPressFunction(".updateSubtopicForm",".enterPress");

		}
	});
}

//update subtopic
function updateSubTopic($topicid, $subtopicid){
	if ($.trim($('.updateSubtopicForm input[name=fullname]').val()) === "") {
		swal({
			icon: 'error',
			showCloseButton: true,
			closeModal:true,
			text: 'The Full Name is required!',
			button:'ok'
			})
		return false;
	}else{
		var updateSubtopicData = {
			'topicid'						: $topicid,
			'subtopicid'					: $subtopicid,
			'fullname'						: $('.updateSubtopicForm input[name=fullname]').val(),
			'shortname'						: $('.updateSubtopicForm input[name=shortname]').val(),
			'idnumber'						: $('.updateSubtopicForm input[name=idnumber]').val(),
			'summary'						: $('.updateSubtopicForm textarea[name=summary]').val(),
		};
		$.ajax({
			url: '/local/b13_dashboard/load-ajax.php?classname=b13subtopics&method=update_subtopic',
			data: updateSubtopicData,
			type: 'POST',
			success: function(data) {
				if(data.error){
					swal({
						icon: 'error',
						showCloseButton: true,
						closeModal:true,
						text: 'ID number is already used for another subtopic ('+$('.updateSubtopicForm input[name=idnumber]').val()+')',
						button:'ok'
					})
				}else{
					doSuccessfully($text="Created Successfully!");  
				}
				console.log(data);
			}
		});
	}
}


//get Subject detail by subject 
function detailSubject($subjectid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=detail_subject&subjectid='+ $subjectid,
		type: 'GET',
		success: function(data) {
			console.log(data);
			//window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13examboards&method=view_detail_examboards&examboardid='+ $examboardid;
			 var detailSubject = data.data[0];
			 console.log(detailSubject);
			$('#updateSubjectForm .modal-body').html(
				'<input type="hidden" value="'+detailSubject.id+'" name="subjectid"/>'+
				'<div id="name-group" class="form-group">' +
                '<label for="name">Name</label>' +
                '<input type="text" class="form-control checkMaxLenth subjectname" name="name" value="'+detailSubject.name+'"'+
                '</div>'+
				'<div id="description-group" class="form-group">'+
                '<label for="description">description</label>'+
                '<input class="form-control" name="description" value="'+detailSubject.description+'">'+
								'</div>'+
								'<div class="form-group">'+
                                           ' <div class="selectImage">'+
                                                '<label>Select Your Image</label>'+
                                               ' <br/>'+
                                               ' <input type="file" name="subjectIcon" class="updateIconFile" required />'+
                                            '</div>'+
                                            '<div class="image_preview">'+
                                                '<img class="updatePreviewing" src="../b13_dashboard/classes/'+detailSubject.icon+ '"/>'+
                                            '</div>'+
                                           ' <br/>'+
                                            '<input type="hidden" class="updateInputUrlIcon" name="updateIcon" value=""/>'+
                                            '<input type="submit" class="updateSubmitSubjectIconBtn hide" style="display:none;" value="Upload" />'+
                                    '</div>'+
								'<div class="modal-footer">'+
                      '<button type="button" class="btn btn-default closeCurrentModel">Close</button>'+
                      '<button type="button" class="btn btn-success submitButton" onclick="updateSubject('+$subjectid+')">Update </button>'+
                    '</div>'
					);

					$(document).ready(function (e) {
						updateUploadIconFile();
					});

					$('.closeCurrentModel').click(function(){
						$('#updateSubject').modal('hide');
					});

					//not allowing special charecters in update textbox subject
					$(".updateSubjectForm input.subjectname").bind('keypress', function (event) {
						preventSpencialChars($text=textSpecial);
					});
					enterPressFunction(".updateSubjectForm",".enterPress");
		}
	});
}

//update subject
function updateSubject($subjectid){
	if ($.trim($('#updateSubjectForm input.subjectname').val()) === "") {
		swal({
			icon: 'error',
			showCloseButton: true,
			closeModal:true,
			text: 'The Name is required!',
			button:'ok'
			})
		return false;
	}else{
		var updateSubjectData = {
			'subjectid'						: $subjectid,
			'name'						: $('#updateSubjectForm input[name=name]').val(),
			'description'						: $('.updateSubjectForm input[name=description]').val(),
			'icon'       : 			$('.updateSubjectForm input.updateInputUrlIcon').val(),
		};
		$.ajax({
			url: '/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=update_subject',
			data: updateSubjectData,
			type: 'POST',
			success: function(data) {
				doSuccessfully($text="Updated Successfully!");
				//console.log(data);
			}
		});
	}
}

//disable subject
function disableSubject($subjectcid){
	swal({
		title: "Are you sure?",
		text: "The Subject will be disabled!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then(function(willDisable){
		if (willDisable){
			$.ajax({
						type: 'POST',
						url: '/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=disable_subject&subjectid='+$subjectcid,
				}).then(function successCallback(response) {
						swal("The Subject has been disabled!", {
								icon: "success",
						}).then(function(){
							location.reload();
						});
				}, function errorCallback(response) {
					console.log(response);
					swal({
						title: "Can not delete this Subject",
						text: "There is a subtopic belong to the selected subject has been added to the examboard!",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					});
				});
		} else {
			swal("Your subject is disabled!");
		}
	});
}

//disable subject
function enableSubject($subjectcid){
	swal({
		title: "Are you sure?",
		text: "The Subject will be enabled!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then(function(willEnable){
		if (willEnable){
			$.ajax({
						type: 'POST',
						url: '/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=enable_subject&subjectid='+$subjectcid,
				}).then(function successCallback(response) {
						swal("The Subject has been enabled!", {
								icon: "success",
						}).then(function(){
							location.reload();
						});
				}, function errorCallback(response) {
						//alert(response);
				});
		} else {
			swal("Your subject is safe!");
		}
	});
}



//remove subject options
function deleteSubjectOption($subjectcid, $subjectOptionid){
	swal({
		title: "Are you sure?",
		text: "The Subject Option will be deleted!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then(function(willDelete){
		if (willDelete) {
			$subjectOptionParams ={
				subjectid:$subjectcid,
				optionid: $subjectOptionid
			}
			$.ajax({
						type: 'POST',
						data: $subjectOptionParams,
						url: '/local/b13_dashboard/load-ajax.php?classname=b13subjectoptions&method=remove_option&subjectid='+$subjectcid+'&optionid='+ $subjectOptionid,
				}).then(function successCallback(response) {
						swal("The Option has been deleted!", {
								icon: "success",
						}).then(function(){
							location.reload();
						});
				}, function errorCallback(response) {
						//alert(response);
				});
		} else {
			swal("Your subject is safe!");
		}
	});
}

//remove subject options
function editSubjectOption($optionid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13subjectoptions&method=detail_option&optionid='+ $optionid,
		type: 'GET',
		success: function(data) {
			console.log(data);
			 var detailOption = data.data[0];
			 console.log(detailOption);
			$('.updateSubjectOption .modal-body').html(
				'<input type="hidden" value="'+detailOption.id+'" name="subjectid"/>'+
				'<div id="name-group" class="form-group">' +
                '<label for="name">Title</label>' +
                '<input type="text" class="form-control checkMaxLenth subjectoptiontitle" maxlength="50" name="title" value="'+detailOption.title+'">'+
                '</div>'+
				'<div id="description-group" class="form-group">'+
                '<label for="descriptionOption">description</label>'+
                '<input class="form-control" name="descriptionOption" value="'+detailOption.description+'">'+
								'</div>'+
								'<div class="modal-footer">'+
                      '<button type="button" class="btn btn-default closeCurrentModel">Close</button>'+
                      '<button type="button" class="btn btn-success submitButton" onclick="updateSubjectOption('+$optionid+')">Update</button>'+
                    '</div>'
					);
					$('.closeCurrentModel').click(function(){
						$('#editSubjectOption').modal('hide');
					});

					//not allowing special charecters in update textbox subject
					$(".updateSubjectOption input.subjectoptiontitle").bind('keypress', function (event) {
						preventSpencialCharsInEditSubjectOption($text=textSpecial);
					});
					enterPressFunction(".updateSubjectOption",".enterEditOptionPress");
					
		}
	});
}

function updateSubjectOption($optionid){
	if ($.trim($('.updateSubjectOption input[name=title]').val()) === "") {
		swal({
			icon: 'error',
			showCloseButton: true,
			closeModal:true,
			text: 'The Title is required!',
			button:'ok'
			})
		return false;
	}else{
		var updateSubjectOptionData = {
			'optionid'						: $optionid,
			'title'						: $('.updateSubjectOption input[name=title]').val(),
			'description'						: $('.updateSubjectOption input[name=descriptionOption]').val(),
		};
		$.ajax({
			url: '/local/b13_dashboard/load-ajax.php?classname=b13subjectoptions&method=update_option',
			data: updateSubjectOptionData,
			type: 'POST',
			success: function(data) {
				doSuccessfully($text="Updated Successfully!");
			}
		});
	}
}


//add topic to option
function addTopicToOption($topicid,$selectOptionId){
		console.log($topicid);
		console.log($selectOptionId);
		$topicOptionParams ={
			topicid:$topicid,
			optionid: $selectOptionId
		}
		$.ajax({
			url: '/local/b13_dashboard/load-ajax.php?classname=b13subjectoptions&method=add_option_topic&topicid='+$topicid+'&optionid='+ $selectOptionId,
			data: $topicOptionParams,
			type: 'POST',
			success: function(data) {
				console.log(data);
				//location.reload();
			}
			});
}
function getLearningMaterial($subtopicid,$topicid,$subjectid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13learningmodules&method=load_all_bookmodules&subtopicid='+ $subtopicid,
		data: {subtopicid: $subtopicid},
		type: 'POST',
		success: function(data) {
			var formTopicData = {
				'subtopic'	: $subtopicid,
			};
			$.ajax({
				type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
				url         : '/local/b13_dashboard/load-ajax.php?classname=b13quizmodule&method=create_quizmodules', // the url where we want to POST
				data        : formTopicData, // our data object
				dataType    : 'json', // what type of data do we expect back from the server
				encode          : true
			});
			$.ajax({
				type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
				url         : '/local/b13_dashboard/load-ajax.php?classname=b13learningmodules&method=create_learningmodules', // the url where we want to POST
				data        : formTopicData, // our data object
				dataType    : 'json', // what type of data do we expect back from the server
				encode          : true
			})
			.done(function(data) {
				window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13learningmodules&method=dashboard&subjectid='+$subjectid+'&topicid='+$topicid+'&subtopicid='+$subtopicid;
			});
		}
	});
}

function update_learning_material(subjectid,topicid,subtopicid,bookid,cid){
	window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13learningmodules&method=update_learning_material&subjectid='+subjectid+'&topicid='+topicid+'&subtopicid='+subtopicid+'&bookid='+bookid+'&edit='+ cid;
}
function update_quiz(subjectid,topicid,subtopicid,quizid,cid){
	window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13quizmodule&method=update_learning_material&subjectid='+subjectid+'&topicid='+topicid+'&subtopicid='+subtopicid+'&quizid='+quizid+'&view='+ cid;
}
function view_detail_examboards($examboardid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_subject_by_examboard&examboardid='+ $examboardid,
		data: {examboardid: $examboardid},
		type: 'POST',
		success: function(data) {
			window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=b13examboards&method=view_detail_examboards&examboardid='+ $examboardid;
			// console.log(data);
		}
	});
}


function load_all_subjectsInExamboard(){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13subjects&method=load_all_subjects',
		type: 'GET',
		success: function(data) {
			var allSubjects = data.data;
			$(".loading-subtopic-box").css('display','none !important');
			$.each(allSubjects, function( key, value ) {
				// console.log(value);
				if($('#allSubjects').find('.subtopic'+value.id).length == 0){
					$('#allSubjects').append('<li class="subject'+value.id+' visible_'+value.visible+'"><label><input class="subject'+value.id+'" type="checkbox" name="subtopicid"  value="'+ value.id +'">'+ value.name +'</label></li>')
				}
			});
			if($('.examboard-page .examboard-box .container-examboard-box').hasClass('container-examboard-box')){
				new PerfectScrollbar('.examboard-page .examboard-box .container-examboard-box');
			};
			$('#allSubjects li').each(function(){
				$(this).find('input').click(function(){
					var getSubjectID = $(this).val();
					if(this.checked){
						load_all_subtopics_by_subject(getSubjectID);
						$(document).ready(function(){
							new PerfectScrollbar('.container-examboard-box');
						});
					}else{
						$('#allSubtopics li.subject'+getSubjectID).remove(); 
					}
				});
			});
		}
	});
}

//register subtopic to examboard
function selectSubtopicByExamboard($getSutopicdId,$getExamboardId){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=register_subtopic_examboard&subtopicid='+ $getSutopicdId+'&examboardid='+$getExamboardId,
		data: {subtopicid: $getSutopicdId,examboardid: $getExamboardId},
		type: 'POST',
		success: function(data) {
			// console.log(data);
			$(".loading-subtopic-box").css('display','none !important');
			selected_subtopics_by_subject($getExamboardId);
		}
	});
}

//unregister subtopic to examboard
function removeSubtopicByExamboard($getSutopicdId,$getExamboardId){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=unregister_subtopic_examboard&subtopicid='+ $getSutopicdId+'&examboardid='+$getExamboardId,
		data: {subtopicid: $getSutopicdId,examboardid: $getExamboardId},
		type: 'POST',
		success: function(data) {
			// console.log(data);
			$(".loading-subtopic-box").css('display','none !important');
			selected_subtopics_by_subject($getExamboardId);
		}
	});
}

function load_all_subtopics_by_subject($subjectid){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13subtopics&method=load_all_subtopics_by_subject&subjectid='+ $subjectid,
		data: {subjectid: $subjectid},
		type: 'POST',
		success: function(data) {
			var allsubtopics = data.data;
			console.log(allsubtopics);
			var getExamboardId = $('input[name="examboardid"]').val();
			$.each(allsubtopics, function( key, value ) {
				if($('#allSubtopics').find('.subtopic'+value.id).length == 0){
					$('#allSubtopics').append('<li id="'+value.id+'" onclick="selectSubtopicByExamboard('+value.id+','+getExamboardId+')" class="subtopicid'+value.id+' subject'+$subjectid+' visible_'+value.visible+'"><label><input class="subtopic'+value.id+'" type="checkbox" name="subtopicid" value="'+ value.id +'">'+ value.subject+'_'+value.topic+'_'+value.fullname +'</label></li>')
				}
				$('.subtopicid'+value.id).on('click',function(){
					$(this).addClass('checked');
					setTimeout(function(){
						$('#allSubtopics .subtopicid'+value.id+' input').attr('disabled', true);
						$('#allSubtopics .subtopicid'+value.id+' input').attr('checked', true);
					}, 100);
				});
				//check selected subtopics
				$('#unregisterSubtopics li').each(function(){
					if($(this).prop('id') == value.id){
						$('#allSubtopics .subtopicid'+value.id).addClass('checked');
						$('#allSubtopics .subtopicid'+value.id+' input').attr('disabled', true);
						$('#allSubtopics .subtopicid'+value.id+' input').attr('checked', true);
					}
				});
			});
			if($('.examboard-page .examboard-box .container-examboard-box').hasClass('selectSubtopicInExamboard')){
				new PerfectScrollbar('.selectSubtopicInExamboard');
			};
		}
	});
}

function selected_subtopics_by_subject(getExamboardId){
	$.ajax({
		url: '/local/b13_dashboard/load-ajax.php?classname=b13examboards&method=load_subtopic_by_examboard&examboardid='+ getExamboardId,
		data: {examboardid: getExamboardId},
		type: 'POST',
		success: function(data) {
			var selectedsubtopics = data.data;
			// console.log(selectedsubtopics);
			$.each(selectedsubtopics, function( key, value ) {
				console.log(value);
				if($('#unregisterSubtopics').find('.subtopic'+value.id).length == 0){
					$('#unregisterSubtopics').prepend('<li id="'+value.id+'" onclick="removeSubtopicByExamboard('+value.id+','+getExamboardId+')"  class="subtopicid'+value.id+' visible_'+value.visible+'"><label><input checked class="subtopic'+value.id+'" type="checkbox" name="subtopicid" value="'+ value.id +'">'+ value.subject+'_'+value.topic+'_'+ value.fullname +'</label></li>')
				}
				$('#unregisterSubtopics .subtopicid'+value.id).click(function(){
					//check selected subtopics
					$('#allSubtopics li').each(function(){
						if($(this).prop('id') == value.id){
							$('#allSubtopics .subtopicid'+value.id).removeClass('checked');
							$('#allSubtopics .subtopicid'+value.id+' input').removeAttr('disabled');
							$('#allSubtopics .subtopicid'+value.id+' input').removeAttr('checked');
							$('#allSubtopics .subtopicid'+value.id+' input').prop('checked', false);
						}
					});
					$(this).remove();
				});
			});	
			if($('.examboard-page .examboard-box .container-examboard-box').hasClass('selectedSubtopicInExamboard')){
				new PerfectScrollbar('.selectedSubtopicInExamboard');
			};

		}
	});
}

$(document).ready(function(){

	// $(".idnumberexamboard").on("keypress keyup blur",function (event) {    
	// 	$(this).val($(this).val().replace(/[^\d].+/, ""));

	// 	if($(this).val().search(/^0/) != -1){
	// 		$(this).val("");
	// 	}
	// 	if ((event.which < 48 || event.which > 57)) {
	// 		 event.preventDefault();
	// 	}
	//  });
	$(".idnumberexamboard").inputFilter(function(value) {
		return /^\d*$/.test(value); });

	//close x button reload page 
	$('#addNewB13Model .close').click(function(){
		location.reload();
	});
	//maxlenth of input text
	$(".checkMaxLenth").on('input', function() {
		if ($(this).val().length>=50) {
			return false;
			// swal({
			// 	icon: 'error',
			// 	showCloseButton: true,
			// 	closeModal:true,
			// 	text: 'Name is too longer!',
			// 	button:'ok'
			// })
		}
	});
	//not allowing special charecters in textbox subject
	$(".createNewSubject input.subjectname").bind('keypress', function (event) {
		preventSpencialChars($text=textSpecial);
	});
	enterPressFunction(".createNewSubject",".enterPress");
	//not allowing special charecters in textbox topic
	$(".createNewTopic input.topicname").bind('keypress', function (event) {
		preventSpencialChars($text=textSpecial);
	});
	enterPressFunction(".createNewTopic",".enterPress");
	//not allowing special charecters in textbox sub topic
	$(".createNewSubtopic input.subtopicname").bind('keypress', function (event) {
		preventSpencialChars($text=textSpecial);
	});
	enterPressFunction(".createNewSubtopic",".enterPress");
	//not allowing special charecters in textbox sub examboard
	$(".createNewExamboard input.examboardtitle").bind('keypress', function (event) {
		preventSpencialChars($text=textSpecial);
	});
	enterPressFunction(".createNewExamboard",".enterPress");
	//not allowing special charecters in textbox sub examboard
	$(".createNewSubjectOption input.subjectoptiontitle").bind('keypress', function (event) {
		preventSpencialCharsInSubjectOption($text=textSpecial);
	});
	enterPressFunction(".createNewSubjectOption",".enterOptionPress");
});
	//report
	//get user visited
	function getanalytics(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=dashboard';
	}
	//download teacher email
	function downloadTeacherEmail(){
		$.ajax({
			url: '/local/b13_dashboard/load-ajax.php?classname=reports&method=download_teacher_report',
			type: 'GET',
			success: function(data) {
				var downloadTeacherEmailData = data.data;
				console.log(downloadTeacherEmailData);
			}
		});
	}
	//get signedUpUser 
	function getUserSignedUp(){
				window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=users_signed_up_dashboard';
	}
	//get user from each school

	function getUserFromEachSchool(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=users_from_each_school_dashboard';
	}

	//get user visited
	function getUserVisited(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=user_visited_dashboard';
	}

	//get user accessed Learning content

	function getUserAccessedLearningContent(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=users_accessed_learning_content_dashboard';
	}

	//get user completed test

	function getUserCompletedTest(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=users_completed_test_dashboard';
	}

	//get topic accessed

	function getTopicAccessed(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=topic_accessed_dashboard';
	}

	//get subtopic accessed
	function getSubtopicAccessed(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=subtopic_accessed_dashboard';
	}

	//get Users have accessed learning content 
	function getTeacherEmail(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=teacher_report_dashboard';
	}
	//get Users have accessed learning content 
	function getBookReport(){
		window.location.href = '/local/b13_dashboard/open-dashboard.php?classname=reports&method=getBookReport';
	}
	$(document).ready(function(){
		// user signed up
		if($('.loadUsersBox').hasClass('loadUsersBox')){
			$('.loadUsersBox #pagination').pagination({
				dataSource: function(done){
					$.ajax({
						type: 'GET',
						url: '/local/b13_dashboard/open-dashboard.php?classname=reports&method=load_users',
						success:function(response){
							done(response.data);
							//console.log(response.data.length);
							$('.teacherItem').text(response.data.length + " Users");
						}
					});
				},
				pageSize: 12,
				showPageNumbers: true,
				callback: function(data, pagination) {
							var getHtmlData = [];
							$.each(data, function( index, item ) {
								var dataHtml = '<tr>'+
									'<td style="text-align:center;">'+item.index+'</td>'+
									'<td>'+item.username+'</td>'+
									'<td>'+item.firstname+'</td>'+
									'<td>'+item.lastname+'</td>'+
									'<td>'+item.email+'</td>'+
									'<td>'+item.institution+'</td>'+
								'</tr>';
								getHtmlData.push(dataHtml);
							});
							console.log(data);
							$('.loadUsersBox tbody').html(getHtmlData);
				}
			});
		}

		// user from each school
		if($('.loadUserFromEachSchoolBox').hasClass('loadUserFromEachSchoolBox')){
			$('.loadUserFromEachSchoolBox #pagination').pagination({
				dataSource: function(done){
					$.ajax({
						type: 'GET',
						url: '/local/b13_dashboard/open-dashboard.php?classname=reports&method=report_total_users_from_school',
						success:function(response){
							done(response.data);
							totalCount = 0;
							$.each(response.data, function( index, item ) {
								totalCount += parseInt(item.totalstudents) ;
							});
							$('.schoolItem').html(totalCount + " Users");
						}
					});
				},
				pageSize: 12,
				showPageNumbers: true,
				callback: function(data, pagination) {
							var getHtmlData = [];
							$.each(data, function( index, item ) {
								var dataHtml = '<tr>'+
									'<td style="text-align:center;">'+item.index+'</td>'+
									'<td>'+item.institution+'</td>'+
									'<td>'+item.totalstudents+'</td>'+
								'</tr>';
								getHtmlData.push(dataHtml);
							});
							console.log(data);
							$('.loadUserFromEachSchoolBox tbody').html(getHtmlData);
				}
			});
		}

		// user accessed learning content
		if($('.userAccessLearningContent').hasClass('userAccessLearningContent')){
			$('.userAccessLearningContent #pagination').pagination({
				dataSource: function(done){
					$.ajax({
						type: 'GET',
						url: '/local/b13_dashboard/open-dashboard.php?classname=reports&method=load_user_learning_material',
						success:function(response){
							done(response.data);
							console.log(response);
							$('.userAccessContentItem').text(response.data.length + " Users");
						}
					});
				},
				pageSize: 12,
				showPageNumbers: true,
				callback: function(data, pagination) {
							var getHtmlData = [];
							$.each(data, function( index, item ) {
								var dataHtml = '<tr>'+
									'<td style="text-align:center">'+item.index+'</td>'+
									'<td>'+item.fullname+'</td>'+
									'<td>'+item.numberusers+'</td>'+
								'</tr>';
								getHtmlData.push(dataHtml);
							});
							console.log(data);
							$('.userAccessLearningContent tbody').html(getHtmlData);
				}
			});
		}

		// user completed test
		if($('.userCompletedTest').hasClass('userCompletedTest')){
			$('.userCompletedTest #pagination').pagination({
				dataSource: function(done){
					$.ajax({
						type: 'GET',
						url: '/local/b13_dashboard/open-dashboard.php?classname=reports&method=report_number_user_completion',
						success:function(response){
							done(response.data);
							$('.userCompletedTestTotal').text(response.data.length + " Users");
						}
					});
				},
				pageSize: 12,
				showPageNumbers: true,
				callback: function(dataTest, pagination) {
							var getHtmlDataTest = [];
							console.log(dataTest);
							$.each(dataTest, function( index, item ) {
								var dataHtmlTest = '<tr>'+
									'<td style="text-align:center;">'+item.index+'</td>'+
									'<td>'+item.subtopicname+'</td>'+
									'<td>'+item.numberusers+'</td>'+
								'</tr>';
								getHtmlDataTest.push(dataHtmlTest);
							});
							console.log(dataTest);
							$('.userCompletedTest tbody').html(getHtmlDataTest);
				}
			});
		}

		// Topic Accessed
		if($('.topicAccessed').hasClass('topicAccessed')){
			$('.topicAccessed #pagination').pagination({
				dataSource: function(done){
					$.ajax({
						type: 'GET',
						url: '/local/b13_dashboard/open-dashboard.php?classname=reports&method=report_number_user_access_topic',
						success:function(response){
							done(response.data);
							// $('.topicAccessedTotal').text(response.data.length + " Users");
						}
					});
				},
				pageSize: 12,
				showPageNumbers: true,
				callback: function(data, pagination) {
							var getHtmlData = [];
							var i =0;
							$.each(data, function( index, item ) {
								var dataHtml = '<tr>'+
									'<td style="text-align:center">'+ item.index +'</td>'+
									'<td>'+item.name+'</td>'+
									'<td>'+item.nousers+'</td>'+
								'</tr>';
								getHtmlData.push(dataHtml);
							});
							console.log(data);
							$('.topicAccessed tbody').html(getHtmlData);
				}
			});
		}

		// subTopic Accessed
		if($('.subtopicAccessed').hasClass('subtopicAccessed')){
			$('.subtopicAccessed #pagination').pagination({
				dataSource: function(done){
					$.ajax({
						type: 'GET',
						url: '/local/b13_dashboard/open-dashboard.php?classname=reports&method=report_number_user_access_subtopic',
						success:function(response){
							done(response.data);
							// $('.subtopicAccessedTotal').text(response.data.length + " Users");
						}
					});
				},
				pageSize: 12,
				showPageNumbers: true,
				callback: function(data, pagination) {
							var getHtmlData = [];
							$.each(data, function( index, item ) {
								var dataHtml = '<tr>'+
									'<td style="text-align:center">'+item.index+'</td>'+
									'<td>'+item.fullname+'</td>'+
									'<td>'+item.nousers+'</td>'+
								'</tr>';
								getHtmlData.push(dataHtml);
							});
							console.log(data);
							$('.subtopicAccessed tbody').html(getHtmlData);
				}
			});
		}

		// teacher email
		if($('.teacherEmail').hasClass('teacherEmail')){
			$('#teacherEmail #pagination').pagination({
				dataSource: function(done){
					$.ajax({
						type: 'GET',
						url: '/local/b13_dashboard/open-dashboard.php?classname=reports&method=load_teachers',
						success:function(response){
							done(response.data);
							$('.teacheremailItem').text(response.data.length + " email");
						}
					});
				},
				pageSize: 12,
				// showGoInput: true,
				// showGoButton: true,
				showPageNumbers: true,
				callback: function(data, pagination) {
							var getHtmlData = [];
							$.each(data, function( index, item ) {
								var dataHtml = '<tr>'+
									'<td style="text-align:center">'+item.index+'</td>'+
									'<td>'+item.teachername+'</td>'+
									'<td>'+item.teacheremail+'</td>'+
								'</tr>';
								getHtmlData.push(dataHtml);
							});
							$('.teacherEmail tbody').html(getHtmlData);

					}
				});
			}
		});

			$.ajax({
				type: 'GET',
				url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=get_content_corrections',
				success:function(response){
					// console.log(response.data);
					if($('.issueReport').hasClass('issueReport')){
					$('.issueReport .pagination').pagination({
						dataSource: response.data,
						pageSize: 12,
						showPageNumbers: true,
						callback: function(data, pagination) {
									var getHtmlData = [];
									var pending = "Pending";
									var done = "Done";
									$.each(data, function( index, item ) {
										var dataHtml = '<tr>'+
											'<td style="text-align:center">'+item.id+'</td>'+
											'<td>'+item.title+'</td>'+
											'<td>'+item.body+'</td>'+
											'<td>'+item.url+'</td>'+
											'<td>  <fieldset style="margin:0px;padding:0px;border:none;" class="groupIssue" id="groupIssue_'+item.id+'"><label><input type="radio" class="pending" name="groupIssue_'+item.id+'" value="'+item.id+'" id="updateProccessingStatus_'+item.id+'"/> Pending </label><label class="'+item.status+'"><input type="radio" name="groupIssue_'+item.id+'" value="'+item.id+'" id="updateDoneStatus_'+item.id+'"/> Done </label></fieldset>' +
										'</tr>';
										getHtmlData.push(dataHtml);
										console.log(item);
										$(document).ready(function(){

											$('#updateProccessingStatus_'+item.id).on('change',function(){
												$.ajax({
													type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
													url         : '/local/b13_dashboard/load-ajax.php?classname=b13students&method=update_content_correction', // the url where we want to POST
													data        : {correctionid: $(this).val(), status:'Pending'}, // our data object
													dataType    : 'json', // what type of data do we expect back from the server
													encode          : true
												}).done(function(response) {
													console.log(response);
														if(response.data.error){
																	swal({
																		icon: 'warning',
																		showCloseButton: true,
																		closeModal:true,
																		text: response.data.error,
																		button:'Ok'
																	}).then(function(){
																		location.reload();
																	});
																}else{
																	swal({
																		icon: 'success',
																		showCloseButton: true,
																		closeModal:true,
																		text: 'Sent successfully!!!',
																		button:'Ok'
																	}).then(function(){
																		location.reload();
																	});
																}
												});
											});
											$('#updateDoneStatus_'+item.id).on('change',function(){
												$.ajax({
													type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
													url         : '/local/b13_dashboard/load-ajax.php?classname=b13students&method=update_content_correction', // the url where we want to POST
													data        : {correctionid: $(this).val(), status:'Done'}, // our data object
													dataType    : 'json', // what type of data do we expect back from the server
													encode          : true
												}).done(function(response) {
													console.log(response);
														if(response.data.error){
																	swal({
																		icon: 'warning',
																		showCloseButton: true,
																		closeModal:true,
																		text: response.data.error,
																		button:'Ok'
																	}).then(function(){
																		location.reload();
																	});
																}else{
																	swal({
																		icon: 'success',
																		showCloseButton: true,
																		closeModal:true,
																		text: 'Sent successfully!!!',
																		button:'Ok'
																	}).then(function(){
																		location.reload();
																	});
																}
												});
											});


											$('#groupIssue_'+item.id+' label').each(function(){
												console.log($(this));
												if($(this).hasClass('Done')){
													$(this).find('input').attr('checked', true);
												}else{
													$(this).find('input.pending').attr('checked', true);
												}
											});
										});
									});
									$('.issueReport tbody').html(getHtmlData);
							}
						});
					}
				}
			});
		function userAccessFromto(startday, endday){
				
				console.log(startday);
				console.log(endday);

				var dates1 =  startday.split("/");
				var newDate1 = dates1[1]+"/"+dates1[0]+"/"+dates1[2];
				var dates2 =  endday.split("/");
				var newDate2 = dates2[1]+"/"+(parseInt(dates2[0])+1)+"/"+dates2[2];

				console.log(newDate1);
				console.log(newDate2);


				var startFrom = new Date(newDate1).getTime();
				var endTo = new Date(newDate2).getTime();
				
				console.log(startFrom);
				console.log(endTo);

				var datetime = 24*60*60*1000;
				var diffDays = Math.round(Math.abs(( startFrom - endTo)/(datetime)));
				console.log(diffDays);
				localStorage.setItem("startFrom", startFrom);
				localStorage.setItem("endTo", endTo);
				localStorage.setItem("diffDays", diffDays);


				$.ajax({
					url: '/local/b13_dashboard/load-ajax.php?classname=reports&method=report_number_user_revisit&startdate='+startFrom+'&numberofday='+diffDays,
					type: 'GET',
					success: function(data) {
						var userAccessFromToData = data.data;
						console.log(userAccessFromToData);
						//Bar chart
						var date = new Date();
						var convertDate = date.toDateString();
						console.log(date);
						var dateChart = [];
						var numberOfUserChar = [];
						var xDateChart = [];
						$.each(userAccessFromToData, function( index, value ) {
							dateChart.push(moment("/Date("+(value.time)*1000+")/").format("DD/MM/YYYY"));
							numberOfUserChar.push(value.nousers);
							xDateChart.push({x:moment("/Date("+(value.time)*1000+")/").format("DD/MM/YYYY"), y:value.nousers})
						});
						console.log(dateChart);
						console.log(numberOfUserChar);
						console.log(xDateChart);

						if($("canvas").length){

							var ctx = document.getElementById("fromToReport").getContext("2d");

							var data = {
								labels: dateChart,
								datasets: [
									{
										data: numberOfUserChar,
										backgroundColor: [
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
											'rgba(54, 162, 235, 1)',
										],
										borderColor: [
										'rgba(54, 162, 235, 1)',
										],
										borderWidth: 1,
										hoverBackgroundColor: [
											"#46BFBD",
										]
									}]
							};
					
							Chart.pluginService.register({
								beforeRender: function (chart) {
									if (chart.config.options.showAllTooltips) {
										// create an array of tooltips
										// we can't use the chart tooltip because there is only one tooltip per chart
										chart.pluginTooltips = [];
										chart.config.data.datasets.forEach(function (dataset, i) {
											chart.getDatasetMeta(i).data.forEach(function (sector, j) {
												chart.pluginTooltips.push(new Chart.Tooltip({
													_chart: chart.chart,
													_chartInstance: chart,
													_data: chart.data,
													_options: chart.options,
													_active: [sector]
												}, chart));
											});
										});
					
										// turn off normal tooltips
										chart.options.tooltips.enabled = false;
									}
								},
								afterDraw: function (chart, easing) {
									if (chart.config.options.showAllTooltips) {
										// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
										if (!chart.allTooltipsOnce) {
											if (easing !== 1)
												return;
											chart.allTooltipsOnce = true;
										}
										// turn on tooltips
										chart.options.tooltips.enabled = true;
										Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
											tooltip.initialize();
											tooltip.update();
											// we don't actually need this since we are not animating tooltips
											tooltip.pivot();
											tooltip.transition(easing).draw();
										});
										chart.options.tooltips.enabled = false;
									}
								}
							})
					
							var myPieChart = new Chart(ctx, {
								type: 'bar',
								data: data,
								options: {
									scales: {
										xAxes: [{
											barPercentage: 0.4,
											barThickness: 2,
											maxBarThickness: 8,
											minBarLength: 2,
											gridLines: {
												offsetGridLines: true
											}
										}]
									},
									showAllTooltips: true,
									events: false,
									legend: {
										display: false,
									},
									tooltips: {
											custom: function(tooltip) {
												if (!tooltip) return;
												tooltip.displayColors = false;
												tooltip.yPadding =3;
											},
											callbacks: {
												// use label callback to return the desired label
												label: function(tooltipItem, data) {
													console.log(tooltipItem.yLabel);
													if(tooltipItem.yLabel>1){
														return tooltipItem.yLabel + " ";
													}else if((tooltipItem.yLabel==0)){
														return tooltipItem.yLabel
													}else{
														return tooltipItem.yLabel;
													}
												},
												// remove title
												title: function(tooltipItems, data) {return;},
											}
									},
									
								}
							});
						}
						console.log(data);
					}
				});
		}	

		function downloadUserAccessFromto(){
			var startFrom = localStorage.getItem("startFrom");
			var diffDays = localStorage.getItem("diffDays");

			window.open('/local/b13_dashboard/load-ajax.php?classname=reports&method=download_number_user_revisit&startdate=' + startFrom + '&numberofday=' +diffDays , '_blank');
		
		}
		//get visits daily, weekly, monthly
		$(document).ready(function(){
			$(function() {
				$( "#tabs" ).tabs();
			});

			// $(".startdate").daterangepicker({
			// 	singleDatePicker: true,
			// 	showDropdowns: true,
			// 	minYear: 1901,
			// 	maxYear: parseInt(moment().format('YYYY'),10)
			//   }, function(start, end, label) {
			// 	var years = moment().diff(start, 'years');
			// 	// userAccessDay(start);
			// });
			  $(function() {
				var start = moment();
				var end = moment();
				function cb(start, end) {
					console.log(start.format("DD/MM/YYYY"));
					$('.startArangeFromTo').attr('value',start.format("DD/MM/YYYY") + ' - ' + end.format("DD/MM/YYYY"));
					userAccessFromto(start.format("DD/MM/YYYY"), end.format("DD/MM/YYYY"));
				}
				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					showCustomRangeLabel:true,
					alwaysShowCalendars:true,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					},
					locale: {
						format: 'DD/MM/YYYY'
					}
				}, cb);

				cb(start, end);
			});

			$("#dwnUserVisit").click(function(){
				downloadUserAccessFromto();
			});
		});
		
//report
$(document).ready(function(){
	$('.teacherEmail').css('max-height',($(window).height() - 230));
	$('.loadUsers').css('max-height',($(window).height() - 180));
});

