 // window loading
 $(window).on('load', function(){
    $(".se-pre-con").fadeOut();
 });
 window.onload=new function() { 
    $(".se-pre-con").fadeOut();
 };

var url = window.location.href;
var parts = url.split("/");
var last_part = parts[parts.length-2];
if(last_part === 'my'){
    window.location.href = '/student/#/student-view'
}
   
    
$(document).ready(function(){
    var $logoHeight = $('.logo').outerHeight();
    var $footerHeight = $('.wrapper-footer').outerHeight();
    $('.page-content-box').css('height', $(window).height() - $logoHeight - $footerHeight - 40);
    $('.authen-page .page-content-box').css('height', $(window).height() - $logoHeight - $footerHeight - 110);
    $('#page-login-signup .authen-page .page-content-box').css('height', $(window).height() - $logoHeight - $footerHeight - 20);

    $('#page-login-index .authen-box #username').attr('placeholder','Email');

    if($('.page-content-box').hasClass('page-content-box')){
        new PerfectScrollbar('.page-content-box');
    }
    //get School list 
    getSchoolList(); 
    function getSchoolList(){
        $("#school_list").on("change paste keyup", function() {
            var text = $(this).val(); 
            console.log(text);
            $.ajax({
                url: '/local/b13_dashboard/load-ajax.php?classname=b13students&method=list_schools&text='+text,
                type: 'GET',
                success: function(data) {
                    console.log(data.data);
                    $('#school_list').autocomplete({
                        minLength: 2,
                        source: function (request, response) {
                            response($.map(data.data, function (obj, key) {
                                
                                var schoolname = obj.schoolname.toUpperCase();
                                
                                if (schoolname.indexOf(request.term.toUpperCase()) != -1) {				
                                    return {
                                        label: obj.schoolname, // Label for Display
                                        value: obj.id // Value
                                    }
                                } else {
                                    return null;
                                }
                            }));			
                        },    
                        focus: function(event, ui) {
                            event.preventDefault();
                        },
                        // Once a value in the drop down list is selected, do the following:
                        select: function(event, ui) {
                            event.preventDefault();
                            // place the person.given_name value into the textfield called 'select_origin'...
                            $('#school_list').val(ui.item.label);
                            // ... any other tasks (like setting Hidden Fields) go here...
                        }
                });	


                }
            });
        });
    }


    window.onload=new function() { 
        $(".se-pre-con").fadeOut();
    };
    $('.book_toc_none li > a').prepend('<i class="fa fa-edit"></i>');
    $('.book_toc_none li > strong').prepend('<i class="fa fa-edit"></i>');
    $('#id_category optgroup').each(function() {
        var text = $(this).find('option').text();
        $(this).find('option').text(text.replace('Default for studentquiz', 'Question')); 
    });
    $('#page-mod-studentquiz-view .questionbankwindow h2').text('Question');
    $('#chooseform .alloptions label[for="item_qtype_multichoice"] .typename').text('Single choice');
    $('#page-question-type-shortanswer #id_generalheader .fcontainer .form-group').each(function(){
        if($(this).find('.col-form-label').text().indexOf('Question name') > 0){
            $(this).find('.col-form-label').text('Question group');
        }
    });

    $('#page-question-type-multichoice #id_generalheader .fcontainer .form-group').each(function(){
        if($(this).find('.col-form-label').text().indexOf('Question name') > 0){
            $(this).find('.col-form-label').text('Question group');
        }
    });
      
      $("#page-question-type-shortanswer .fcontainer div[data-fieldtype='group'] input").bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9- ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        } 
      });
    $('label[for="id_submitbutton"]').next('span[data-fieldtype="submit"]').append('<span class="floading"></span>');
    $('.floading').css({
            "position": "fixed",
            "left": "0px",
            "top": "0px",
            "width": "100%",
            "height": "100%",
            "z-index": "9999",
            "background": "url(../../local/b13_dashboard/assets/dashboard/img/processing-icon.gif) center no-repeat #fff"
    });
    $('#page-mod-studentquiz-view .se-pre-con').empty();
    setTimeout(function(){
        $('.floading').fadeOut( "slow" );
    }, 3000); 
});
function checkPasswordMatch() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmpassword").val();
    $('#password, #confirmpassword').on('change', function(){
        if (password != confirmPassword){
                if($(this).val() == '' || confirmPassword == ''){
                    $("#divCheckPasswordMatch").html('');
                }else if(password != confirmPassword && confirmPassword != '' && password != ''){
                    $("#divCheckPasswordMatch").html('<span class="badge badge-danger">Passwords do not match!</span>');
                }else if(password == confirmPassword && password !=''){
                    $("#divCheckPasswordMatch").html('<span class="badge badge-success">Passwords match!</span>');
                }
    
            // $("#confirmpassword").on('change', function(){
            //     if($(this).val() == password){
            //         $("#divCheckPasswordMatch").html('<span class="badge badge-success">Passwords match!</span>');
            //     }else{
            //         $("#divCheckPasswordMatch").html('<span class="badge badge-danger">Passwords do not match!</span>');
            //     }
            // });
                if(confirmPassword == '' || password == ''){
                    $("#divCheckPasswordMatch").html('');
                }else if(password != confirmPassword && confirmPassword != '' && password != ''){
                    $("#divCheckPasswordMatch").html('<span class="badge badge-danger">Passwords do not match!</span>');
                }else if(password == confirmPassword && confirmPassword !=''){
                    $("#divCheckPasswordMatch").html('<span class="badge badge-success">Passwords match!</span>');
                }
    
            $('.checkAction').addClass('not-allowed');
        }else if(password == ""){
            $('.checkAction').addClass('not-allowed');
        }
        else if(password == confirmPassword){
            // $("#password").on('change', function(){
            //     if($(this).val() == confirmPassword){
            //         $("#divCheckPasswordMatch").html('<span class="badge badge-success">Passwords match!</span>');
            //     }else{
            //         $("#divCheckPasswordMatch").html('<span class="badge badge-danger">Passwords do not match!</span>');
            //     }
            // });
            // $("#confirmpassword").on('change', function(){
            //     if($(this).val() == '' || password == ''){
            //         $("#divCheckPasswordMatch").html('');
            //     }else if($(this).val() != password && password != '' && confirmPassword != ''){
            //         $("#divCheckPasswordMatch").html('<span class="badge badge-danger">Passwords do not match!</span>');
            //     }else if($(this).val() == password && password != ''){
            //         $("#divCheckPasswordMatch").html('<span class="badge badge-success">Passwords match!</span>');
            //     }
            // });
            $("#divCheckPasswordMatch").html('<span class="badge badge-success">Passwords match!</span>');
            $('.checkAction').removeClass('not-allowed');
        }
    });
    

}

$(document).ready(function () {
    $(window).resize(function() {
        if ($(window).width() <= 641) {
            $('.submit-form .cancelbtn').remove().insertAfter($('.submit-form .loginbtn'));
            $('.submit-form .cancelBtnRegister').remove().insertAfter($('.submit-form .signupBtnRegister'));

        } else {
            $('.submit-form .cancelbtn').remove().insertBefore($('.submit-form .loginbtn'));
            $('.submit-form .cancelBtnRegister').remove().insertBefore($('.submit-form .signupBtnRegister'));

        }
      });
        if ($(window).width() <= 641) {
            $('.submit-form .cancelbtn').remove().insertAfter($('.submit-form .loginbtn'));
            $('.submit-form .cancelBtnRegister').remove().insertAfter($('.submit-form .signupBtnRegister'));

        } else {
            $('.submit-form .cancelbtn').remove().insertBefore($('.submit-form .loginbtn'));
            $('.submit-form .cancelBtnRegister').remove().insertBefore($('.submit-form .signupBtnRegister'));

        }
    checkPasswordMatch();
   $("#password, #confirmpassword").keyup(checkPasswordMatch);
});

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {return true;}
    else {return false;}
}
$(document).ready(function(e){
    $('.signupBtn').click(function(){
    var sEmail = $('#signupForm #email').val();
        if ($.trim(sEmail).length == 0) {
            swal({
                icon: 'error',
                showCloseButton: true,
                closeModal: true,
                text: 'Please enter valid email address',
                button: 'ok'
            });
            e.preventDefault();
        }
        if (validateEmail(sEmail)) {
            var fullname = $('#signupForm #fullname').val();
            if(fullname.length == 0){
                swal({
                    icon: 'error',
                    showCloseButton: true,
                    closeModal: true,
                    text: 'Please enter Full Name',
                    button: 'ok'
                });
                e.preventDefault();
            }else{
            var schoolValue = $('#school option:selected').val();
            var formSignupData = {
                'fullname'						: $('input[name=fullname]').val(),
                'username'						: $('input[name=username]').val(),
                'email'						: $('input[name=email]').val(),
                'school'						: $('input[name=school]').val(),
                'password'						: $('input[name=password]').val(),
            };
            $.ajax({
                url: '/local/b13_dashboard/load-ajax.php?classname=b13signup&method=signup',
                data: formSignupData,
                type: 'POST',
                success: function(data) {
                    console.log(data);
                    if(data.error){
                        swal({
                            icon: 'error',
                            showCloseButton: true,
                            closeModal:true,
                            text: data.error,
                            button:'Ok'
                            })
                        e.preventDefault();
                    }else{
                        swal({
                            icon: 'success',
                            showCloseButton: true,
                            closeModal:true,
                            text: "Registration complete. You can now log in to LearnLock.",
                            button:'Login'
                            }).then(function(){
                                window.location.href = '/login';
                            });
                    }
                   
                       
                }
            });
         }
        }
        else {
            swal({
                icon: 'error',
                showCloseButton: true,
                closeModal: true,
                text: 'Invalid Email Address',
                button: 'ok'
            });
            e.preventDefault();
        }
    });
});
