<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_b13_dashboard
 * @copyright  2019 b13 {@link https://www.b13technology.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_local_b13_dashboard_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2017061102) {
        $table = new xmldb_table('b13_dashboard_events');
        $field1 = new xmldb_field('subject',
            XMLDB_TYPE_CHAR,
            '255',
            null,
            XMLDB_NOTNULL,
            null,
            null,
            'userto');

        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }

        $field2 = new xmldb_field('status',
            XMLDB_TYPE_INTEGER,
            '1',
            null,
            XMLDB_NOTNULL,
            null,
            1,
            'event');

        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }

        upgrade_plugin_savepoint(true, 2017061102, 'local', 'b13_dashboard');
    }

    if ($oldversion < 2017071714) {

        if (!$dbman->table_exists('b13_dashboard_reportcat')) {
            $table = new xmldb_table('b13_dashboard_reportcat');

            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
            $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL);
            $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL);
            $table->add_field('image', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL);
            $table->add_field('enable', XMLDB_TYPE_INTEGER, '1', true, XMLDB_NOTNULL);
            $table->add_field('enablesql', XMLDB_TYPE_TEXT, 'big');

            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $table->add_index('type', XMLDB_INDEX_NOTUNIQUE, array('type'));

            $dbman->create_table($table);
        }

        if (!$dbman->table_exists('b13_dashboard_reports')) {
            $table = new xmldb_table('b13_dashboard_reports');

            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
            $table->add_field('reportcatid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
            $table->add_field('reportkey', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL);
            $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL);
            $table->add_field('enable', XMLDB_TYPE_INTEGER, '1', true, XMLDB_NOTNULL);
            $table->add_field('enablesql', XMLDB_TYPE_TEXT, 'big');
            $table->add_field('reportsql', XMLDB_TYPE_TEXT, 'big');
            $table->add_field('prerequisit', XMLDB_TYPE_CHAR, '60', null, XMLDB_NOTNULL);
            $table->add_field('columns', XMLDB_TYPE_TEXT, 'big');
            $table->add_field('foreach', XMLDB_TYPE_TEXT, 'big');

            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2017071714, 'local', 'b13_dashboard');
    }

    if ($oldversion < 2017081601) {
        $table = new xmldb_table('b13_dashboard_menu');
        $index = new xmldb_index('unique', XMLDB_INDEX_NOTUNIQUE, array('link'));

        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        $table = new xmldb_table('b13_dashboard_webpages');
        $index = new xmldb_index('unique', XMLDB_INDEX_NOTUNIQUE, array('link'));

        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        $table = new xmldb_table('b13_dashboard_events');
        $index = new xmldb_index('unique', XMLDB_INDEX_NOTUNIQUE, array('module', 'event'));

        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        $table = new xmldb_table('b13_dashboard_reportcat');
        $index = new xmldb_index('unique', XMLDB_INDEX_NOTUNIQUE, array('type'));

        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        $table = new xmldb_table('b13_dashboard_reports');
        $index = new xmldb_index('unique', XMLDB_INDEX_NOTUNIQUE, array('reportkey'));

        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        upgrade_plugin_savepoint(true, 2017081601, 'local', 'b13_dashboard');
    }

    if ($oldversion < 2018032409) {
        $DB->delete_records('b13_dashboard_reports');
        $DB->delete_records('b13_dashboard_reportcat');

        upgrade_plugin_savepoint(true, 2018032409, 'local', 'b13_dashboard');
    }
    if ($oldversion < 2019051901) {
       
        $table = new xmldb_table('b13_examboards');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL);
        $table->add_field('idnumber', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL);
        $table->add_field('description', XMLDB_TYPE_TEXT, 'big');
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);

        $table1 = new xmldb_table('b13_examboards_courses');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table1->add_field('examboardid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('courseid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table1);
        
        upgrade_plugin_savepoint(true, 2019051901, 'local', 'b13_dashboard');

    }
    if ($oldversion < 2019051903) {
        $table1 = new xmldb_table('b13_student_learning_status');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table1->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('courseid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('moduleid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('readedpage', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table1);
        upgrade_plugin_savepoint(true, 2019051903, 'local', 'b13_dashboard');
    }
    if ($oldversion < 2019051904) {
        $table1 = new xmldb_table('b13_student_subject');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table1->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table1);

        $table2 = new xmldb_table('b13_student_topic');
        $table2->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table2->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table2->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table2->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table2);

        upgrade_plugin_savepoint(true, 2019051904, 'local', 'b13_dashboard');
    }
    

    if ($oldversion < 2019051905) {

        $table = new xmldb_table('b13_option');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL);
        $table->add_field('description', XMLDB_TYPE_TEXT, 'big');
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);

        $table1 = new xmldb_table('b13_subject_option');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table1->add_field('optionid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table1);

        $table1 = new xmldb_table('b13_topic_option');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table1->add_field('optionid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('categoryid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table1);

        upgrade_plugin_savepoint(true, 2019051905, 'local', 'b13_dashboard');

    }
    if ($oldversion < 2019051906) {

        $table1 = new xmldb_table('b13_student_selection');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table1->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('subjectid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('optionid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_field('examboardid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table1);
        upgrade_plugin_savepoint(true, 2019051906, 'local', 'b13_dashboard');
    }

    if($oldversion < 2019051907){
         $table = new xmldb_table('b13_student_learning_status');
         if(!$dbman->table_exists($table)) {
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
            $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
            $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
            $table->add_field('moduleid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
            $table->add_field('readedpage', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
            $table->add_field('totalpage', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
         }

         $field = new xmldb_field('totalpage', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
         if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
         }
        
         upgrade_plugin_savepoint(true, 2019051907, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051908){

        $table = new xmldb_table('b13_student_learning_status');
        $field = new xmldb_field('subjectid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
         if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
         }

         $field = new xmldb_field('topicid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
         if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
         }
        upgrade_plugin_savepoint(true, 2019051908, 'local', 'b13_dashboard');
    }

    if($oldversion < 2019051912){

        $table = new xmldb_table('b13_status_student_subtopic');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('bookstatus', XMLDB_TYPE_FLOAT, '10,5', true, XMLDB_NOTNULL);
        $table->add_field('quizstatus', XMLDB_TYPE_FLOAT, '10,5', true, XMLDB_NOTNULL);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
        upgrade_plugin_savepoint(true, 2019051912, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051913){
        $table = new xmldb_table('b13_status_student_subtopic');
        $field = new xmldb_field('readedpage', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('readedquiz', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('subjectid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('topicid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051913, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051914){
        $table = new xmldb_table('b13_status_student_subtopic');
        $field = new xmldb_field('totalpages', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2019051914, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051916){
        $table = new xmldb_table('b13_status_student_subtopic');
        $field = new xmldb_field('quizprogression', XMLDB_TYPE_TEXT, 'big');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        upgrade_plugin_savepoint(true, 2019051916, 'local', 'b13_dashboard');
    }

    if($oldversion < 2019051918){
        $table = new xmldb_table('b13_student_progression_quiz');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('subtopicid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('time', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('result', XMLDB_TYPE_FLOAT, '10,5', true);
        $table->add_field('completed', XMLDB_TYPE_INTEGER, '10,5', true);
        $table->add_field('quizlist', XMLDB_TYPE_TEXT, 'big');
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
        upgrade_plugin_savepoint(true, 2019051918, 'local', 'b13_dashboard');
    }

    if($oldversion < 2019051919){
        $table = new xmldb_table('b13_student_quiz_box');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('questionid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('boxnumber', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
        upgrade_plugin_savepoint(true, 2019051919, 'local', 'b13_dashboard');

    }
    if($oldversion < 2019051920){
        $table = new xmldb_table('b13_student_quiz_box');
        $field = new xmldb_field('subtopicid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051920, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051921){
        $table = new xmldb_table('b13_examboards_courses');
        $field = new xmldb_field('topicid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051921, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051922){
        $table = new xmldb_table('b13_examboards_courses');
        $field = new xmldb_field('subjectid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051922, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051925){
        $table = new xmldb_table('b13_students_teachers');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('subjectid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('teachername', XMLDB_TYPE_TEXT, 'big');
        $table->add_field('teacheremail', XMLDB_TYPE_TEXT, 'big');
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
        upgrade_plugin_savepoint(true, 2019051925, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051926){

        $table = new xmldb_table('b13_school');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('schoolname', XMLDB_TYPE_TEXT, 'big');
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
        upgrade_plugin_savepoint(true, 2019051926, 'local', 'b13_dashboard');

    }
    if($oldversion < 2019051927){

        $table = new xmldb_table('b13_examboards_options');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('examboardid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('optionid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_table($table);
        upgrade_plugin_savepoint(true, 2019051927, 'local', 'b13_dashboard');

    }
   
    if($oldversion < 2019051928){
        $table = new xmldb_table('b13_student_quiz_box');
        $field = new xmldb_field('topicid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051928, 'local', 'b13_dashboard');

    }

    if($oldversion < 2019051929){
        $table = new xmldb_table('b13_examboards_options');
        $field = new xmldb_field('subjectid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table2 = new xmldb_table('b13_student_progression_quiz');
        $field2 = new xmldb_field('examboardid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table2, $field2)) {
            $dbman->add_field($table2, $field2);
        }
        upgrade_plugin_savepoint(true, 2019051929, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051930){
        $table = new xmldb_table('b13_student_quiz_box');
        $field = new xmldb_field('examboardid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051930, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051931){
        $table = new xmldb_table('b13_report_user_visit');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('time', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        $table1 = new xmldb_table('b13_report_user_quiz');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table1->add_field('userid', XMLDB_TYPE_INTEGER, '10', true);
        $table1->add_field('questionid', XMLDB_TYPE_INTEGER, '10', true);
        $table1->add_field('subtopicid', XMLDB_TYPE_INTEGER, '10', true);
        $table1->add_field('time', XMLDB_TYPE_INTEGER, '10', true);
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table1)) {
            $dbman->create_table($table1);
        }

        $table2 = new xmldb_table('b13_report_user_ativity');
        $table2->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table2->add_field('userid', XMLDB_TYPE_INTEGER, '10', true);
        $table2->add_field('subtopicid', XMLDB_TYPE_INTEGER, '10', true);
        $table2->add_field('topicid', XMLDB_TYPE_INTEGER, '10', true);
        $table2->add_field('subjectid', XMLDB_TYPE_INTEGER, '10', true);
        $table2->add_field('time', XMLDB_TYPE_INTEGER, '10', true);
        $table2->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table2)) {
            $dbman->create_table($table2);
        }
        upgrade_plugin_savepoint(true, 2019051931, 'local', 'b13_dashboard');
    }

    if($oldversion < 2019051932){
        $table = new xmldb_table('b13_subject_icon');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('subjectid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('icon', XMLDB_TYPE_TEXT, 'big');
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        upgrade_plugin_savepoint(true, 2019051932, 'local', 'b13_dashboard');
    }

    if($oldversion < 2019051933){
        $table = new xmldb_table('b13_correction_report');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('title', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('body', XMLDB_TYPE_TEXT, 'big');
        $table->add_field('url', XMLDB_TYPE_TEXT, 'big');
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10');
        $table->add_field('subjectid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('topicid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('subtopicid', XMLDB_TYPE_INTEGER, '10', true);
     
        $table->add_field('questionid', XMLDB_TYPE_INTEGER, '10', true);
       
        $table->add_field('chapterid', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '10', true);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        upgrade_plugin_savepoint(true, 2019051933, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051934){

        $table = new xmldb_table('b13_correction_report');
        $field = new xmldb_field('cmid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
       
        upgrade_plugin_savepoint(true, 2019051934, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051936){
        $table = new xmldb_table('b13_correction_report');
        $field = new xmldb_field('status');
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        
        $field = new xmldb_field('status', XMLDB_TYPE_CHAR, '255', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051936, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051937){
        $table = new xmldb_table('b13_questiongroup');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('title', XMLDB_TYPE_TEXT, '10', true);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        $table = new xmldb_table('b13_questiongroup_question');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('questiongroupid', XMLDB_TYPE_INTEGER, '10');
        $table->add_field('questionid', XMLDB_TYPE_INTEGER, '10');
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        upgrade_plugin_savepoint(true, 2019051937, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051938){ 
        $table = new xmldb_table('b13_questiongroup_question');
        $field = new xmldb_field('qtype', XMLDB_TYPE_CHAR, '255', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051938, 'local', 'b13_dashboard');
    }

    if($oldversion < 2019051939){ 
        $table = new xmldb_table('b13_questiongroup');
        $field = new xmldb_field('subtopicid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('topicid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('subjectid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('cmid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        upgrade_plugin_savepoint(true, 2019051939, 'local', 'b13_dashboard');
    }
    if($oldversion < 2019051940){ 
        $table = new xmldb_table('b13_student_qgroup_box');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('questiongroupid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('boxnumber', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('examboardid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('subtopicid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('topicid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_field('subjectid', XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        upgrade_plugin_savepoint(true, 2019051940, 'local', 'b13_dashboard');

    }
    if($oldversion < 2019051941){ 
        $table = new xmldb_table('b13_student_quiz_box');
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }
        $table = new xmldb_table('b13_student_learning_status');
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }
        upgrade_plugin_savepoint(true, 2019051941, 'local', 'b13_dashboard');
    }
    \local_b13_dashboard\install\report_install::create_categores();
    \local_b13_dashboard\install\report_install::create_reports();
    \local_b13_dashboard\install\users_import_install::install_or_update();

    return true;
}
